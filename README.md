# Life in Space #

My most recent game project pastime. It is coded in Scala and uses the libGDX game library.
Started on 12-20-2015 for the libGDX game jam, although I probably won't enter.

### To Do ###
1. Enemy Types and Hunting / Moss
2. Implement All Menu State Functionality
3. 2 Hostile Textures and Some Bullet Textures / Sounds

### Story ###
You were part of Science Mission to an Asteroid Field. 
Your Ship was Hit By an Asteroid.
You are the Sole Survivor.
Build a distress beacon to call for help, until then, survive!

### Player Goals ###
1. Acquire water and oxygen From Ice.
2. Acquire food by harvesting moss or hunting aliens.  
3. Defense yourself from hostiles like guns, turrets, and predators.
4. Carve out asteroids to mine resources and survive.
5. Explore the map to find new lands and challenges.

### What is Done ###
1. Art
2. Player Movement Mechanics
3. Crafting and Inventories
4. Worlds
5. Interactions between Player and World
6. Threats: Generic Hostile
7. In Game GUI
8. Menu States: Functional  (Ugly and Not Specialized)
9. End Goal
10. Dropping and Destroying Items

### Stretch Goals ###
1. Accurate Asteroid Maps
2. Generators, Wires, and Power Management
3. Base Building
4. Infinite Space Worlds
5. Ships and Raiders

### Features ###
    Utility
    1. Game Loading and Saving
    2. Options Screen
    3. Pause Screen

    Machines
    1. Ore Processor
    2. Material Assembler
    3. Food Processor
    4. Ice Processor
    5. Medic Station
    6. Turret
    7. Distress Beacon

    Functional Items
    1. Gun
    2. Drill
    3. Food Ration
    4. Oxygen Tank
    5. Water Tank

    Enemies
    1. Predator
    2. Turrets

    Block Types
    1. Space
    2. Asteroid
    3. Holes
    4. Ores
    5. Ice
    
    List
    14 Crafting Components
    11 Ores (Ice and Rock Included)
    5 Block Types
    7 Machines
    2 Tools
    2 Enemies