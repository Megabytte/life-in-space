package com.megabytte.lisg.desktop

import com.badlogic.gdx.backends.lwjgl.{LwjglApplication, LwjglApplicationConfiguration}
import com.megabytte.lisg.Core

/**
  * Created by Keith Webb on 12/14/15.
  */
object DesktopLauncher
{
    def main(args: Array[String]): Unit =
    {
        val config = new LwjglApplicationConfiguration()
        //16x9 Ratio
        config.width = 1152
        config.height = 648
        config.title = "Life In Space : Keith Webb"
        config.resizable = false
        new LwjglApplication(new Core(), config)
    }
}
