package com.megabytte.lisg.inventory.items

/**
  * Copyright © 2015 Keith Webb
  */
object Item {
    object IDs {
        private var id = 0

        private def genID: Int = {
            val toReturn = id
            id += 1
            toReturn
        }

        val EMPTY = genID

        //Ores
        val ROCK = genID
        val ORE_IRON = genID
        val ORE_GOLD = genID
        val ORE_URANIUM = genID
        val ORE_COBALT = genID
        val ORE_NICKEL = genID
        val ORE_SILVER = genID
        val ORE_PLATINUM = genID
        val ORE_MAGNESIUM = genID
        val ORE_SILICON = genID

        //Raw Organics
        val ICE = genID
        val SPACE_MOSS = genID
        val SPACE_MEAT = genID

        //Refined Player Materials
        val FOOD = genID
        val OXYGEN_TANK = genID
        val WATER_TANK = genID

        //Refined Minerals
        val IRON_INGOT = genID
        val GOLD_INGOT = genID
        val COBALT_INGOT = genID
        val NICKEL_INGOT = genID
        val SILVER_INGOT = genID
        val URANIUM_INGOT = genID
        val SILICON_WAFER = genID
        val GRAVEL = genID
        val PLATINUM_INGOT = genID   //Used for thrusters
        val MAGNESIUM_POWDER = genID //Used for ammo

        //Items That Are Machines
        val GENERIC_MACHINE = genID
        val MEDIC_STATION = genID
        val TURRET = genID
        val GENERATOR = genID
        val ICE_PROCESSOR = genID
        val ORE_REFINERY = genID
        val MATERIAL_ASSEMBLER = genID
        val FOOD_PROCESSOR = genID
        val DISTRESS_BEACON = genID

        //Crafted Goods
        val STEEL_PLATE = genID
        val INTERIOR_PLATE = genID
        val CONSTRUCTION_COMPONENT = genID
        val METAL_GRID = genID
        val MEDICAL_COMPONENTS = genID
        val REACTOR_COMPONENTS = genID
        val SUPERCONDUCTOR_COMPONENTS = genID
        val COMPUTER = genID
        val RADIO_COMMUNICATIONS_COMPONENTS = genID
        val LARGE_STEEL_TUBE = genID
        val SMALL_STEEL_TUBE = genID
        val DISPLAY = genID
        val MOTOR = genID

        //Items That Are "Fire"-able
        val GUN = genID
        val GRINDER = genID
        val DRILL = genID
        val WELDER = genID
    }
    object Types {
        val NoneType = 0
        val MachineType = 1
        val UsableType = 2
        val InertType = 3
    }
}