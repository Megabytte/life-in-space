package com.megabytte.lisg.inventory.items

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.scenes.scene2d.ui._
import com.megabytte.lisg.util.Assets

/**
  * Copyright © 2015 Keith Webb
  */
class InventoryItem(lButton: Button) {
    var ID : Int = Item.IDs.EMPTY
    var count : Int = 0
    var cellID : Int = 0
    var itemType = Item.Types.NoneType
    var toolTip : TextTooltip = null

    val numberLabel = new Label("0", Assets.ui_skin)
    numberLabel.setFontScale(0.7f)
    numberLabel.setVisible(false)

    var texture: Texture = null
    private var blankTexture = false
    var image: Image = null
    val button = lButton

    determineImage()

    def determineImage(): Unit = {
        if(blankTexture) {
            blankTexture = false
        }

        texture = Assets.determineTextureFromItemID(ID)
        if(ID == 0) {
            blankTexture = true
        }

        itemType = Assets.determineItemTypeFromID(ID)

        image = new Image(texture)
    }

    def changeID(newID: Int): Unit = {
        ID = newID

        if(toolTip != null)
            lButton.removeListener(toolTip)
        if(ID != 0) {
            toolTip = new TextTooltip(Assets.getNameFromItemID(ID), TooltipManager.getInstance(), Assets.ui_skin)
            lButton.addListener(toolTip)
            toolTip.setInstant(true)
        }

        determineImage()
        button.getCells.get(0).setActor(image)
    }

    def setCount(newCount: Int): Unit = {
        count = newCount
        addToCount(0)
    }

    def addToCount(newCount: Int): Unit = {
        count += newCount
        numberLabel.setText(count.toString)
        if(count > 0) {
            numberLabel.setVisible(true)
        } else {
            numberLabel.setVisible(false)
        }
    }

    def subFromCount(newCount: Int): Unit = {
        count -= newCount
        numberLabel.setText(count.toString)
        if(count > 0) {
            numberLabel.setVisible(true)
        } else {
            numberLabel.setVisible(false)
            count = 0
            addToCount(0)
            changeID(0)
        }
    }
}
