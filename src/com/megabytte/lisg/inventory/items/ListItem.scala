package com.megabytte.lisg.inventory.items

import com.badlogic.gdx.scenes.scene2d.ui.{Button, TooltipManager, TextTooltip, Image}
import com.megabytte.lisg.util.Assets

/**
  * Copyright © 2015 Keith Webb
  */
class ListItem(id: Int, lButton: Button) {
    private var ID : Int = id
    val image = new Image(Assets.determineTextureFromItemID(ID))
    var toolTip = new TextTooltip(Assets.getNameFromItemID(ID), TooltipManager.getInstance(), Assets.ui_skin)
    val button = lButton

    def getID: Int = ID

    def changeID(id: Int): Unit = {
        ID = id
        button.removeListener(toolTip)
        toolTip = new TextTooltip(Assets.getNameFromItemID(ID), TooltipManager.getInstance(), Assets.ui_skin)
        button.addListener(toolTip)
    }

    //val nameLabel = new Label(Assets.getNameFromItemID(ID), Assets.ui_skin)
    //nameLabel.setFontScale(0.7f)
}
