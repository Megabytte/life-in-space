package com.megabytte.lisg.inventory

import com.badlogic.gdx.Input.Keys
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.scenes.scene2d.ui.{Button, Skin, Window}
import com.badlogic.gdx.scenes.scene2d.{Actor, InputEvent, InputListener, Stage}
import com.badlogic.gdx.{Gdx, Input}
import com.megabytte.lisg.inventory.items.InventoryItem
import com.megabytte.lisg.util.Assets
import org.json.JSONObject

import scala.collection.mutable

/**
  * Copyright © 2015 Keith Webb
  */
class HotBar(inventory: Inventory, stage: Stage, lSkin: Skin) extends Actor {

    val slotWidth = 6
    val slotHeight = 1
    val numSlots = slotWidth * slotHeight
    val slotPixelWidth = 64
    val slotPixelHeight = 64

    val skin = lSkin
    val selectedButtonColor = Color.LIGHT_GRAY

    var hotBarSelectedID = 0
    var selectedItem: InventoryItem = null

    val window = new Window("", skin)
    window.setMovable(false)
    window.setSize(slotPixelWidth * slotWidth, slotPixelHeight * slotHeight + slotPixelHeight / 4)
    window.setPosition(Gdx.graphics.getWidth / 2 - window.getWidth / 2, 0)
    stage.addActor(window)

    val slots = new mutable.MutableList[InventoryItem]()

    def serialize(): JSONObject = {
        val json = new JSONObject()
        json.put("selected", hotBarSelectedID)
        var iterator = 0
        for(i <- slots.indices) {
            val slot = slots(i)
            if(slot.ID != 0) {
                json.put("slot" + iterator.toString, (i, slot.ID, slot.count))
                iterator += 1
            }
        }
        json
    }

    def deserialize(json: JSONObject): Unit = {
        hotBarSelectedID = json.get("selected").asInstanceOf[Int]
        var iterator = 0
        for (i <- slots.indices) {
            val exists = json.has("slot" + iterator.toString)
            if (exists) {
                var info = json.get("slot" + iterator.toString).asInstanceOf[String]
                info = info.replace("(", "")
                info = info.replace(")", "")
                val pieces = info.split(",")
                val li = pieces(0).toInt
                val id = pieces(1).toInt
                val count = pieces(2).toInt
                val slot = slots(li)
                slot.changeID(id)
                slot.setCount(count)
            }
            iterator += 1
        }
    }

    for (y <- 0 until slotHeight) {
        for (x <- 0 until slotWidth) {
            val item = createCell(x)
            window.add(item.button).size(slotPixelWidth)
            slots += item
        }
        window.row()
    }

    stage.addActor(this)
    select()

    def select(): Unit = {
        for (i <- slots.indices) {
            val cell = slots.get(i).get
            if (i == hotBarSelectedID) {
                cell.button.setColor(selectedButtonColor)
                selectedItem = cell
            } else if (cell.button.getColor == selectedButtonColor) {
                cell.button.setColor(inventory.defaultButtonColor)
            }
        }
    }

    def addItem(ID: Int, amount: Int): Unit = {
        for (x <- 0 until slotWidth) {
            val item = slots.get(x).get

            if (item.ID == ID) {
                item.addToCount(amount)
                return
            }
            else if (item.ID == 0) {
                item.changeID(ID)
                item.addToCount(amount)
                return
            }
        }

    }

    def getNumberOfEmptySlots: Int = {
        var number = 0
        for(item: InventoryItem <- slots) {
            if(item.ID == 0)
                number += 1
        }
        number
    }
    def getNumberOfFilledSlots: Int = {
        var number = 0
        for(item: InventoryItem <- slots) {
            if(item.ID != 0)
                number += 1
        }
        number
    }

    override def act(deltaTime: Float): Unit = {
        super.act(deltaTime)
        window.getTitleLabel.setText(Assets.getNameFromItemID(selectedItem.ID))

        if (!inventory.isEnabled) {
            if (Gdx.input.isKeyJustPressed(Keys.NUM_1)) {
                hotBarSelectedID = 0
                select()
            }
            else if (Gdx.input.isKeyJustPressed(Keys.NUM_2)) {
                hotBarSelectedID = 1
                select()
            }
            else if (Gdx.input.isKeyJustPressed(Keys.NUM_3)) {
                hotBarSelectedID = 2
                select()
            }
            else if (Gdx.input.isKeyJustPressed(Keys.NUM_4)) {
                hotBarSelectedID = 3
                select()
            }
            else if (Gdx.input.isKeyJustPressed(Keys.NUM_5)) {
                hotBarSelectedID = 4
                select()
            }
            else if (Gdx.input.isKeyJustPressed(Keys.NUM_6)) {
                hotBarSelectedID = 5
                select()
            }
        }

    }

    private def createCell(cellID: Int): InventoryItem = {
        val button = new Button(skin)
        button.setSize(slotPixelWidth, slotPixelHeight)
        //Add Inventory Item To Cell

        val inventoryItem = new InventoryItem(button)
        inventoryItem.cellID = cellID
        button.add(inventoryItem.image).center().row()
        button.add(inventoryItem.numberLabel).bottom().right()
        button.setUserObject(inventoryItem)

        button.addListener(new InputListener() {
            override def scrolled(event: InputEvent, x: Float, y: Float, amount: Int): Boolean = {
                //Todo: Implement
                false
            }

            override def touchDown(event: InputEvent, x: Float, y: Float, pointer: Int, button: Int): Boolean = {
                if (inventory.isEnabled) {
                    if (!inventory.selecting && button == Input.Buttons.LEFT) {
                        inventory.selecting = true
                        inventory.getHalf = false
                        inventory.sourceCell = event.getListenerActor.asInstanceOf[Button].getUserObject.asInstanceOf[InventoryItem]
                        inventory.sourceCell.button.setColor(Color.RED)
                        if (inventory.sourceCell.count == 0) {
                            inventory.sourceCell.button.setColor(inventory.defaultButtonColor)
                            inventory.sourceCell = null
                            inventory.selecting = false
                        }
                    }
                    else if (inventory.selecting && button == Input.Buttons.LEFT) {
                        inventory.selecting = false
                        inventory.dropOne = false
                        inventory.destinationCell = event.getListenerActor.asInstanceOf[Button].getUserObject.asInstanceOf[InventoryItem]
                        inventory.move()
                    }
                    if (!inventory.getHalf && !inventory.selecting && button == Input.Buttons.RIGHT) {
                        inventory.selecting = true
                        inventory.getHalf = true
                        inventory.sourceCell = event.getListenerActor.asInstanceOf[Button].getUserObject.asInstanceOf[InventoryItem]
                        inventory.sourceCell.button.setColor(Color.GREEN)
                        if (inventory.sourceCell.count == 0) {
                            inventory.sourceCell.button.setColor(inventory.defaultButtonColor)
                            inventory.sourceCell = null
                            inventory.selecting = false
                            inventory.getHalf = false
                        }
                    }
                    else if (inventory.selecting && button == Input.Buttons.RIGHT) {
                        inventory.dropOne = true
                        inventory.destinationCell = event.getListenerActor.asInstanceOf[Button].getUserObject.asInstanceOf[InventoryItem]
                        inventory.move()
                    }

                    val bk = hotBarSelectedID
                    hotBarSelectedID = -1
                    select()
                    hotBarSelectedID = bk
                }
                else if(button == Input.Buttons.LEFT) {
                    //Make Slot Active!
                    hotBarSelectedID = inventoryItem.cellID
                    select()
                }

                true
            }
        })

        inventoryItem
    }
}
