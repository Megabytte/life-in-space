package com.megabytte.lisg.inventory

import com.badlogic.gdx.Input
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.scenes.scene2d.ui.{TooltipManager, Button, Window}
import com.badlogic.gdx.scenes.scene2d.{Actor, InputEvent, InputListener, Stage}
import com.megabytte.lisg.inventory.items.{InventoryItem, Item}
import com.megabytte.lisg.util.{Assets, Config}
import org.json.JSONObject

import scala.collection.mutable

/**
  * Copyright © 2015 Keith Webb
  */
class Inventory(stage: Stage, windowTitle: String, widthInSlots: Int, heightInSlots: Int, centerX: Int, centerY: Int) extends Actor {

    val slotWidth = widthInSlots
    val slotHeight = heightInSlots
    val numSlots = slotWidth * slotHeight

    var externalInventory : Inventory = null

    val defaultButtonColor = Color.WHITE

    var isEnabled = false

    var sourceCell: InventoryItem = null
    var destinationCell: InventoryItem = null

    var selecting = false
    var getHalf = false
    var dropOne = false

    val window = new Window(windowTitle, Assets.ui_skin)
    window.setMovable(false)
    window.setSize(Config.inventorySlotPixelWidth * slotWidth + Config.inventorySlotPixelWidth,
        Config.inventorySlotPixelHeight * slotHeight + Config.inventorySlotPixelHeight)

    window.setPosition(centerX - window.getWidth / 2, centerY - window.getHeight / 2)

    stage.addActor(window)

    val slots = new mutable.HashMap[(Int, Int), InventoryItem]()

    for(y <- 0 until slotHeight) {
        for(x <- 0 until slotWidth) {
            val item = createCell()
            window.add(item.button).size(Config.inventorySlotPixelWidth)
            slots.put((x, y), item)
        }
        window.row()
    }

    stage.addActor(this)
    hide()

    def serialize(): JSONObject = {
        val json = new JSONObject()
        var iterator = 0
        for(y <- 0 until slotHeight) {
            for (x <- 0 until slotWidth) {
                val slot = slots((x, y))
                if(slot.ID != 0) {
                    json.put("slot" + iterator.toString, (x, y, slot.ID, slot.count))
                    iterator += 1
                }
            }
        }
        json
    }
    def deserialize(json: JSONObject): Unit = {
        var iterator = 0
        for(y <- 0 until slotHeight) {
            for (x <- 0 until slotWidth) {
                val exists = json.has("slot" + iterator.toString)
                if(exists) {
                    var info = json.get("slot" + iterator.toString).asInstanceOf[String]
                    info = info.replace("(", "")
                    info = info.replace(")", "")
                    val pieces = info.split(",")
                    val lx = pieces(0).toInt
                    val ly = pieces(1).toInt
                    val id = pieces(2).toInt
                    val count = pieces(3).toInt
                    val slot = slots((lx, ly))
                    slot.changeID(id)
                    slot.setCount(count)
                }
                iterator += 1
            }
        }
    }

    def getNumberOfSlotsWithID(ID: Int): Int = {
        var count = 0
        for(y <- 0 until slotHeight) {
            for (x <- 0 until slotWidth) {
                val item = slots.get((x, y)).get

                if(item.ID == ID) {
                    count += 1
                }
            }
        }
        count
    }

    def getNumberOfEmptySlots: Int = {
        var count = 0
        for(y <- 0 until slotHeight) {
            for (x <- 0 until slotWidth) {
                val item = slots.get((x, y)).get

                if(item.ID == Item.IDs.EMPTY) {
                    count += 1
                }
            }
        }
        count
    }

    def getNumberOfFilledSlots: Int = {
        var count = 0
        for(y <- 0 until slotHeight) {
            for (x <- 0 until slotWidth) {
                val item = slots.get((x, y)).get

                if(item.ID != Item.IDs.EMPTY) {
                    count += 1
                }
            }
        }
        count
    }

    def getASlotWithID(ID: Int): InventoryItem = {
        for(y <- 0 until slotHeight) {
            for (x <- 0 until slotWidth) {
                val item = slots.get((x, y)).get

                if(item.ID == ID) {
                    return item
                }
            }
        }

        null
    }

    def removeCountOfItem(ID: Int, amount: Int): Unit = {
        var count = 0
        for(y <- 0 until slotHeight) {
            for (x <- 0 until slotWidth) {
                val item = slots.get((x, y)).get

                if(item.ID == ID) {
                    count += item.count
                    item.subFromCount(amount)
                    if(count >= amount)
                        return
                }
            }
        }
    }

    def removeAllOfItem(ID: Int): Unit = {
        for(y <- 0 until slotHeight) {
            for (x <- 0 until slotWidth) {
                val item = slots.get((x, y)).get

                if(item.ID == ID) {
                    item.subFromCount(item.count)
                }
            }
        }
    }

    def containsItem(ID: Int): Boolean = {
        for(y <- 0 until slotHeight) {
            for (x <- 0 until slotWidth) {
                val item = slots.get((x, y)).get

                if(item.ID == ID) {
                    return true
                }
            }
        }

        false
    }

    def containsItemWithCount(ID: Int, amount: Int): Boolean = {
        var count = 0
        for(y <- 0 until slotHeight) {
            for (x <- 0 until slotWidth) {
                val item = slots.get((x, y)).get

                if(item.ID == ID) {
                    count += item.count
                }
            }
        }

        if(count >= amount)
            return true

        false
    }

    def addItem(ID: Int, amount: Int): Unit = {
        for(y <- 0 until slotHeight) {
            for (x <- 0 until slotWidth) {
                val item = slots.get((x, y)).get

                if(item.ID == ID) {
                    item.addToCount(amount)
                    return
                }
                else if(item.ID == 0) {
                    item.changeID(ID)
                    item.addToCount(amount)
                    return
                }
            }
        }
    }

    override def act(deltaTime: Float): Unit = {
        super.act(deltaTime)

        if(sourceCell != null) {
            //Todo: Have the image on sourceCell follow the mouse, so that item moving is more clear.
        }
    }

    def move(): Unit = {
        if(sourceCell == null || destinationCell == null)
            return
        if(sourceCell == destinationCell) {
            moveReset()
            return
        }

        //Perform Move
        val id = sourceCell.ID
        val count = sourceCell.count

        if(id == 0) {
            moveReset()
            return
        }

        if(destinationCell.ID == 0 || destinationCell.ID == id) {
            if(getHalf && !dropOne) {
                getHalf = false
                val newCount = count / 2
                if(newCount >= 1) {
                    if(destinationCell.ID == 0)
                        destinationCell.changeID(id)
                    sourceCell.subFromCount(newCount)
                    destinationCell.addToCount(newCount)
                }
            } else if(getHalf && dropOne) {
                if(sourceCell.count != 0) {
                    if (destinationCell.ID == 0)
                        destinationCell.changeID(id)
                    sourceCell.subFromCount(1)
                    destinationCell.addToCount(1)
                    if(sourceCell.count == 0)
                        moveReset()
                }
            } else {
                if(destinationCell.ID == 0)
                    destinationCell.changeID(id)
                if(dropOne) {
                    sourceCell.subFromCount(1)
                    destinationCell.addToCount(1)
                    if(sourceCell.count == 0)
                        moveReset()
                } else {
                    destinationCell.addToCount(count)
                    sourceCell.changeID(0)
                    sourceCell.setCount(0)
                }
            }
        }

        if(!dropOne) {
            moveReset()
        }
    }

    def moveReset(): Unit = {
        if(sourceCell != null)
            sourceCell.button.setColor(defaultButtonColor)
        if(destinationCell != null)
            destinationCell.button.setColor(defaultButtonColor)

        sourceCell = null
        destinationCell = null
    }

    def show(): Unit = {
        isEnabled = true
        window.setVisible(isEnabled)
        TooltipManager.getInstance().enabled = isEnabled
    }

    def hide(): Unit = {
        if(externalInventory != null) {
            externalInventory.hide()
        }

        isEnabled = false
        TooltipManager.getInstance().enabled = isEnabled
        window.setVisible(isEnabled)
        selecting = false
        dropOne = false
        getHalf = false
        if(sourceCell != null)
            sourceCell.button.setColor(defaultButtonColor)
        if(destinationCell != null)
            destinationCell.button.setColor(defaultButtonColor)
        sourceCell = null
        destinationCell = null
    }

    private def createCell(): InventoryItem = {
        val button = new Button(Assets.ui_skin)
        button.setSize(Config.inventorySlotPixelWidth, Config.inventorySlotPixelHeight)
        //Add Inventory Item To Cell

        val inventoryItem = new InventoryItem(button)
        button.add(inventoryItem.image).center().row()
        button.add(inventoryItem.numberLabel).bottom().right()
        button.setUserObject(inventoryItem)

        button.addListener(new InputListener() {
            override def touchDown(event: InputEvent, x: Float, y: Float, pointer: Int, button: Int): Boolean = {
                if(externalInventory != null) {
                    if (!externalInventory.selecting && button == Input.Buttons.LEFT) {
                        externalInventory.selecting = true
                        externalInventory.getHalf = false
                        externalInventory.sourceCell = event.getListenerActor.asInstanceOf[Button].getUserObject.asInstanceOf[InventoryItem]
                        externalInventory.sourceCell.button.setColor(Color.RED)
                        if (externalInventory.sourceCell.count == 0) {
                            externalInventory.sourceCell.button.setColor(externalInventory.defaultButtonColor)
                            externalInventory.sourceCell = null
                            externalInventory.selecting = false
                        }
                    } else if (externalInventory.selecting && button == Input.Buttons.LEFT) {
                        externalInventory.selecting = false
                        externalInventory.dropOne = false
                        externalInventory.destinationCell = event.getListenerActor.asInstanceOf[Button].getUserObject.asInstanceOf[InventoryItem]
                        externalInventory.move()
                    }
                    if (!externalInventory.getHalf && !externalInventory.selecting && button == Input.Buttons.RIGHT) {
                        externalInventory.selecting = true
                        externalInventory.getHalf = true
                        externalInventory.sourceCell = event.getListenerActor.asInstanceOf[Button].getUserObject.asInstanceOf[InventoryItem]
                        externalInventory.sourceCell.button.setColor(Color.GREEN)
                        if (externalInventory.sourceCell.count == 0) {
                            externalInventory.sourceCell.button.setColor(externalInventory.defaultButtonColor)
                            externalInventory.sourceCell = null
                            externalInventory.selecting = false
                            externalInventory.getHalf = false
                        }
                    }
                    else if (externalInventory.selecting && button == Input.Buttons.RIGHT) {
                        externalInventory.dropOne = true
                        externalInventory.destinationCell = event.getListenerActor.asInstanceOf[Button].getUserObject.asInstanceOf[InventoryItem]
                        externalInventory.move()
                    }
                }
                else {
                    if (!selecting && button == Input.Buttons.LEFT) {
                        selecting = true
                        getHalf = false
                        sourceCell = event.getListenerActor.asInstanceOf[Button].getUserObject.asInstanceOf[InventoryItem]
                        sourceCell.button.setColor(Color.RED)
                        if (sourceCell.count == 0) {
                            sourceCell.button.setColor(defaultButtonColor)
                            sourceCell = null
                            selecting = false
                        }
                    } else if (selecting && button == Input.Buttons.LEFT) {
                        selecting = false
                        dropOne = false
                        destinationCell = event.getListenerActor.asInstanceOf[Button].getUserObject.asInstanceOf[InventoryItem]
                        move()
                    }
                    if (!getHalf && !selecting && button == Input.Buttons.RIGHT) {
                        selecting = true
                        getHalf = true
                        sourceCell = event.getListenerActor.asInstanceOf[Button].getUserObject.asInstanceOf[InventoryItem]
                        sourceCell.button.setColor(Color.GREEN)
                        if (sourceCell.count == 0) {
                            sourceCell.button.setColor(defaultButtonColor)
                            sourceCell = null
                            selecting = false
                            getHalf = false
                        }
                    }
                    else if (selecting && button == Input.Buttons.RIGHT) {
                        dropOne = true
                        destinationCell = event.getListenerActor.asInstanceOf[Button].getUserObject.asInstanceOf[InventoryItem]
                        move()
                    }
                }

                true
            }
        })

        inventoryItem
    }
}
