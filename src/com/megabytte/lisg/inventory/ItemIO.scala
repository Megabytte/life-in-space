package com.megabytte.lisg.inventory

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.scenes.scene2d.ui.{Dialog, Label, TextButton, Window}
import com.badlogic.gdx.scenes.scene2d.{Actor, InputEvent, InputListener, Stage}
import com.megabytte.lisg.entities.player.Player
import com.megabytte.lisg.util.{Assets, Config}

/**
  * Copyright © 2015 Keith Webb
  */
class ItemIO(stage: Stage, player: Player, centerX: Float, centerY: Float) extends Actor {

    private var buttonIterator = 0

    var externalInventory = player.inventory
    var enabled = false

    val window = new Window("Item IO", Assets.ui_skin)
    window.setMovable(false)
    window.setSize(Config.inventorySlotPixelWidth + Config.inventorySlotPixelWidth / 2,
        Config.inventorySlotPixelHeight * 2 + Config.inventorySlotPixelHeight / 2)

    window.setPosition(centerX - window.getWidth / 2, centerY - window.getHeight / 2)

    stage.addActor(window)

    window.add(createCell()).size(Config.inventorySlotPixelWidth).row()
    window.add(createCell()).size(Config.inventorySlotPixelWidth)

    stage.addActor(this)
    hide()

    def show(): Unit = {
        enabled = true
        window.setVisible(enabled)
    }

    def hide(): Unit = {
        enabled = false
        window.setVisible(enabled)
    }

    def confirmDestroyWindow(): Unit = {
        val cell = externalInventory.sourceCell
        if(cell != null) {
            val name = Assets.getNameFromItemID(cell.ID)
            val count = cell.count

            val confirmText = "Are you sure you wish to destroy " + count + " of " + name + "?"
            val dialog = new Dialog("Confirm Item Destruction", Assets.ui_skin)
            dialog.setModal(false)

            val width = 500
            val height = 150

            dialog.setSize(width, height)
            dialog.setPosition(Gdx.graphics.getWidth / 2 - width / 2, Gdx.graphics.getHeight / 2 - height / 2)

            val text = new Label(confirmText, Assets.ui_skin)
            text.setPosition(0, height - 50)
            val noButton = new TextButton("No", Assets.ui_skin)
            val yesButton = new TextButton("Yes", Assets.ui_skin)

            dialog.addActor(text)
            dialog.button(noButton)
            dialog.button(yesButton)

            yesButton.addListener(new InputListener() {
                override def touchDown(event: InputEvent, x: Float, y: Float, pointer: Int, button: Int): Boolean = {
                    cell.changeID(0)
                    cell.setCount(0)
                    externalInventory.moveReset()
                    externalInventory.selecting = false
                    externalInventory.dropOne = false
                    externalInventory.getHalf = false
                    true
                }
            })

            stage.addActor(dialog)
        }
    }

    def dropItem(): Unit = {
        val cell = externalInventory.sourceCell
        if(cell != null) {
            Assets.gameWorld.addItem(player.sprite.getPixelX, player.sprite.getPixelY, cell.ID, cell.count)

            cell.changeID(0)
            cell.setCount(0)
            externalInventory.moveReset()
            externalInventory.selecting = false
            externalInventory.dropOne = false
            externalInventory.getHalf = false
        }
    }

    def createCell(): TextButton = {
        var text: String = ""

        if(buttonIterator == 0) {
            text = "Drop"
        }
        else if(buttonIterator == 1) {
            text = "Destroy"
        }

        val button = new TextButton(text, Assets.ui_skin)
        button.setSize(Config.inventorySlotPixelWidth, Config.inventorySlotPixelHeight)

        if(buttonIterator == 0) {
            button.addListener(new InputListener() {
                override def touchDown(event: InputEvent, x: Float, y: Float, pointer: Int, button: Int): Boolean = {
                    dropItem()
                    true
                }
            })
        }
        else if(buttonIterator == 1) {
            button.addListener(new InputListener() {
                override def touchDown(event: InputEvent, x: Float, y: Float, pointer: Int, button: Int): Boolean = {
                    confirmDestroyWindow()
                    true
                }
            })
        }

        buttonIterator += 1
        button
    }

}
