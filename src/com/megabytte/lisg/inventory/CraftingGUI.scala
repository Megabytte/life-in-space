package com.megabytte.lisg.inventory

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input.Keys
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.scenes.scene2d.ui.{TooltipManager, Button, Window}
import com.badlogic.gdx.scenes.scene2d.{Actor, InputEvent, InputListener, Stage}
import com.megabytte.lisg.inventory.items.{InventoryItem, Item, ListItem}
import com.megabytte.lisg.util.{Assets, Config}
import org.json.JSONObject

import scala.collection.mutable

/**
  * Copyright © 2015 Keith Webb
  */
class CraftingGUI(stage: Stage) extends Actor {

    val defaultButtonColor = Color.WHITE

    var isEnabled = false

    val queueCellWidth = 8
    val queueCellHeight = 1

    val listCellWidth = 4
    val listCellHeight = 6

    private val startID = Item.IDs.MEDIC_STATION
    private var IDIter = startID
    private val endID = Item.IDs.WELDER
    private var buttonIter = 0

    private var isShiftDown = false
    private var isControlDown = false

    val queueCenterX = Gdx.graphics.getWidth / 2
    val queueCenterY = Gdx.graphics.getHeight - (Gdx.graphics.getHeight / 8)
    val listCenterX = Gdx.graphics.getWidth / 8
    val listCenterY = Gdx.graphics.getHeight / 2

    val windowQueue = new Window("Queue", Assets.ui_skin)
    val windowList = new Window("List", Assets.ui_skin)
    windowQueue.setMovable(false)
    windowQueue.setSize(Config.inventorySlotPixelWidth * queueCellWidth + Config.inventorySlotPixelWidth,
        Config.inventorySlotPixelHeight * queueCellHeight + Config.inventorySlotPixelHeight)

    windowList.setMovable(false)
    windowList.setSize(Config.inventorySlotPixelWidth * listCellWidth + Config.inventorySlotPixelWidth / 4,
        Config.inventorySlotPixelHeight * listCellHeight + Config.inventorySlotPixelHeight / 2)

    windowQueue.setPosition(queueCenterX - windowQueue.getWidth / 2, queueCenterY - windowQueue.getHeight / 2)
    windowList.setPosition(listCenterX - windowList.getWidth / 2, listCenterY - windowList.getHeight / 2)

    stage.addActor(windowQueue)
    stage.addActor(windowList)

    var craftingQueueList = new mutable.MutableList[InventoryItem]()
    val itemList = new mutable.HashMap[String, ListItem]()

    for(y <- 0 until listCellHeight) {
        for(x <- 0 until listCellWidth) {
            val button = createListCell()
            windowList.add(button).size(Config.inventorySlotPixelWidth)
        }
        windowList.row()
    }

    for(y <- 0 until queueCellHeight) {
        for(x <- 0 until queueCellWidth) {
            val inventoryItem = createQueueCell()
            windowQueue.add(inventoryItem.button).size(Config.inventorySlotPixelWidth)
            craftingQueueList += inventoryItem
        }
        windowQueue.row()
    }

    stage.addActor(this)
    hide()

    def serialize(): JSONObject = {
        val json = new JSONObject()
        var iterator = 0
        for(i <- craftingQueueList.indices) {
            val slot = craftingQueueList(i)
            if(slot.ID != 0) {
                json.put("slot" + iterator.toString, (i, slot.ID, slot.count))
                iterator += 1
            }
        }
        json
    }
    def deserialize(json: JSONObject): Unit = {
        var iterator = 0
        for (i <- craftingQueueList.indices) {
            val exists = json.has("slot" + iterator.toString)
            if (exists) {
                var info = json.get("slot" + iterator.toString).asInstanceOf[String]
                info = info.replace("(", "")
                info = info.replace(")", "")
                val pieces = info.split(",")
                val li = pieces(0).toInt
                val id = pieces(1).toInt
                val count = pieces(2).toInt
                val slot = craftingQueueList(li)
                slot.changeID(id)
                slot.setCount(count)
            }
            iterator += 1
        }
    }

    def getFirstOpen: Int = {
        for(i <- craftingQueueList.indices) {
            val item = craftingQueueList.get(i).get
            if(item.ID == 0 || item.count <= 0) {
                return i
            }
        }

        -1
    }

    def queueItem(ID: Int): Unit = {
        if(isShiftDown) {
            val index = getFirstOpen
            if(index == -1)
                return
            val item = craftingQueueList.get(index).get
            item.changeID(ID)
            item.setCount(10)
        }
        else if(isControlDown) {
            val index = getFirstOpen
            if(index == -1)
                return
            val item = craftingQueueList.get(index).get
            item.changeID(ID)
            item.setCount(100)
        }
        else {
            val index = getFirstOpen
            if(index == -1)
                return
            val item = craftingQueueList.get(index).get
            item.changeID(ID)
            item.setCount(1)
        }
    }

    def reorder(): Unit = {
        for(i <- craftingQueueList.indices) {
            val item = craftingQueueList.get(i).get

            if(item.count <= 0) {
                val opt = craftingQueueList.get(i + 1)
                if(opt.isDefined) {
                    val next = opt.get
                    item.changeID(next.ID)
                    item.setCount(next.count)
                    next.setCount(0)
                }
                else {
                    item.changeID(0)
                    item.setCount(0)
                }
            }
        }
    }

    def cancelItem(index: Int): Unit = {
        if(isShiftDown) {
            val QItem = craftingQueueList.get(index).get
            QItem.subFromCount(10)
            if(QItem.count <= 0) {
                reorder()
            }
        }
        else if(isControlDown) {
            val QItem = craftingQueueList.get(index).get
            QItem.subFromCount(100)
            if(QItem.count <= 0) {
                reorder()
            }
        }
        else {
            val QItem = craftingQueueList.get(index).get
            QItem.subFromCount(1)
            if(QItem.count <= 0) {
                reorder()
            }
        }
    }

    override def act(deltaTime: Float): Unit = {
        super.act(deltaTime)

        isShiftDown = Gdx.input.isKeyPressed(Keys.SHIFT_LEFT)
        isControlDown = Gdx.input.isKeyPressed(Keys.CONTROL_LEFT)
    }

    def show(): Unit = {
        isEnabled = true
        windowQueue.setVisible(isEnabled)
        windowList.setVisible(isEnabled)
        TooltipManager.getInstance().enabled = isEnabled
    }

    def hide(): Unit = {
        isEnabled = false
        windowQueue.setVisible(isEnabled)
        windowList.setVisible(isEnabled)
        TooltipManager.getInstance().enabled = isEnabled
    }

    private def createQueueCell(): InventoryItem = {
        val button = new Button(Assets.ui_skin)
        button.setSize(Config.inventorySlotPixelWidth, Config.inventorySlotPixelHeight)
        //Add Inventory Item To Cell

        val inventoryItem = new InventoryItem(button)
        inventoryItem.cellID = buttonIter
        buttonIter += 1

        button.add(inventoryItem.image).center().row()
        button.add(inventoryItem.numberLabel).bottom().right()
        button.setUserObject(inventoryItem)

        button.addListener(new InputListener() {
            override def touchDown(event: InputEvent, x: Float, y: Float, pointer: Int, button: Int): Boolean = {
                cancelItem(inventoryItem.cellID)
                TooltipManager.getInstance().instant()
                true
            }
        })

        inventoryItem
    }

    private def createListCell(): Button = {
        val button = new Button(Assets.ui_skin)
        button.setSize(Config.inventorySlotPixelWidth, Config.inventorySlotPixelHeight)
        //Add List Item To Cell

        if(IDIter > endID)
            return null

        val listItem = new ListItem(IDIter, button)
        IDIter += 1

        itemList.put(Assets.getNameFromItemID(listItem.getID), listItem)

        //button.add(listItem.nameLabel).top().center().row()
        button.add(listItem.image).center()
        button.setUserObject(listItem)

        button.addListener(listItem.toolTip)
        button.addListener(new InputListener() {
            override def touchDown(event: InputEvent, x: Float, y: Float, pointer: Int, buttonID: Int): Boolean = {
                queueItem(listItem.getID)
                TooltipManager.getInstance().instant()
                true
            }
        })

        button
    }
}
