package com.megabytte.lisg.util

import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.audio.Sound
import com.badlogic.gdx.graphics.{Pixmap, Texture}
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.World
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.utils.ScreenUtils
import com.megabytte.lisg.entities.machines._
import com.megabytte.lisg.inventory.items.Item
import com.megabytte.lisg.world.GameWorld

import scala.collection.mutable

/**
  * Copyright © 2015 Keith Webb
  */
object Assets {
    var world : World = null
    var gameWorld: GameWorld = null
    var spaceMode = true

    //var texMap = new mutable.HashMap[String, Texture] //Could Use Common Name As Identifier

    var emptyInventorySlotTexture : Texture = null
    var genericMachineTexture : Texture = null
    var medicStationTexture : Texture = null
    var foodProcessorTexture : Texture = null
    var generatorTexture : Texture = null
    var oreRefineryTexture : Texture = null
    var turretTexture : Texture = null
    var iceProcessorTexture : Texture = null
    var materialAssemblerTexture : Texture = null
    var distressBeaconTexture : Texture = null

    var gunItemTexture : Texture = null
    var grinderItemTexture : Texture = null                         
    var drillItemTexture : Texture = null                           
    var welderItemTexture : Texture = null                          

    var rockItemTexture : Texture = null
    var oreIceItemTexture : Texture = null
    var oreIronItemTexture : Texture = null
    var oreGoldItemTexture : Texture = null
    var oreUraniumItemTexture : Texture = null
    var oreCobaltItemTexture : Texture = null
    var oreSilverItemTexture : Texture = null
    var oreSiliconItemTexture : Texture = null
    var oreNickelItemTexture : Texture = null
    var oreMagnesiumItemTexture : Texture = null
    var orePlatinumItemTexture : Texture = null

    var gravelItemTexture : Texture = null                          
    var ironIngotItemTexture : Texture = null                       
    var goldIngotItemTexture : Texture = null                       
    var uraniumIngotItemTexture : Texture = null                    
    var cobaltIngotItemTexture : Texture = null                     
    var silverIngotItemTexture : Texture = null                     
    var platinumIngotItemTexture : Texture = null                   
    var nickelIngotItemTexture : Texture = null                     
    var siliconWaferItemTexture : Texture = null                    
    var magnesiumPowderItemTexture : Texture = null                 

    var steelPlateItemTexture : Texture = null                      
    var interiorPlateItemTexture : Texture = null                   
    var constructionComponentItemTexture : Texture = null           
    var metalGridItemTexture : Texture = null                       
    var medicalComponentsItemTexture : Texture = null               
    var reactorComponentsItemTexture : Texture = null               
    var superconductorComponentsItemTexture : Texture = null        
    var computerItemTexture : Texture = null                        
    var radioCommunicationsComponentsItemTexture : Texture = null   
    var largeSteelTubeItemTexture : Texture = null                  
    var smallSteelTubeItemTexture : Texture = null                  
    var displayItemTexture : Texture = null                         
    var motorItemTexture : Texture = null                           

    var spaceMossTexture : Texture = null
    var spaceMeatTexture : Texture = null
    var foodTexture : Texture = null

    var oxygenTankItemTexture : Texture = null
    var waterTankItemTexture : Texture = null

    var playerTexture : Texture = null
    var predatorTexture : Texture = null                            //Need
    var raiderTexture : Texture = null                              //Need

    var bulletTexture : Texture = null                              //Need More Than 1

    var destroySound : Sound = null
    var laserSound : Sound = null
    var bulletHitSound : Sound = null
    var bounceSound : Sound = null
    var jetsSound : Sound = null
    var gameOverSound : Sound = null
    var scaredAnimalSound : Sound = null

    var ui_skin : Skin = null
    var ui_clickSound : Sound = null

    val PIXELS_TO_METERS = 100.0f
    //When translating to Box2D coordinates, divide by this value, when translating back, multiply by it.
    def toPixelsFromMeters(valueInMeters: Float): Float = valueInMeters * PIXELS_TO_METERS
    def toPixelsFromMeters(vectorInMeters: Vector2): Vector2 =
        vectorInMeters.set(vectorInMeters.x * PIXELS_TO_METERS, vectorInMeters.y * PIXELS_TO_METERS)
    def toMetersFromPixels(valueInPixels: Float): Float = valueInPixels / PIXELS_TO_METERS
    def toMetersFromPixels(vectorInPixels: Vector2): Vector2 =
        vectorInPixels.set(vectorInPixels.x / PIXELS_TO_METERS, vectorInPixels.y / PIXELS_TO_METERS)
    def toPfM(valueInMeters: Float): Float = toPixelsFromMeters(valueInMeters)
    def toMfP(valueInPixels: Float): Float = toMetersFromPixels(valueInPixels)

    def determineItemTypeFromID(id: Int): Int = id match {
            case Item.IDs.EMPTY => Item.Types.NoneType

            case Item.IDs.GENERIC_MACHINE => Item.Types.MachineType
            case Item.IDs.MEDIC_STATION => Item.Types.MachineType
            case Item.IDs.TURRET => Item.Types.MachineType
            case Item.IDs.GENERATOR => Item.Types.MachineType
            case Item.IDs.ICE_PROCESSOR => Item.Types.MachineType
            case Item.IDs.ORE_REFINERY => Item.Types.MachineType
            case Item.IDs.MATERIAL_ASSEMBLER => Item.Types.MachineType
            case Item.IDs.FOOD_PROCESSOR => Item.Types.MachineType
            case Item.IDs.DISTRESS_BEACON => Item.Types.MachineType

            case Item.IDs.GUN => Item.Types.UsableType
            case Item.IDs.GRINDER => Item.Types.UsableType
            case Item.IDs.DRILL => Item.Types.UsableType
            case Item.IDs.WELDER => Item.Types.UsableType

            case Item.IDs.ROCK => Item.Types.InertType
            case Item.IDs.ICE => Item.Types.InertType
            case Item.IDs.ORE_IRON => Item.Types.InertType
            case Item.IDs.ORE_GOLD => Item.Types.InertType
            case Item.IDs.ORE_URANIUM => Item.Types.InertType
            case Item.IDs.ORE_COBALT => Item.Types.InertType
            case Item.IDs.ORE_MAGNESIUM => Item.Types.InertType
            case Item.IDs.ORE_NICKEL => Item.Types.InertType
            case Item.IDs.ORE_PLATINUM => Item.Types.InertType
            case Item.IDs.ORE_SILICON => Item.Types.InertType
            case Item.IDs.ORE_SILVER => Item.Types.InertType

            case Item.IDs.IRON_INGOT => Item.Types.InertType
            case Item.IDs.GOLD_INGOT => Item.Types.InertType
            case Item.IDs.COBALT_INGOT => Item.Types.InertType
            case Item.IDs.NICKEL_INGOT => Item.Types.InertType
            case Item.IDs.SILVER_INGOT => Item.Types.InertType
            case Item.IDs.URANIUM_INGOT => Item.Types.InertType
            case Item.IDs.PLATINUM_INGOT => Item.Types.InertType
            case Item.IDs.MAGNESIUM_POWDER => Item.Types.InertType
            case Item.IDs.SILICON_WAFER => Item.Types.InertType
            case Item.IDs.GRAVEL => Item.Types.InertType

            case Item.IDs.STEEL_PLATE => Item.Types.InertType
            case Item.IDs.INTERIOR_PLATE => Item.Types.InertType
            case Item.IDs.CONSTRUCTION_COMPONENT => Item.Types.InertType
            case Item.IDs.METAL_GRID => Item.Types.InertType
            case Item.IDs.MEDICAL_COMPONENTS => Item.Types.InertType
            case Item.IDs.REACTOR_COMPONENTS => Item.Types.InertType
            case Item.IDs.SUPERCONDUCTOR_COMPONENTS => Item.Types.InertType
            case Item.IDs.COMPUTER => Item.Types.InertType
            case Item.IDs.RADIO_COMMUNICATIONS_COMPONENTS => Item.Types.InertType
            case Item.IDs.LARGE_STEEL_TUBE => Item.Types.InertType
            case Item.IDs.SMALL_STEEL_TUBE => Item.Types.InertType
            case Item.IDs.DISPLAY => Item.Types.InertType
            case Item.IDs.MOTOR => Item.Types.InertType

            case Item.IDs.SPACE_MOSS => Item.Types.InertType
            case Item.IDs.SPACE_MEAT => Item.Types.InertType

            case Item.IDs.OXYGEN_TANK => Item.Types.UsableType
            case Item.IDs.WATER_TANK => Item.Types.UsableType
            case Item.IDs.FOOD => Item.Types.UsableType

            case _ => Item.Types.NoneType
    }
    def determineTextureFromItemID(id: Int): Texture = id match {
            case Item.IDs.EMPTY => emptyInventorySlotTexture

            case Item.IDs.GENERIC_MACHINE => genericMachineTexture
            case Item.IDs.MEDIC_STATION => medicStationTexture
            case Item.IDs.TURRET => turretTexture
            case Item.IDs.GENERATOR => generatorTexture
            case Item.IDs.ICE_PROCESSOR => iceProcessorTexture
            case Item.IDs.ORE_REFINERY => oreRefineryTexture
            case Item.IDs.MATERIAL_ASSEMBLER => materialAssemblerTexture
            case Item.IDs.FOOD_PROCESSOR => foodProcessorTexture
            case Item.IDs.DISTRESS_BEACON => distressBeaconTexture

            case Item.IDs.GUN => gunItemTexture
            case Item.IDs.GRINDER => grinderItemTexture
            case Item.IDs.DRILL => drillItemTexture
            case Item.IDs.WELDER => welderItemTexture

            case Item.IDs.ROCK => rockItemTexture
            case Item.IDs.ICE => oreIceItemTexture
            case Item.IDs.ORE_IRON => oreIronItemTexture
            case Item.IDs.ORE_GOLD => oreGoldItemTexture
            case Item.IDs.ORE_URANIUM => oreUraniumItemTexture
            case Item.IDs.ORE_COBALT => oreCobaltItemTexture
            case Item.IDs.ORE_MAGNESIUM => oreMagnesiumItemTexture
            case Item.IDs.ORE_NICKEL => oreNickelItemTexture
            case Item.IDs.ORE_PLATINUM => orePlatinumItemTexture
            case Item.IDs.ORE_SILICON => oreSiliconItemTexture
            case Item.IDs.ORE_SILVER => oreSilverItemTexture

            case Item.IDs.IRON_INGOT => ironIngotItemTexture
            case Item.IDs.GOLD_INGOT => goldIngotItemTexture
            case Item.IDs.COBALT_INGOT => cobaltIngotItemTexture
            case Item.IDs.NICKEL_INGOT => nickelIngotItemTexture
            case Item.IDs.SILVER_INGOT => silverIngotItemTexture
            case Item.IDs.URANIUM_INGOT => uraniumIngotItemTexture
            case Item.IDs.PLATINUM_INGOT => platinumIngotItemTexture
            case Item.IDs.MAGNESIUM_POWDER => magnesiumPowderItemTexture
            case Item.IDs.SILICON_WAFER => siliconWaferItemTexture
            case Item.IDs.GRAVEL => gravelItemTexture

            case Item.IDs.STEEL_PLATE => steelPlateItemTexture
            case Item.IDs.INTERIOR_PLATE => interiorPlateItemTexture
            case Item.IDs.CONSTRUCTION_COMPONENT => constructionComponentItemTexture
            case Item.IDs.METAL_GRID => metalGridItemTexture
            case Item.IDs.MEDICAL_COMPONENTS => medicalComponentsItemTexture
            case Item.IDs.REACTOR_COMPONENTS => reactorComponentsItemTexture
            case Item.IDs.SUPERCONDUCTOR_COMPONENTS => superconductorComponentsItemTexture
            case Item.IDs.COMPUTER => computerItemTexture
            case Item.IDs.RADIO_COMMUNICATIONS_COMPONENTS => radioCommunicationsComponentsItemTexture
            case Item.IDs.LARGE_STEEL_TUBE => largeSteelTubeItemTexture
            case Item.IDs.SMALL_STEEL_TUBE => smallSteelTubeItemTexture
            case Item.IDs.DISPLAY => displayItemTexture
            case Item.IDs.MOTOR => motorItemTexture

            case Item.IDs.WATER_TANK => waterTankItemTexture
            case Item.IDs.OXYGEN_TANK => oxygenTankItemTexture
            case Item.IDs.SPACE_MOSS => spaceMossTexture
            case Item.IDs.SPACE_MEAT => spaceMeatTexture
            case Item.IDs.FOOD => foodTexture

            case _ => emptyInventorySlotTexture
    }
    def newMachineFromItemID(id: Int, stage: Stage, x: Float, y: Float): Machine = id match {
        case Item.IDs.GENERIC_MACHINE => new Machine(stage, x, y)
        case Item.IDs.MEDIC_STATION => new MedicStation(stage, x, y)
        case Item.IDs.TURRET => new Turret(stage, x, y)
        case Item.IDs.GENERATOR => new Generator(stage, x, y)
        case Item.IDs.ICE_PROCESSOR => new IceProcessor(stage, x, y)
        case Item.IDs.ORE_REFINERY => new OreRefinery(stage, x, y)
        case Item.IDs.MATERIAL_ASSEMBLER => new MaterialAssembler(stage, x, y)
        case Item.IDs.FOOD_PROCESSOR => new FoodProcessor(stage, x, y)
        case Item.IDs.DISTRESS_BEACON => new DistressBeacon(stage, x, y)
        case _ => new Machine(stage, x, y)
    }
    def getNameFromItemID(id: Int): String = id match {
        case Item.IDs.MEDIC_STATION => "Medic Station"
        case Item.IDs.TURRET => "Turret"
        case Item.IDs.GENERATOR => "Generator"
        case Item.IDs.ICE_PROCESSOR => "Ice Processor"
        case Item.IDs.ORE_REFINERY => "Ore Refinery"
        case Item.IDs.MATERIAL_ASSEMBLER => "Material Assembler"
        case Item.IDs.FOOD_PROCESSOR => "Food Processor"
        case Item.IDs.DISTRESS_BEACON => "Distress Beacon"

        case Item.IDs.GUN => "Laser Gun"
        case Item.IDs.GRINDER => "Grinder"
        case Item.IDs.DRILL => "Drill"
        case Item.IDs.WELDER => "Welder"

        case Item.IDs.ROCK => "Rock"
        case Item.IDs.ICE => "Ice"
        case Item.IDs.ORE_IRON => "Iron Ore"
        case Item.IDs.ORE_GOLD => "Gold Ore"
        case Item.IDs.ORE_URANIUM => "Uranium Ore"
        case Item.IDs.ORE_COBALT => "Cobalt Ore"
        case Item.IDs.ORE_MAGNESIUM => "Magnesium Ore"
        case Item.IDs.ORE_NICKEL => "Nickel Ore"
        case Item.IDs.ORE_PLATINUM => "Platinum Ore"
        case Item.IDs.ORE_SILICON => "Silicon Ore"
        case Item.IDs.ORE_SILVER => "Silver Ore"

        case Item.IDs.WATER_TANK => "Water Tank"
        case Item.IDs.OXYGEN_TANK => "Oxygen Tank"
        case Item.IDs.SPACE_MOSS => "Moss"
        case Item.IDs.SPACE_MEAT => "Meat"
        case Item.IDs.FOOD => "Food Ration"

        case Item.IDs.IRON_INGOT => "Iron Ingot"
        case Item.IDs.GOLD_INGOT => "Gold Ingot"
        case Item.IDs.COBALT_INGOT => "Cobalt Ingot"
        case Item.IDs.NICKEL_INGOT => "Nickel Ingot"
        case Item.IDs.SILVER_INGOT => "Silver Ingot"
        case Item.IDs.URANIUM_INGOT => "Uranium Ingot"
        case Item.IDs.PLATINUM_INGOT => "Platinum Ingot"
        case Item.IDs.MAGNESIUM_POWDER => "Magnesium Powder"
        case Item.IDs.SILICON_WAFER => "Silicon Wafer"
        case Item.IDs.GRAVEL => "Gravel"

        case Item.IDs.STEEL_PLATE => "Steel Plate"
        case Item.IDs.INTERIOR_PLATE => "Interior Plate"
        case Item.IDs.CONSTRUCTION_COMPONENT => "Construction Component"
        case Item.IDs.METAL_GRID => "Metal Grid"
        case Item.IDs.MEDICAL_COMPONENTS => "Medical Components"
        case Item.IDs.REACTOR_COMPONENTS => "Reactor Components"
        case Item.IDs.SUPERCONDUCTOR_COMPONENTS => "Superconductor Components"
        case Item.IDs.COMPUTER => "Computer"
        case Item.IDs.RADIO_COMMUNICATIONS_COMPONENTS => "Radio Communications Components"
        case Item.IDs.LARGE_STEEL_TUBE => "Large Steel Tube"
        case Item.IDs.SMALL_STEEL_TUBE => "Small Steel Tube"
        case Item.IDs.DISPLAY => "Display"
        case Item.IDs.MOTOR => "Motor"

        case Item.IDs.EMPTY => ""
        case _ => "Unknown"
    }
    def getItemRecipeFromID(id: Int): mutable.HashMap[Int, Int] = {
        //Takes ID, returns Map of Required ID and Count of ID Needed
        val map = new mutable.HashMap[Int, Int]()

        id match {
            case Item.IDs.MEDIC_STATION =>
                map.put(Item.IDs.INTERIOR_PLATE, 240)
                map.put(Item.IDs.CONSTRUCTION_COMPONENT, 80)
                map.put(Item.IDs.MEDICAL_COMPONENTS, 15)
                map.put(Item.IDs.DISPLAY, 10)
                map.put(Item.IDs.LARGE_STEEL_TUBE, 5)
                map.put(Item.IDs.SMALL_STEEL_TUBE, 20)
                map.put(Item.IDs.METAL_GRID, 60)
            case Item.IDs.TURRET =>
                map.put(Item.IDs.STEEL_PLATE, 20)
                map.put(Item.IDs.CONSTRUCTION_COMPONENT, 30)
                map.put(Item.IDs.COMPUTER, 10)
                map.put(Item.IDs.MOTOR, 8)
                map.put(Item.IDs.LARGE_STEEL_TUBE, 1)
                map.put(Item.IDs.METAL_GRID, 15)
            case Item.IDs.GENERATOR =>
                map.put(Item.IDs.STEEL_PLATE, 1000)
                map.put(Item.IDs.CONSTRUCTION_COMPONENT, 70)
                map.put(Item.IDs.METAL_GRID, 40)
                map.put(Item.IDs.REACTOR_COMPONENTS, 2000)
                map.put(Item.IDs.SUPERCONDUCTOR_COMPONENTS, 100)
                map.put(Item.IDs.LARGE_STEEL_TUBE, 40)
                map.put(Item.IDs.COMPUTER, 75)
                map.put(Item.IDs.MOTOR, 20)
            case Item.IDs.ICE_PROCESSOR =>
                map.put(Item.IDs.STEEL_PLATE, 120)
                map.put(Item.IDs.CONSTRUCTION_COMPONENT, 5)
                map.put(Item.IDs.COMPUTER, 5)
                map.put(Item.IDs.MOTOR, 4)
                map.put(Item.IDs.LARGE_STEEL_TUBE, 2)
            case Item.IDs.ORE_REFINERY =>
                map.put(Item.IDs.STEEL_PLATE, 1200)
                map.put(Item.IDs.CONSTRUCTION_COMPONENT, 40)
                map.put(Item.IDs.COMPUTER, 20)
                map.put(Item.IDs.MOTOR, 16)
                map.put(Item.IDs.LARGE_STEEL_TUBE, 20)
            case Item.IDs.MATERIAL_ASSEMBLER =>
                map.put(Item.IDs.STEEL_PLATE, 150)
                map.put(Item.IDs.CONSTRUCTION_COMPONENT, 40)
                map.put(Item.IDs.COMPUTER, 80)
                map.put(Item.IDs.DISPLAY, 4)
                map.put(Item.IDs.MOTOR, 8)
            case Item.IDs.FOOD_PROCESSOR =>
                map.put(Item.IDs.STEEL_PLATE, 120)
                map.put(Item.IDs.CONSTRUCTION_COMPONENT, 5)
                map.put(Item.IDs.COMPUTER, 5)
                map.put(Item.IDs.MOTOR, 4)
                map.put(Item.IDs.LARGE_STEEL_TUBE, 2)
            case Item.IDs.DISTRESS_BEACON =>
                map.put(Item.IDs.STEEL_PLATE, 80)
                map.put(Item.IDs.CONSTRUCTION_COMPONENT, 40)
                map.put(Item.IDs.RADIO_COMMUNICATIONS_COMPONENTS, 40)
                map.put(Item.IDs.COMPUTER, 8)
                map.put(Item.IDs.LARGE_STEEL_TUBE, 40)
                map.put(Item.IDs.SMALL_STEEL_TUBE, 60)
            case Item.IDs.GUN =>
                map.put(Item.IDs.IRON_INGOT, 3)
                map.put(Item.IDs.NICKEL_INGOT, 1)
            case Item.IDs.STEEL_PLATE =>
                map.put(Item.IDs.IRON_INGOT, 21)
            case Item.IDs.INTERIOR_PLATE =>
                map.put(Item.IDs.IRON_INGOT, 4)
            case Item.IDs.CONSTRUCTION_COMPONENT =>
                map.put(Item.IDs.IRON_INGOT, 10)
            case Item.IDs.METAL_GRID =>
                map.put(Item.IDs.IRON_INGOT, 12)
                map.put(Item.IDs.NICKEL_INGOT, 5)
                map.put(Item.IDs.COBALT_INGOT, 3)
            case Item.IDs.MEDICAL_COMPONENTS =>
                map.put(Item.IDs.IRON_INGOT, 60)
                map.put(Item.IDs.NICKEL_INGOT, 70)
                map.put(Item.IDs.SILVER_INGOT, 20)
            case Item.IDs.REACTOR_COMPONENTS =>
                map.put(Item.IDs.IRON_INGOT, 15)
                map.put(Item.IDs.GRAVEL, 20)
                map.put(Item.IDs.SILVER_INGOT, 5)
            case Item.IDs.SUPERCONDUCTOR_COMPONENTS =>
                map.put(Item.IDs.IRON_INGOT, 10)
                map.put(Item.IDs.GOLD_INGOT, 5)
            case Item.IDs.COMPUTER =>
                map.put(Item.IDs.SILICON_WAFER, 1)
                map.put(Item.IDs.IRON_INGOT, 1)
            case Item.IDs.RADIO_COMMUNICATIONS_COMPONENTS =>
                map.put(Item.IDs.IRON_INGOT, 8)
                map.put(Item.IDs.SILICON_WAFER, 1)
            case Item.IDs.LARGE_STEEL_TUBE =>
                map.put(Item.IDs.IRON_INGOT, 30)
            case Item.IDs.SMALL_STEEL_TUBE =>
                map.put(Item.IDs.IRON_INGOT, 5)
            case Item.IDs.DISPLAY =>
                map.put(Item.IDs.IRON_INGOT, 1)
                map.put(Item.IDs.SILICON_WAFER, 5)
            case Item.IDs.MOTOR =>
                map.put(Item.IDs.IRON_INGOT, 20)
                map.put(Item.IDs.NICKEL_INGOT, 5)
            case _ =>
        }

        map
    }
    def getAssemblerTime(id: Int): Float = id match {
        case Item.IDs.MEDIC_STATION => 4.0f
        case Item.IDs.TURRET => 4.0f
        case Item.IDs.GENERATOR => 4.0f
        case Item.IDs.ICE_PROCESSOR => 4.0f
        case Item.IDs.ORE_REFINERY => 4.0f
        case Item.IDs.MATERIAL_ASSEMBLER => 4.0f
        case Item.IDs.FOOD_PROCESSOR => 4.0f
        case Item.IDs.DISTRESS_BEACON => 4.0f
        case Item.IDs.GUN => 4.0f
        case Item.IDs.WATER_TANK => 4.0f
        case Item.IDs.OXYGEN_TANK => 4.0f
        case Item.IDs.SPACE_MOSS => 4.0f
        case Item.IDs.SPACE_MEAT => 4.0f
        case Item.IDs.FOOD => 4.0f

        case Item.IDs.IRON_INGOT => 4.0f
        case Item.IDs.GOLD_INGOT => 4.0f
        case Item.IDs.COBALT_INGOT => 4.0f
        case Item.IDs.NICKEL_INGOT => 4.0f
        case Item.IDs.SILVER_INGOT => 4.0f
        case Item.IDs.URANIUM_INGOT => 4.0f
        case Item.IDs.PLATINUM_INGOT => 4.0f
        case Item.IDs.MAGNESIUM_POWDER => 4.0f
        case Item.IDs.SILICON_WAFER => 4.0f
        case Item.IDs.GRAVEL => 4.0f

        case Item.IDs.STEEL_PLATE => 4.0f
        case Item.IDs.INTERIOR_PLATE => 4.0f
        case Item.IDs.CONSTRUCTION_COMPONENT => 4.0f
        case Item.IDs.METAL_GRID => 4.0f
        case Item.IDs.MEDICAL_COMPONENTS => 4.0f
        case Item.IDs.REACTOR_COMPONENTS => 4.0f
        case Item.IDs.SUPERCONDUCTOR_COMPONENTS => 4.0f
        case Item.IDs.COMPUTER => 4.0f
        case Item.IDs.RADIO_COMMUNICATIONS_COMPONENTS => 4.0f
        case Item.IDs.LARGE_STEEL_TUBE => 4.0f
        case Item.IDs.SMALL_STEEL_TUBE => 4.0f
        case Item.IDs.DISPLAY => 4.0f
        case Item.IDs.MOTOR => 4.0f
        case _ => 4.0f
    }

    def getScreenShot(x: Int, y: Int, w: Int, h: Int, yDown: Boolean) : Pixmap = {
        val pixmap = ScreenUtils.getFrameBufferPixmap(x, y, w, h)

        if(yDown) {
            // Flip the pixmap upside down
            val pixels = pixmap.getPixels
            val numBytes = w * h * 4
            val lines = new Array[Byte](numBytes)
            val numBytesPerLine = w * 4
            for(i <- 0 until h) {
                pixels.position((h - i - 1) * numBytesPerLine)
                pixels.get(lines, i * numBytesPerLine, numBytesPerLine)
            }
            pixels.clear()
            pixels.put(lines)
            pixels.clear()
        }

        pixmap
    }

    def setup(assetManager: AssetManager, lGameWorld: GameWorld): Unit = {
        world = lGameWorld.box2DWorld
        gameWorld = lGameWorld

        emptyInventorySlotTexture = new Texture(Config.inventorySlotPixelWidth, Config.inventorySlotPixelHeight, Pixmap.Format.RGBA8888)

        genericMachineTexture = assetManager.get("textures/machines/machine.png", classOf[Texture])
        medicStationTexture = assetManager.get("textures/machines/medicStation.png")
        turretTexture = assetManager.get("textures/machines/turret.png", classOf[Texture])
        iceProcessorTexture = assetManager.get("textures/machines/iceProcessor.png", classOf[Texture])
        foodProcessorTexture = assetManager.get("textures/machines/foodProcessor.png", classOf[Texture])
        oreRefineryTexture = assetManager.get("textures/machines/oreRefinery.png", classOf[Texture])
        materialAssemblerTexture = assetManager.get("textures/machines/materialAssembler.png", classOf[Texture])
        generatorTexture = assetManager.get("textures/machines/generator.png", classOf[Texture])
        distressBeaconTexture = assetManager.get("textures/machines/distressBeacon.png", classOf[Texture])

        bulletTexture = assetManager.get("textures/bullet.png", classOf[Texture])
        playerTexture = assetManager.get("textures/player.png", classOf[Texture])
        rockItemTexture = assetManager.get("textures/items/ores/Rock.png", classOf[Texture])
        oreIceItemTexture = assetManager.get("textures/items/ores/RockIce.png", classOf[Texture])
        oreIronItemTexture = assetManager.get("textures/items/ores/OreIron.png", classOf[Texture])
        oreGoldItemTexture = assetManager.get("textures/items/ores/OreGold.png", classOf[Texture])
        oreUraniumItemTexture = assetManager.get("textures/items/ores/OreUranium.png", classOf[Texture])
        oreNickelItemTexture = assetManager.get("textures/items/ores/OreNickel.png", classOf[Texture])
        orePlatinumItemTexture = assetManager.get("textures/items/ores/OrePlatinum.png", classOf[Texture])
        oreCobaltItemTexture = assetManager.get("textures/items/ores/OreCobalt.png", classOf[Texture])
        oreSilverItemTexture = assetManager.get("textures/items/ores/OreSilver.png", classOf[Texture])
        oreSiliconItemTexture = assetManager.get("textures/items/ores/OreSilicon.png", classOf[Texture])
        oreMagnesiumItemTexture = assetManager.get("textures/items/ores/OreMagnesium.png", classOf[Texture])

        gunItemTexture = assetManager.get("textures/items/tools/LaserGun.png", classOf[Texture])
        drillItemTexture = assetManager.get("textures/items/tools/Drill.png", classOf[Texture])
        grinderItemTexture = assetManager.get("textures/items/tools/Grinder.png", classOf[Texture])
        welderItemTexture = assetManager.get("textures/items/tools/Welder.png", classOf[Texture])

        foodTexture = assetManager.get("textures/items/consumable/food.png", classOf[Texture])
        oxygenTankItemTexture = assetManager.get("textures/items/consumable/oxygenTank.png", classOf[Texture])
        waterTankItemTexture = assetManager.get("textures/items/consumable/waterTank.png", classOf[Texture])

        cobaltIngotItemTexture = assetManager.get("textures/items/metals/CobaltIngot.png", classOf[Texture])
        goldIngotItemTexture = assetManager.get("textures/items/metals/GoldIngot.png", classOf[Texture])
        ironIngotItemTexture = assetManager.get("textures/items/metals/IronIngot.png", classOf[Texture])
        gravelItemTexture = assetManager.get("textures/items/metals/Gravel.png", classOf[Texture])
        magnesiumPowderItemTexture = assetManager.get("textures/items/metals/MagnesiumPowder.png", classOf[Texture])
        nickelIngotItemTexture = assetManager.get("textures/items/metals/NickelIngot.png", classOf[Texture])
        platinumIngotItemTexture = assetManager.get("textures/items/metals/PlatinumIngot.png", classOf[Texture])
        silverIngotItemTexture = assetManager.get("textures/items/metals/SilverIngot.png", classOf[Texture])
        uraniumIngotItemTexture = assetManager.get("textures/items/metals/UraniumIngot.png", classOf[Texture])

        spaceMossTexture = assetManager.get("textures/items/components/Moss.png", classOf[Texture])
        spaceMeatTexture = assetManager.get("textures/items/components/Meat.png", classOf[Texture])
        computerItemTexture = assetManager.get("textures/items/components/Computer.png", classOf[Texture])
        constructionComponentItemTexture = assetManager.get("textures/items/components/ConstructionComponents.png", classOf[Texture])
        displayItemTexture = assetManager.get("textures/items/components/Display.png", classOf[Texture])
        interiorPlateItemTexture = assetManager.get("textures/items/components/InteriorPlate.png", classOf[Texture])
        largeSteelTubeItemTexture = assetManager.get("textures/items/components/LargeSteelTube.png", classOf[Texture])
        smallSteelTubeItemTexture = assetManager.get("textures/items/components/SmallSteelTube.png", classOf[Texture])
        medicalComponentsItemTexture = assetManager.get("textures/items/components/MedicalComponents.png", classOf[Texture])
        metalGridItemTexture = assetManager.get("textures/items/components/MetalGrid.png", classOf[Texture])
        motorItemTexture = assetManager.get("textures/items/components/Motor.png", classOf[Texture])
        radioCommunicationsComponentsItemTexture = assetManager.get("textures/items/components/RadioCommunicationsComponents.png", classOf[Texture])
        reactorComponentsItemTexture = assetManager.get("textures/items/components/ReactorComponents.png", classOf[Texture])
        siliconWaferItemTexture = assetManager.get("textures/items/components/SiliconWafer.png", classOf[Texture])
        steelPlateItemTexture = assetManager.get("textures/items/components/SteelPlate.png", classOf[Texture])
        superconductorComponentsItemTexture = assetManager.get("textures/items/components/SuperconductorComponents.png", classOf[Texture])

        bulletHitSound = assetManager.get("audio/bullet_hit.wav", classOf[Sound])
        destroySound = assetManager.get("audio/destroy.wav", classOf[Sound])
        laserSound = assetManager.get("audio/laser.wav", classOf[Sound])
        bounceSound = assetManager.get("audio/bounce.wav", classOf[Sound])
        gameOverSound = assetManager.get("audio/gameOver.wav", classOf[Sound])
        jetsSound = assetManager.get("audio/jets.wav", classOf[Sound])
        scaredAnimalSound = assetManager.get("audio/scaredAnimal.wav", classOf[Sound])
    }
}