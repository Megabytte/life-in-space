package com.megabytte.lisg.util

import java.io.{BufferedReader, File, FileReader, FileWriter}
import java.nio.file.{Files, Paths}

import com.badlogic.gdx.maps.tiled.TmxMapLoader
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType
import com.badlogic.gdx.physics.box2d.{Body, BodyDef}
import com.badlogic.gdx.scenes.scene2d.Stage
import com.megabytte.lisg.entities.hostiles.Hostile
import com.megabytte.lisg.entities.items.{Bullet, GenericItem, Gun}
import com.megabytte.lisg.entities.machines._
import com.megabytte.lisg.inventory.items.Item
import com.megabytte.lisg.states.MainGameState
import com.megabytte.lisg.world.MapChunk
import net.dermetfan.gdx.maps.tiled.TmxMapWriter
import net.dermetfan.gdx.maps.tiled.TmxMapWriter.Format
import org.json.JSONObject

import scala.collection.mutable

/**
  * Copyright © 2015 Keith Webb
  */
object GameFileUtil {
    var prefix = "saves/"

    def serializeBody(body: Body): JSONObject = {
        val json = new JSONObject()

        json.put("bodyType", body.getType.getValue)
        json.put("positionX", body.getPosition.x)
        json.put("positionY", body.getPosition.y)
        json.put("angle", body.getAngle)
        json.put("velocityX", body.getLinearVelocity.x)
        json.put("velocityY", body.getLinearVelocity.y)
        json.put("linearDampening", body.getLinearDamping)
        json.put("angularDampening", body.getAngularDamping)
        json.put("isBullet", body.isBullet)
        json.put("isFixedRotation", body.isFixedRotation)
        json.put("angularVelocity", body.getAngularVelocity)

        json
    }

    def deserializeBody(json: JSONObject): Body = {
        val bodyType = json.get("bodyType").asInstanceOf[Int]
        val positionX = json.getDouble("positionX")
        val positionY = json.getDouble("positionY")
        val angle = json.getDouble("angle")
        val velocityX = json.getDouble("velocityX")
        val velocityY = json.getDouble("velocityY")
        val linearDampening = json.getDouble("linearDampening")
        val angularDampening = json.getDouble("angularDampening")
        val angularVelocity = json.getDouble("angularVelocity")
        val isBullet = json.get("isBullet").asInstanceOf[Boolean]
        val isFixedRotation = json.get("isFixedRotation").asInstanceOf[Boolean]

        val body = Assets.world.createBody(new BodyDef)

        body.setType(bodyType match {
            case 0 => BodyType.StaticBody
            case 1 => BodyType.KinematicBody
            case 2 => BodyType.DynamicBody
            case _ => BodyType.StaticBody
        })
        body.setFixedRotation(isFixedRotation)
        body.setLinearDamping(linearDampening.toFloat)
        body.setTransform(positionX.toFloat, positionY.toFloat, angle.toFloat)
        body.setLinearVelocity(velocityX.toFloat, velocityY.toFloat)
        body.setBullet(isBullet)
        body.setAngularDamping(angularDampening.toFloat)
        body.setAngularVelocity(angularVelocity.toFloat)

        body
    }

    def copyFields(body1: Body, body2: Body): Unit = {
        body2.setAngularDamping(body1.getAngularDamping)
        body2.setAngularVelocity(body1.getAngularVelocity)
        body2.setTransform(body1.getPosition, body1.getAngle)
        body2.setLinearDamping(body1.getLinearDamping)
        body2.setLinearVelocity(body1.getLinearVelocity)
        body2.setBullet(body1.isBullet)
        body2.setFixedRotation(body1.isFixedRotation)
    }

    def serializeMachines(machines: mutable.HashSet[Machine]): JSONObject = {
        val json = new JSONObject()

        var iterator = 0
        var id = "machine" + iterator.toString
        for(machine: Machine <- machines) {
            json.put(id, 0)
            json.put(id + "id", machine.id)
            json.put(id + "body", serializeBody(machine.body))

            if(machine.hasGUI) {
                json.put(id + "inventory", machine.inventory.serialize())
            }

            machine match {
                case _: DistressBeacon =>
                    val specificMachine = machine.asInstanceOf[DistressBeacon]
                    json.put(id + "timeAlive", specificMachine.timeAlive)
                case _: FoodProcessor =>
                    val specificMachine = machine.asInstanceOf[FoodProcessor]
                    json.put(id + "processTime", specificMachine.processTime)
                case _: IceProcessor =>
                    val specificMachine = machine.asInstanceOf[IceProcessor]
                    json.put(id + "processTime", specificMachine.processTime)
                case _: MaterialAssembler =>
                    val specificMachine = machine.asInstanceOf[MaterialAssembler]
                    json.put(id + "processTime", specificMachine.processTime)
                    json.put(id + "craftingGUI", specificMachine.craftingGUI.serialize())
                case _: OreRefinery =>
                    val specificMachine = machine.asInstanceOf[OreRefinery]
                    json.put(id + "processTime", specificMachine.processTime)
                case _: Turret =>
                    val specificMachine = machine.asInstanceOf[Turret]
                    json.put(id + "friendly", specificMachine.friendly)
                    json.put(id + "gun", serializeGun(specificMachine.gun))
                case _: MedicStation =>
                case _: Generator =>
                case _ =>
            }
            iterator += 1
            id = "machine" + iterator.toString
        }

        json
    }

    def deserializeMachines(stage: Stage, mapChunk: MapChunk, json: JSONObject): Unit = {
        var iterator = 0
        var id = "machine" + iterator.toString
        while(json.has(id)) {
            val machineID = json.get(id + "id").asInstanceOf[Int]

            val machine = mapChunk.addMachine(machineID, stage, 0, 0)

            val body = deserializeBody(json.getJSONObject(id + "body"))
            copyFields(body, machine.body)

            if(machine.hasGUI) {
                machine.inventory.deserialize(json.getJSONObject(id + "inventory"))
            }

            machineID match {
                case Item.IDs.DISTRESS_BEACON =>
                    val specificMachine = machine.asInstanceOf[DistressBeacon]
                    val timeAlive = json.getLong(id + "timeAlive")

                    specificMachine.timeAlive = timeAlive
                case Item.IDs.FOOD_PROCESSOR =>
                    val specificMachine = machine.asInstanceOf[FoodProcessor]
                    val processTime = json.getLong(id + "processTime")

                    specificMachine.processTime = processTime
                case Item.IDs.ICE_PROCESSOR =>
                    val specificMachine = machine.asInstanceOf[IceProcessor]
                    val processTime = json.getLong(id + "processTime")

                    specificMachine.processTime = processTime
                case Item.IDs.MATERIAL_ASSEMBLER =>
                    val specificMachine = machine.asInstanceOf[MaterialAssembler]
                    val processTime = json.getLong(id + "processTime")

                    specificMachine.processTime = processTime
                    specificMachine.craftingGUI.deserialize(json.getJSONObject(id + "craftingGUI"))
                case Item.IDs.ORE_REFINERY =>
                    val specificMachine = machine.asInstanceOf[OreRefinery]
                    val processTime = json.getLong(id + "processTime")

                    specificMachine.processTime = processTime
                case Item.IDs.TURRET =>
                    val specificMachine = machine.asInstanceOf[Turret]

                    val friendly = json.getBoolean(id + "friendly")
                    specificMachine.friendly = friendly
                    deserializeGun(specificMachine.gun, json.getJSONObject(id + "gun"))
                case Item.IDs.MEDIC_STATION =>
                case Item.IDs.GENERATOR =>
                case _ =>
            }

            iterator += 1
            id = "machine" + iterator.toString
        }
    }

    def serializeGun(gun: Gun): JSONObject = {
        val json = new JSONObject()

        var iterator = 0
        for(bullet: Bullet <- gun.bullets) {
            val id = "bullet" + iterator.toString
            json.put(id, 0)
            json.put(id + "body", serializeBody(bullet.body))
            json.put(id + "timeAlive", bullet.timeAlive)
            json.put(id + "friendly", bullet.friendly)
            iterator += 1
        }

        json
    }

    def deserializeGun(gun: Gun, json: JSONObject): Unit = {
        val dummy = new Vector2()

        var iterator = 0
        var id = "bullet" + iterator.toString
        while(json.has(id)) {
            val body = deserializeBody(json.getJSONObject(id + "body"))
            val timeAlive = json.getLong(id + "timeAlive")
            val friendly = json.getBoolean(id + "friendly")

            val bullet = new Bullet(0, dummy, dummy, friendly)
            copyFields(body, bullet.body)
            bullet.timeAlive = timeAlive

            gun.bullets += bullet

            iterator += 1
            id = "bullet" + iterator.toString
        }
    }

    def serializeHostiles(hostiles: mutable.HashSet[Hostile]): JSONObject = {
        val json = new JSONObject()

        var iterator = 0
        for(hostile: Hostile <- hostiles) {
            val id = "hostile" + iterator.toString
            json.put(id, 0)
            json.put(id + "body", serializeBody(hostile.body))
            json.put(id + "processTime", hostile.processTime)
            json.put(id + "currentState", hostile.currentState)
            json.put(id + "friendly", hostile.friendly)
            json.put(id + "directionX", hostile.direction.x)
            json.put(id + "directionY", hostile.direction.y)
            json.put(id + "health", hostile.health)

            iterator += 1
        }

        json
    }

    def deserializeHostiles(mapChunk: MapChunk, json: JSONObject): Unit = {
        var iterator = 0
        var id = "hostile" + iterator.toString
        while(json.has(id)) {

            val hostile = mapChunk.addHostile(0, 0)
            val body = deserializeBody(json.getJSONObject(id + "body"))
            copyFields(body, hostile.body)

            val processTime = json.getLong(id + "processTime")
            val currentState = json.getInt(id + "currentState")
            val friendly = json.getBoolean(id + "friendly")
            val directionX = json.getDouble(id + "directionX").toFloat
            val directionY = json.getDouble(id + "directionY").toFloat
            val health = json.getDouble(id + "health").toFloat

            hostile.health = health
            hostile.processTime = processTime
            hostile.currentState = currentState
            hostile.friendly = friendly
            hostile.direction.set(directionX, directionY)

            iterator += 1
            id = "hostile" + iterator.toString
        }
    }

    def serializeItems(items: mutable.HashSet[GenericItem]): JSONObject = {
        val json = new JSONObject()

        var iterator = 0
        var id = "item" + iterator.toString
        for(item: GenericItem <- items) {
            json.put(id, 0)
            json.put(id + "ID", item.ID)
            json.put(id + "count", item.count)
            json.put(id + "body", serializeBody(item.body))

            iterator += 1
            id = "item" + iterator.toString
        }

        json
    }

    def deserializeItems(mapChunk: MapChunk, json: JSONObject): Unit = {
        var iterator = 0
        var id = "item" + iterator.toString
        while(json.has(id)) {
            val itemID = json.getInt(id + "ID")
            val itemCount = json.getInt(id + "count")

            val item = mapChunk.addItem(0, 0, itemID, itemCount)
            val body = deserializeBody(json.getJSONObject(id + "body"))
            copyFields(body, item.body)

            iterator += 1
            id = "item" + iterator.toString
        }
    }

    /**
      * Sets up serializers
      */
    def setup(): Unit = {
        createDirectory("saves")
    }

    /**
      * Creates a new save game name when none is provided.
      * @return An untaken folder name.
      */
    def createName(): String = {
        var attempt = 1
        while(Files.isDirectory(Paths.get(prefix + "save" + attempt.toString))) {
            attempt += 1
        }
        "save" + attempt.toString
    }

    /**
      * Creates a new directory.
      * @param name The name of the directory to make.
      * @return Returns whether or not this was successful.
      */
    def createDirectory(name: String): Boolean = {
        val dir = new File(name)
        dir.mkdir()
    }

    /**
      * Saves The MainGameState to a new directory. Two files are present. A TMX map file and a JSON file for entities.
      * @param name The Name of the Directory / Save to create.
      * @param mainGameState A reference to the Main Game State, where all relevant info is stored.
      */
    def saveGame(name: String, mainGameState: MainGameState): Unit = {
        if(!createDirectory(prefix + name))
            return

        var mapWriter = new TmxMapWriter(new FileWriter(prefix + name + "/map.tmx"))
        val spaceSolidLayer = mainGameState.gameWorld.spaceMap.map.getLayers.get("Solid")
        mainGameState.gameWorld.spaceMap.map.getLayers.remove(mainGameState.gameWorld.spaceMap.map.getLayers.getIndex("Solid"))
        mapWriter.tmx(mainGameState.gameWorld.spaceMap.map, Format.Base64Zlib)
        mainGameState.gameWorld.spaceMap.map.getLayers.add(spaceSolidLayer)
        mapWriter.close()

        //Map Data
        var iter = 0
        val mapJSON = new JSONObject()
        for((x: Int, y: Int) <- mainGameState.gameWorld.asteroidMaps.keys) {
            val theMapName = mainGameState.gameWorld.asteroidMaps.get((x, y)).get.name
            mapJSON.put(iter.toString, theMapName)
            mapWriter = new TmxMapWriter(new FileWriter(prefix + name + "/" + theMapName + ".tmx"))
            val map = mainGameState.gameWorld.asteroidMaps.get((x, y)).get.map
            val solidLayer = map.getLayers.get("Solid")

            map.getLayers.remove(map.getLayers.getIndex("Solid"))
            mapWriter.tmx(map, Format.Base64Zlib)
            map.getLayers.add(solidLayer)

            mapWriter.close()
            iter += 1
        }

        val fileWriter = new FileWriter(prefix + name + "/info.json")

        //handle player
        val player = mainGameState.player
        val playerJSON = new JSONObject()

        playerJSON.put("name", "Player")

        //Stats
        playerJSON.put("health", player.health)
        playerJSON.put("oxygen", player.oxygen)
        playerJSON.put("hunger", player.hunger)
        playerJSON.put("thirst", player.thirst)

        //Inventory and Hotbar
        playerJSON.put("inventory", player.inventory.serialize())
        playerJSON.put("hotbar", player.itemHotBar.serialize())

        //Physics
        playerJSON.put("body", serializeBody(player.body))

        //Gun
        playerJSON.put("gun", serializeGun(player.gun))

        //Location
        playerJSON.put("location", player.location)
        playerJSON.put("playerSpacePosX", mainGameState.gameWorld.playerBKPos.x)
        playerJSON.put("playerSpacePosY", mainGameState.gameWorld.playerBKPos.y)

        val saveJSON = new JSONObject()

        //Player
        saveJSON.put("Player", playerJSON)

        for(mapChunk: MapChunk <- mainGameState.gameWorld.asteroidMaps.values) {
            //Hostiles
            saveJSON.put(mapChunk.name + " hostiles", serializeHostiles(mapChunk.hostiles))
            //Machines
            saveJSON.put(mapChunk.name + " machines", serializeMachines(mapChunk.machines))
            //Items
            saveJSON.put(mapChunk.name + " items", serializeItems(mapChunk.items))
        }

        //Maps
        saveJSON.put("maps", mapJSON)

        fileWriter.write(saveJSON.toString)

        fileWriter.close()
    }

    /**
      * Loads the game. Creates a main game state from file info.
      * @param name The Name of the Directory / Save to load.
      * @return A reference to a Main Game State, where all relevant info is stored.
      */
    def loadGame(name: String): MainGameState = {
        val path = prefix + name + "/map.tmx"
        val mainGameState = new MainGameState(path)

        val fileReader = new BufferedReader(new FileReader(prefix + name + "/info.json"))

        val player = mainGameState.player
        val saveJSON = new JSONObject(fileReader.readLine())
        val playerJSON = saveJSON.getJSONObject("Player")
        val mapJSON = saveJSON.getJSONObject("maps")

        //Stats
        player.health = playerJSON.getDouble("health").toFloat
        player.oxygen = playerJSON.getDouble("oxygen").toFloat
        player.hunger = playerJSON.getDouble("hunger").toFloat
        player.thirst = playerJSON.getDouble("thirst").toFloat
        player.location = playerJSON.getString("location")

        val playerBKX = playerJSON.getDouble("playerSpacePosX").toFloat
        val playerBKY = playerJSON.getDouble("playerSpacePosY").toFloat
        mainGameState.gameWorld.playerBKPos.set(playerBKX, playerBKY)

        //Inventory and Hotbar
        player.inventory.deserialize(playerJSON.getJSONObject("inventory"))
        player.itemHotBar.deserialize(playerJSON.getJSONObject("hotbar"))

        //Physics
        val body = deserializeBody(playerJSON.getJSONObject("body"))
        copyFields(body, player.body)

        //Gun
        deserializeGun(player.gun, playerJSON.getJSONObject("gun"))

        val mapIter = mapJSON.keys()
        while(mapIter.hasNext) {
            val key = mapIter.next()
            var mapID = mapJSON.get(key.asInstanceOf[String]).asInstanceOf[String]
            mapID = mapID.replace("Asteroid", "")
            mapID = mapID.replace("(", "")
            mapID = mapID.replace(")", "")
            mapID = mapID.replace(" ", "")

            val pieces = mapID.split(",")

            val x = pieces(0).toInt
            val y = pieces(1).toInt

            val mapName = "Asteroid (" + x + ", " + y + ")"
            val map = new TmxMapLoader().load(prefix + name + "/" + mapName + ".tmx")

            Config.addSolidObjectsToMap(map,
                mainGameState.gameWorld.IDToNameMap,
                mainGameState.gameWorld.worldGenParameters.tileSize)

            Config.addWallsToMap(map.getLayers.get("Solid"),
                mainGameState.gameWorld.worldGenParameters.chunkWidth,
                mainGameState.gameWorld.worldGenParameters.chunkHeight,
                mainGameState.gameWorld.worldGenParameters.tileSize)

            val mapChunk = new MapChunk(map, mapName)
            mainGameState.gameWorld.asteroidMaps.put((x, y), mapChunk)
            Assets.world = mapChunk.box2DWorld

            if(player.location == mapName) {
                mainGameState.gameWorld.currentMap = mapChunk
                mainGameState.gameWorld.box2DWorld = mapChunk.box2DWorld
                mapChunk.initializePhysics()
                mapChunk.setupPlayer(player, 0, 0, usePos = false)
                Assets.spaceMode = false
                mainGameState.gameWorld.spaceMode = false
            }

            //Hostiles
            deserializeHostiles(mapChunk, saveJSON.getJSONObject(mapName + " hostiles"))
            //Machines
            deserializeMachines(mainGameState.stage, mapChunk, saveJSON.getJSONObject(mapName + " machines"))
            //Machines
            deserializeItems(mapChunk, saveJSON.getJSONObject(mapName + " items"))
        }

        mainGameState
    }
}