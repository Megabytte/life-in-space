package com.megabytte.lisg.util

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.maps.tiled.TiledMap

/**
  * Copyright © 2015 Keith Webb
  */
class CameraConstrainer(camera: OrthographicCamera, map: TiledMap) {

    val mapWidth_InTiles = map.getProperties.get("width").asInstanceOf[Int]
    val mapHeight_InTiles = map.getProperties.get("height").asInstanceOf[Int]
    val tileWidth_InPixels = map.getProperties.get("tilewidth").asInstanceOf[Int]
    val tileHeight_InPixels = map.getProperties.get("tileheight").asInstanceOf[Int]

    val mapWidth_InPixels = mapWidth_InTiles * tileWidth_InPixels
    val mapHeight_InPixels = mapHeight_InTiles * tileHeight_InPixels
    var screen_width = Gdx.graphics.getWidth
    var screen_height = Gdx.graphics.getHeight
    var lowerLimitX = screen_width / 2.0f
    var lowerLimitY = screen_height / 2.0f
    var upperLimitX = mapWidth_InPixels - lowerLimitX
    var upperLimitY = mapHeight_InPixels - lowerLimitY

    def update(): Unit = {
        val cameraPosition = camera.position

        if(cameraPosition.x < lowerLimitX)
            cameraPosition.x = lowerLimitX
        if(cameraPosition.y < lowerLimitY)
            cameraPosition.y = lowerLimitY
        if(cameraPosition.x > upperLimitX)
            cameraPosition.x = upperLimitX
        if(cameraPosition.y > upperLimitY)
            cameraPosition.y = upperLimitY

        camera.position.set(cameraPosition)
    }

    def resize(width: Int, height: Int): Unit = {
        screen_width = width
        screen_height = height
        lowerLimitX = screen_width / 2.0f
        lowerLimitY = screen_height / 2.0f
        upperLimitX = mapWidth_InPixels - lowerLimitX
        upperLimitY = mapHeight_InPixels - lowerLimitY
    }

}
