package com.megabytte.lisg.util

import com.megabytte.lisg.entities.Entity
import com.megabytte.lisg.entities.hostiles.Hostile
import com.megabytte.lisg.entities.items.{GenericItem, Bullet}
import com.megabytte.lisg.entities.machines.Machine
import com.megabytte.lisg.entities.player.Player
import com.megabytte.lisg.entities.util.{AreaSensor, Mouse}
import com.megabytte.lisg.world.blocks.{Floor, Asteroid, Wall}

import scala.collection.mutable

/**
  * Copyright © 2015 Keith Webb
  */
object CollisionCases {

    val collisions = new mutable.HashMap[AnyRef, mutable.HashSet[AnyRef]]()

    def handle(data1: AnyRef, data2: AnyRef, isStart: Boolean): Unit = {

        if(isStart) {
            if (collisions.contains(data1)) {
                val set = collisions.get(data1).get
                if (!set.contains(data2)) {
                    set += data2
                }
            } else {
                collisions.put(data1, new mutable.HashSet[AnyRef]())
                collisions.get(data1).get += data2
            }

            if (collisions.contains(data2)) {
                val set = collisions.get(data2).get
                if (!set.contains(data1)) {
                    set += data1
                }
            } else {
                collisions.put(data2, new mutable.HashSet[AnyRef]())
                collisions.get(data2).get += data1
            }
        }
        else {
            if (collisions.contains(data1)) {
                val set = collisions.get(data1).get
                if (set.contains(data2)) {
                    set -= data2
                }
            }
            if (collisions.contains(data2)) {
                val set = collisions.get(data2).get
                if (set.contains(data1)) {
                    set -= data1
                }
            }
        }

        val isD1Bullet = data1.isInstanceOf[Bullet]
        val isD1Machine = data1.isInstanceOf[Machine]
        val isD1Wall = data1.isInstanceOf[Wall]
        val isD1Hostile = data1.isInstanceOf[Hostile]
        val isD1Asteroid = data1.isInstanceOf[Asteroid]
        val isD1Player = data1.isInstanceOf[Player]
        val isD1Mouse = data1.isInstanceOf[Mouse]
        val isD1AreaSensor = data1.isInstanceOf[AreaSensor]
        val isD1Item = data1.isInstanceOf[GenericItem]

        val isD1Entity = data1.isInstanceOf[Entity]
        val isD2Entity = data1.isInstanceOf[Entity]

        val isD2Bullet = data2.isInstanceOf[Bullet]
        val isD2Machine = data2.isInstanceOf[Machine]
        val isD2Wall = data2.isInstanceOf[Wall]
        val isD2Hostile = data2.isInstanceOf[Hostile]
        val isD2Asteroid = data2.isInstanceOf[Asteroid]
        val isD2Player = data2.isInstanceOf[Player]
        val isD2Mouse = data2.isInstanceOf[Mouse]
        val isD2AreaSensor = data2.isInstanceOf[AreaSensor]
        val isD2Item = data2.isInstanceOf[GenericItem]

        if(isStart) {
            if(isD1AreaSensor || isD2AreaSensor) {
                if(isD1Entity || isD2Entity) {
                    if (isD1Entity)
                        entityVAreaSensorCollideCase(data1.asInstanceOf[Entity], data2.asInstanceOf[AreaSensor])
                    else
                        entityVAreaSensorCollideCase(data2.asInstanceOf[Entity], data1.asInstanceOf[AreaSensor])
                }
            }
            if(isD1Hostile || isD2Hostile) {
                if(isD1Wall || isD2Wall) {
                    if(isD1Hostile)
                        hostileVWallCase(data1.asInstanceOf[Hostile], data2.asInstanceOf[Wall])
                    else
                        hostileVWallCase(data2.asInstanceOf[Hostile], data1.asInstanceOf[Wall])
                }
            }
            if (isD1Mouse || isD2Mouse) {
                if (isD1Machine || isD2Machine) {
                    if (isD1Mouse)
                        mouseVMachineCase(data1.asInstanceOf[Mouse], data2.asInstanceOf[Machine])
                    else
                        mouseVMachineCase(data2.asInstanceOf[Mouse], data1.asInstanceOf[Machine])
                }
                if (isD1Asteroid || isD2Asteroid) {
                    if (isD1Mouse)
                        mouseVAsteroidCase(data1.asInstanceOf[Mouse], data2.asInstanceOf[Asteroid])
                    else
                        mouseVAsteroidCase(data2.asInstanceOf[Mouse], data1.asInstanceOf[Asteroid])
                }
                if (isD1Item || isD2Item) {
                    if (isD1Mouse)
                        mouseVItemCase(data1.asInstanceOf[Mouse], data2.asInstanceOf[GenericItem])
                    else
                        mouseVItemCase(data2.asInstanceOf[Mouse], data1.asInstanceOf[GenericItem])
                }
            }
            if (isD1Bullet || isD2Bullet) {
                if (isD1Machine || isD2Machine) {
                    if (isD1Bullet)
                        bulletVMachineCase(data1.asInstanceOf[Bullet], data2.asInstanceOf[Machine])
                    else
                        bulletVMachineCase(data2.asInstanceOf[Bullet], data1.asInstanceOf[Machine])
                }
                else if (isD1Asteroid || isD2Asteroid) {
                    if (isD1Bullet)
                        bulletVAsteroidCase(data1.asInstanceOf[Bullet], data2.asInstanceOf[Asteroid])
                    else
                        bulletVAsteroidCase(data2.asInstanceOf[Bullet], data1.asInstanceOf[Asteroid])
                }
                else if (isD1Wall || isD2Wall) {
                    if (isD1Bullet)
                        bulletVWallCase(data1.asInstanceOf[Bullet], data2.asInstanceOf[Wall])
                    else
                        bulletVWallCase(data2.asInstanceOf[Bullet], data1.asInstanceOf[Wall])
                }
                else if (isD1Player || isD2Player) {
                    if (isD1Bullet)
                        bulletVPlayerCase(data1.asInstanceOf[Bullet], data2.asInstanceOf[Player])
                    else
                        bulletVPlayerCase(data2.asInstanceOf[Bullet], data1.asInstanceOf[Player])
                }
                else if (isD1Hostile || isD2Hostile) {
                    if (isD1Bullet)
                        bulletVHostileCase(data1.asInstanceOf[Bullet], data2.asInstanceOf[Hostile])
                    else
                        bulletVHostileCase(data2.asInstanceOf[Bullet], data1.asInstanceOf[Hostile])
                }
            }
            if (isD1Player || isD2Player) {
                if (isD1Wall || isD2Wall) {
                    if (isD1Player)
                        playerVWallCase(data1.asInstanceOf[Player], data2.asInstanceOf[Wall])
                    else
                        playerVWallCase(data2.asInstanceOf[Player], data1.asInstanceOf[Wall])
                }
                else if (isD1Asteroid || isD2Asteroid) {
                    if (isD1Player)
                        playerVAsteroidCase(data1.asInstanceOf[Player], data2.asInstanceOf[Asteroid])
                    else
                        playerVAsteroidCase(data2.asInstanceOf[Player], data1.asInstanceOf[Asteroid])
                }
            }
        }
        else {
            if(isD1Mouse || isD2Mouse) {
                if(isD1Machine || isD2Machine) {
                    if(isD1Mouse)
                        data1.asInstanceOf[Mouse].machine = null
                    else
                        data2.asInstanceOf[Mouse].machine = null
                }
                if(isD1Item || isD2Item) {
                    if(isD1Mouse)
                        data1.asInstanceOf[Mouse].item = null
                    else
                        data2.asInstanceOf[Mouse].item = null
                }
                if(isD1Asteroid || isD2Asteroid) {
                    if(isD1Mouse) {
                        val asteroid = data2.asInstanceOf[Asteroid]
                        val mouse = data1.asInstanceOf[Mouse]

                        if(asteroid == mouse.asteroid)
                            mouse.asteroid = null
                    }
                    else {
                        val asteroid = data1.asInstanceOf[Asteroid]
                        val mouse = data2.asInstanceOf[Mouse]

                        if(asteroid == mouse.asteroid)
                            mouse.asteroid = null
                    }
                }
            }
            else if (isD1AreaSensor || isD2AreaSensor) {
                if (isD1Entity || isD2Entity) {
                    if (isD1Entity)
                        entityVAreaSensorSeparateCase(data1.asInstanceOf[Entity], data2.asInstanceOf[AreaSensor])
                    else
                        entityVAreaSensorSeparateCase(data2.asInstanceOf[Entity], data1.asInstanceOf[AreaSensor])
                }
            }
        }
    }

    def entityVAreaSensorCollideCase(entity: Entity, sensor: AreaSensor): Unit = sensor.parentEntity.areaCollide(entity)
    def entityVAreaSensorSeparateCase(entity: Entity, sensor: AreaSensor): Unit = sensor.parentEntity.areaSeparate(entity)

    def mouseVMachineCase(mouse: Mouse, machine: Machine): Unit = mouse.machine = machine
    def mouseVAsteroidCase(mouse: Mouse, asteroid: Asteroid): Unit = mouse.asteroid = asteroid
    def mouseVItemCase(mouse: Mouse, item: GenericItem): Unit = mouse.item = item

    def hostileVWallCase(hostile: Hostile, wall: Wall): Unit = {}

    def bulletVMachineCase(bullet: Bullet, machine: Machine): Unit = {
        if(!bullet.friendly) {
            Assets.bulletHitSound.play()
            bullet.dispose()
            machine.health -= 10
            if (machine.health <= 0) {
                machine.isAlive = false
                Assets.destroySound.play()
            }
        }
    }

    def bulletVAsteroidCase(bullet: Bullet, asteroid: Asteroid): Unit = {
        bullet.dispose()
        Assets.bulletHitSound.play()
    }

    def bulletVWallCase(bullet: Bullet, wall: Wall): Unit = {
        Assets.bulletHitSound.play()
        bullet.dispose()
    }

    def bulletVPlayerCase(bullet: Bullet, player: Player): Unit = {
        if (!bullet.friendly) {
            bullet.dispose()
            Assets.bulletHitSound.play()
            player.health -= 10
            if (player.health <= 0) {
                Assets.destroySound.play()
            }
        }
    }

    def bulletVHostileCase(bullet: Bullet, hostile: Hostile): Unit = {
        if (bullet.friendly) {
            bullet.dispose()
            Assets.bulletHitSound.play()
            hostile.health -= 10
        }
    }

    def playerVWallCase(player: Player, wall: Wall): Unit = Assets.bounceSound.play()
    def playerVAsteroidCase(player: Player, asteroid: Asteroid): Unit = {
        if(!asteroid.isInstanceOf[Floor])
            Assets.bounceSound.play()
    }
}
