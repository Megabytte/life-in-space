package com.megabytte.lisg.util

import java.io.File

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.graphics.g2d.{SpriteBatch, TextureRegion}
import com.badlogic.gdx.graphics.glutils.FrameBuffer
import com.badlogic.gdx.graphics.{PixmapIO, GL20, Pixmap}

/**
  * Created by Megabytte on 12/14/15.
  */
class RenderTexture(width: Int, height: Int)
{
    private val mWidth = width
    private val mHeight = height
    private var mReady = false
    private val mFrameBuffer = new FrameBuffer(Pixmap.Format.RGBA8888, mWidth, mHeight, false)

    private val mRegion = new TextureRegion(mFrameBuffer.getColorBufferTexture)

    mRegion.flip(false, true)

    def getTexture: TextureRegion = mRegion

    def save(name : String, region : TextureRegion): Unit = {
        val pixmap = new Pixmap(region.getRegionWidth, region.getRegionHeight, Pixmap.Format.RGBA8888)

        val texture = region.getTexture
        if (!texture.getTextureData.isPrepared) {
            texture.getTextureData.prepare()
        }

        val pixmap2 = texture.getTextureData.consumePixmap()

        for (x <- 0 until region.getRegionWidth) {
            for (y <- 0 until region.getRegionHeight) {
                val colorInt = pixmap2.getPixel(region.getRegionX + x, region.getRegionY + y)
                pixmap.drawPixel(x, y, colorInt)
            }
        }

        val file = new File(name)
        file.createNewFile()

        PixmapIO.writePNG(new FileHandle(file), pixmap)

        pixmap.dispose()
    }

    def begin(batch:SpriteBatch): Unit =
    {
        if(!mReady)
        {
            mFrameBuffer.begin()
            batch.getProjectionMatrix.setToOrtho2D(0, 0, mFrameBuffer.getWidth, mFrameBuffer.getHeight)
            mReady = true
        }
    }

    def end(batch:SpriteBatch): Unit =
    {
        if(mReady)
        {
            mFrameBuffer.end()
            batch.getProjectionMatrix.setToOrtho2D(0, 0, Gdx.graphics.getWidth, Gdx.graphics.getHeight)
            mReady = false
        }
    }

    def clear(r:Float=0, g:Float=0, b:Float=0, a:Float=1): Unit =
    {
        if(mReady)
        {
            Gdx.gl.glClearColor(r, g, b, a)
            Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA)
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
        }
    }

    def dispose(): Unit =
    {
        mFrameBuffer.dispose()
        mReady = false
    }
}