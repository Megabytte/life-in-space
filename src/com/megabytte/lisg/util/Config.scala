package com.megabytte.lisg.util

import com.badlogic.gdx.maps.MapLayer
import com.badlogic.gdx.maps.objects.RectangleMapObject
import com.badlogic.gdx.maps.tiled.{TiledMapTileLayer, TiledMap}
import com.badlogic.gdx.physics.box2d._

import scala.collection.mutable

/**
  * Copyright © 2015 Keith Webb
  */
object Config {
    //All measurements are read as meters!!
    val bulletSpeed = Assets.toMetersFromPixels(500.0f)

    val playerAcceleration = Assets.toMetersFromPixels(100.0f)
    val playerSpeedLimit = Assets.toMetersFromPixels(200.0f)

    val hostileAcceleration = Assets.toMetersFromPixels(30.0f)
    val hostileSpeedLimit = Assets.toMetersFromPixels(100.0f)

    val entityAcceleration = Assets.toMetersFromPixels(30.0f)
    val entitySpeedLimit = Assets.toMetersFromPixels(110.0f)

    //Rates are in seconds.
    //Timers have a delay of said value,
    //then Timers repeat with that value
    //until certain conditions are met.
    val playerDrainHealthFastSpeed = 1
    val playerDrainHealthSlowSpeed = 6
    val playerDrainOxygenSpeed = 8
    val playerDrainHungerSpeed = 16
    val playerDrainThirstSpeed = 12

    val bulletDeathTime = 4

    val medicStationHealRate = 0.5f
    val medicStationHealAmount = 1.0f
    val turretFireRate = 0.5f
    val iceProcessorTickTime = 4
    val foodProcessorTickTime = 4
    val oreRefineryTickTime = 0.1f
    val distressBeaconWinTime = 5 * 60 //5 Minutes
    val assemblerSpeed = 1

    val inventorySlotPixelWidth = 64
    val inventorySlotPixelHeight = 64

    val oxygenTankRestoreValue = 5
    val waterTankRestoreValue = 5
    val foodRestoreValue = 5

    val hostileFireRate = 2

    var masterVolume = 1.0f
    var sfxVolume = 1.0f
    var musicVolume = 1.0f

    def setMasterVolume(volume: Float): Unit = {
        masterVolume = volume
        //Modify All Audio Volume By This Amount
        //To Be Called From Options State
    }
    def setSFXVolume(volume: Float): Unit = {
        sfxVolume = volume
        //Modify SFX Audio Volume By This Amount
        //To Be Called From Options State
    }
    def setMusicVolume(volume: Float): Unit = {
        musicVolume = volume
        //Modify Music Audio Volume By This Amount
        //To Be Called From Options State
    }

    def createMapObject(name: String, x: Int, y: Int, width: Int, height: Int): RectangleMapObject = {
        val mapObject = new RectangleMapObject(x * width, y * height, width, height)

        mapObject.setName(name + " (" + x + "," + y + ")")
        mapObject.getProperties.put("x", x * width)
        mapObject.getProperties.put("y", y * height)
        mapObject.getProperties.put("width", width)
        mapObject.getProperties.put("height", height)
        mapObject.getProperties.put("type", "object")
        mapObject.getProperties.put("bodyType", "StaticBody")
        mapObject.getProperties.put("density", 1)
        mapObject.getProperties.put("restitution", 1)

        mapObject
    }

    def addWallsToMap(mapLayer: MapLayer, mapWidth_InTiles: Int, mapHeight_InTiles: Int, tileSize_InPixels: Int): Unit = {
        val mapWidth_InPixels = mapWidth_InTiles * tileSize_InPixels
        val mapHeight_InPixels = mapHeight_InTiles * tileSize_InPixels
        val offset = 10

        val topWall = new RectangleMapObject(-offset, mapHeight_InPixels, mapWidth_InPixels + offset, offset)
        topWall.setName("Wall")
        topWall.getProperties.put("x", -offset)
        topWall.getProperties.put("y", mapHeight_InPixels)
        topWall.getProperties.put("width", mapWidth_InPixels + offset)
        topWall.getProperties.put("height", offset)
        topWall.getProperties.put("type", "object")
        topWall.getProperties.put("bodyType", "StaticBody")
        topWall.getProperties.put("density", 1)
        topWall.getProperties.put("restitution", 1)

        val bottomWall = new RectangleMapObject(-offset, -offset, mapWidth_InPixels + offset, offset)
        bottomWall.setName("Wall")
        bottomWall.getProperties.put("x", -offset)
        bottomWall.getProperties.put("y", -offset)
        bottomWall.getProperties.put("width", mapWidth_InPixels + offset)
        bottomWall.getProperties.put("height", offset)
        bottomWall.getProperties.put("type", "object")
        bottomWall.getProperties.put("bodyType", "StaticBody")
        bottomWall.getProperties.put("density", 1)
        bottomWall.getProperties.put("restitution", 1)

        val leftWall = new RectangleMapObject(-offset, -offset, offset, mapHeight_InPixels + offset)
        leftWall.setName("Wall")
        leftWall.getProperties.put("x", -offset)
        leftWall.getProperties.put("y", -offset)
        leftWall.getProperties.put("width", offset)
        leftWall.getProperties.put("height", mapHeight_InPixels + offset)
        leftWall.getProperties.put("type", "object")
        leftWall.getProperties.put("bodyType", "StaticBody")
        leftWall.getProperties.put("density", 1)
        leftWall.getProperties.put("restitution", 1)

        val rightWall = new RectangleMapObject(mapWidth_InPixels, -offset, offset, mapHeight_InPixels + offset)
        rightWall.setName("Wall")
        rightWall.getProperties.put("x", mapWidth_InPixels)
        rightWall.getProperties.put("y", -offset)
        rightWall.getProperties.put("width", offset)
        rightWall.getProperties.put("height", mapHeight_InPixels + offset)
        rightWall.getProperties.put("type", "object")
        rightWall.getProperties.put("bodyType", "StaticBody")
        rightWall.getProperties.put("density", 1)
        rightWall.getProperties.put("restitution", 1)

        mapLayer.getObjects.add(topWall)
        mapLayer.getObjects.add(bottomWall)
        mapLayer.getObjects.add(leftWall)
        mapLayer.getObjects.add(rightWall)
    }

    def addSolidObjectsToMap(map: TiledMap, IDToNameMap: mutable.HashMap[Int, String], tileSize: Int): Unit = {
        val tileLayer = map.getLayers.get("Tiles").asInstanceOf[TiledMapTileLayer]
        val solidLayer = new MapLayer
        solidLayer.setName("Solid")

        for(y <- 0 until tileLayer.getHeight) {
            for(x <- 0 until tileLayer.getWidth) {
                val cell = tileLayer.getCell(x, y)
                val cellID = cell.getTile.getId

                var cellTileName = "Unknown"
                if(IDToNameMap.contains(cellID))
                    cellTileName = IDToNameMap.get(cellID).get

                //Hard Coded
                if(cellID != 0 && cellID != 1 && cellID != 14) {
                    val solidObject = Config.createMapObject(cellTileName, x, y, tileSize, tileSize)
                    solidLayer.getObjects.add(solidObject)
                }
            }
        }
        map.getLayers.add(solidLayer)
    }

    def createBodyDefFromBody(body: Body): BodyDef = {
        val bodyDef = new BodyDef

        bodyDef.`type` = body.getType
        bodyDef.fixedRotation = body.isFixedRotation
        bodyDef.linearDamping = body.getLinearDamping
        bodyDef.position.set(body.getPosition)
        bodyDef.angle = body.getAngle
        bodyDef.bullet = body.isBullet
        bodyDef.angularDamping = body.getAngularDamping
        bodyDef.angularVelocity = body.getAngularVelocity

        bodyDef
    }

    def setupBody(bType: BodyDef.BodyType, xPosPixels: Float, yPosPixels: Float, fixedRotation: Boolean, linearDampening: Float): Body = {
        val bodyDef = new BodyDef
        bodyDef.`type` = bType
        bodyDef.fixedRotation = fixedRotation
        bodyDef.linearDamping = linearDampening
        bodyDef.position.set(Assets.toMetersFromPixels(xPosPixels), Assets.toMetersFromPixels(yPosPixels))
        Assets.world.createBody(bodyDef)
    }
    //Change Radius To Use Pixels Too
    def setupFixture(body: Body, shapeName: String, density: Float, sizeXInPixels: Float=1, sizeYInPixels: Float=1, radiusInPixels: Float=10, isSensor: Boolean=false): Fixture = {
        val fixtureDef = new FixtureDef
        var fix : Fixture = null

        fixtureDef.density = density
        fixtureDef.isSensor = isSensor

        if(shapeName.contains("Polygon") || shapeName.contains("Square")) {
            val shape = new PolygonShape
            shape.setAsBox(Assets.toMetersFromPixels(sizeXInPixels), Assets.toMetersFromPixels(sizeYInPixels))
            fixtureDef.shape = shape
            fix = body.createFixture(fixtureDef)
            shape.dispose()
        } else if(shapeName.contains("Circle")) {
            val shape = new CircleShape()
            shape.setRadius(Assets.toMfP(radiusInPixels))
            fixtureDef.shape = shape
            fix = body.createFixture(fixtureDef)
            shape.dispose()
        }

        fix
    }
}
