package com.megabytte.lisg

import com.badlogic.gdx.ApplicationListener
import com.megabytte.lisg.states.MainMenuState
import com.megabytte.lisg.states.gsm.GameStateManager
import com.megabytte.lisg.util.GameFileUtil

/**
  * Copyright © 2015 Keith Webb
  */
class Core extends ApplicationListener
{
    override def create(): Unit =
    {
        GameFileUtil.setup()

        GameStateManager.pushState(new MainMenuState)
    }

    override def render(): Unit =
    {
        GameStateManager.render()
    }

    override def dispose(): Unit =
    {
        GameStateManager.dispose()
    }

    override def pause(): Unit =
    {
        GameStateManager.pause()
    }

    override def resume(): Unit =
    {
        GameStateManager.resume()
    }

    override def resize(width: Int, height: Int): Unit =
    {
        GameStateManager.resize(width, height)
    }
}
