package com.megabytte.lisg.states

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input.Keys
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.{GL20, Pixmap, Texture}
import com.badlogic.gdx.scenes.scene2d.ui.TextButton
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.scenes.scene2d.{InputEvent, Stage}
import com.badlogic.gdx.utils.viewport.ScreenViewport
import com.megabytte.lisg.states.gsm.{GameState, GameStateManager}
import com.megabytte.lisg.util.{GameFileUtil, Assets}

/**
  * Copyright © 2015 Keith Webb
  */
class PauseGameState(bg: Pixmap) extends GameState {
    val stage = new Stage(new ScreenViewport())

    val bgTexture = new Texture(bg)
    val bgSprite = new Sprite(bgTexture)

    bgSprite.setAlpha(0.4f)

    //Add Buttons
    val resumeButton = new TextButton("Resume", Assets.ui_skin, "default")
    resumeButton.setWidth(200f)
    resumeButton.setHeight(20f)
    resumeButton.setPosition(Gdx.graphics.getWidth / 2 - 100f, Gdx.graphics.getHeight / 2 - 40f)
    resumeButton.addListener(new ClickListener() {
        override def clicked(event : InputEvent, x : Float, y : Float): Unit = {
            Assets.ui_clickSound.play()
            GameStateManager.popState()
        }
    })
    stage.addActor(resumeButton)

    val mainMenuButton = new TextButton("Main Menu", Assets.ui_skin, "default")
    mainMenuButton.setWidth(200f)
    mainMenuButton.setHeight(20f)
    mainMenuButton.setPosition(Gdx.graphics.getWidth / 2 - 100f, Gdx.graphics.getHeight / 2 - 70f)
    mainMenuButton.addListener(new ClickListener() {
        override def clicked(event : InputEvent, x : Float, y : Float): Unit = {
            Assets.ui_clickSound.play()
            GameStateManager.popState()
            Assets.spaceMode = true
            GameStateManager.changeState(new MainMenuState)
        }
    })
    stage.addActor(mainMenuButton)

    val saveButton = new TextButton("Save Game", Assets.ui_skin, "default")
    saveButton.setWidth(200f)
    saveButton.setHeight(20f)
    saveButton.setPosition(Gdx.graphics.getWidth / 2 - 100f, Gdx.graphics.getHeight / 2 - 100f)
    saveButton.addListener(new ClickListener() {
        override def clicked(event : InputEvent, x : Float, y : Float): Unit = {
            Assets.ui_clickSound.play()
            GameStateManager.popState()
            GameStateManager.top() match {
                case state: MainGameState =>
                    GameFileUtil.saveGame(GameFileUtil.createName(), state)
                case _ =>
            }
        }
    })
    stage.addActor(saveButton)

    val exitGameButton = new TextButton("Quit to OS", Assets.ui_skin, "default")
    exitGameButton.setWidth(200f)
    exitGameButton.setHeight(20f)
    exitGameButton.setPosition(Gdx.graphics.getWidth / 2 - 100f, Gdx.graphics.getHeight / 2 - 140f)
    exitGameButton.addListener(new ClickListener() {
        override def clicked(event : InputEvent, x : Float, y : Float): Unit = {
            Gdx.app.exit()
        }
    })
    stage.addActor(exitGameButton)

    Gdx.input.setInputProcessor(stage)

    override def update(deltaTime: Float): Unit = {
        stage.act(deltaTime)

        if(Gdx.input.isKeyJustPressed(Keys.ESCAPE)) {
            GameStateManager.popState()
        }
    }

    override def render(): Unit = {
        Gdx.gl.glClearColor(0, 0, 0, 1)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        spriteBatch.begin()
        bgSprite.draw(spriteBatch)
        spriteBatch.end()

        stage.draw()
    }

    override def dispose(): Unit = {
        super.dispose()
        stage.dispose()
        bg.dispose()
        bgTexture.dispose()
    }

    override def resize(width: Int, height: Int) : Unit = {
        stage.getViewport.update(width, height, false)
    }

    override def resume(): Unit = {
        Gdx.input.setInputProcessor(stage)
    }

}
