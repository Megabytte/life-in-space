package com.megabytte.lisg.states.gsm

import com.badlogic.gdx.Gdx

import scala.collection.mutable

/**
  * Created by Megabytte on 12/14/15.
  */
object GameStateManager
{
    private val stack : mutable.Stack[GameState] = new mutable.Stack[GameState]()
    private var stateDepth = 0

    def pushState(state:GameState): Unit =
    {
        if(stateDepth != 0)
            stack.top.pause()
        stack.push(state)
        stateDepth += 1
    }

    def top(): GameState = {
        stack.top
    }

    def popState(): Unit =
    {
        stack.pop().dispose()
        stateDepth -= 1

        if(stateDepth != 0)
            stack.top.resume()
    }

    def changeState(state:GameState): Unit =
    {
        popState()
        pushState(state)
    }

    def update(): Unit =
    {
        stack.top.update(Gdx.graphics.getDeltaTime)
    }

    def render(): Unit =
    {
        update()
        stack.top.render()
    }

    def dispose(): Unit =
    {
        for(state <- stack)
        {
            state.dispose()
        }
        stack.clear()
    }

    def pause() : Unit =
    {
        stack.top.pause()
    }

    def resume() : Unit =
    {
        stack.top.resume()
    }

    def resize(width: Int, height: Int) : Unit =
    {
        for(state <- stack)
        {
            state.resize(width, height)
        }
    }
}
