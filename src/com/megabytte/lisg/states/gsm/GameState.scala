package com.megabytte.lisg.states.gsm

import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.g2d.freetype.{FreeTypeFontGeneratorLoader, FreeTypeFontGenerator}
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.maps.tiled.{TiledMap, TmxMapLoader}

/**
  * Created by Megabytte on 12/14/15.
  */
trait GameState
{
    val spriteBatch = new SpriteBatch()
    val shapeRenderer = new ShapeRenderer()
    val assetManager = new AssetManager()
    assetManager.setLoader(classOf[FreeTypeFontGenerator], new FreeTypeFontGeneratorLoader(new InternalFileHandleResolver()))
    assetManager.setLoader(classOf[TiledMap], new TmxMapLoader(new InternalFileHandleResolver()))

    def update(deltaTime:Float) : Unit
    def render() : Unit
    def dispose() : Unit = {
        spriteBatch.dispose()
        shapeRenderer.dispose()
        assetManager.dispose()
    }
    def pause() : Unit = {}
    def resume() : Unit = {}
    def resize(width: Int, height: Int) : Unit = {}
}
