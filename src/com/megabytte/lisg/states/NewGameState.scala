package com.megabytte.lisg.states

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.scenes.scene2d.ui.TextButton
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.scenes.scene2d.{InputEvent, Stage}
import com.badlogic.gdx.utils.viewport.ScreenViewport
import com.megabytte.lisg.states.gsm.{GameState, GameStateManager}
import com.megabytte.lisg.util.Assets
import com.megabytte.lisg.world.WorldGenerationParameters

/**
  * Copyright © 2015 Keith Webb
  */
class NewGameState extends GameState {
    val stage = new Stage(new ScreenViewport())

    val worldGenerationParameters = new WorldGenerationParameters

    //Add Buttons
    val backButton = new TextButton("Back", Assets.ui_skin, "default")
    backButton.setWidth(100f)
    backButton.setHeight(20f)
    backButton.setPosition(100f, 30f)
    backButton.addListener(new ClickListener() {
        override def clicked(event : InputEvent, x : Float, y : Float): Unit = {
            Assets.ui_clickSound.play()
            GameStateManager.changeState(new MainMenuState)
        }
    })
    stage.addActor(backButton)

    val startButton = new TextButton("Start Game", Assets.ui_skin, "default")
    startButton.setWidth(100f)
    startButton.setHeight(20f)
    startButton.setPosition(700f, 30f)
    startButton.addListener(new ClickListener() {
        override def clicked(event : InputEvent, x : Float, y : Float): Unit = {
            Assets.ui_clickSound.play()
            GameStateManager.changeState(new MainGameState(worldGenerationParameters))
        }
    })
    stage.addActor(startButton)

    Gdx.input.setInputProcessor(stage)

    override def update(deltaTime: Float): Unit = {
        stage.act(deltaTime)
    }

    override def render(): Unit = {
        Gdx.gl.glClearColor(0, 0, 0, 1)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        stage.draw()
    }

    override def dispose(): Unit = {
        super.dispose()
        stage.dispose()
    }

    override def resize(width: Int, height: Int) : Unit = {
        stage.getViewport.update(width, height, false)
    }
}
