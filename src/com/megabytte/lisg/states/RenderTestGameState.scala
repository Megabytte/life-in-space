package com.megabytte.lisg.states

import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter
import com.badlogic.gdx.graphics.g2d.{BitmapFont, Sprite, SpriteBatch}
import com.badlogic.gdx.graphics.{Color, GL20, Texture}
import com.badlogic.gdx.{Gdx, Input}
import com.megabytte.lisg.states.gsm.{GameState, GameStateManager}
import com.megabytte.lisg.util.RenderTexture

/**
  * Copyright © 2015 Keith Webb
  */
class RenderTestGameState extends GameState
{
    assetManager.load("badlogic.jpg", classOf[Texture])
    assetManager.load("arial.ttf", classOf[FreeTypeFontGenerator])
    assetManager.finishLoading()

    var texture = assetManager.get("badlogic.jpg", classOf[Texture])
    var renderTexture = new RenderTexture(256, 256)
    var sprite = new Sprite(renderTexture.getTexture)
    var arialGenerator = assetManager.get("arial.ttf", classOf[FreeTypeFontGenerator])
    var font16 : BitmapFont = arialGenerator.generateFont(new FreeTypeFontParameter)

    font16.setColor(Color.BLUE)

    override def update(deltaTime: Float): Unit =
    {
        if(Gdx.input.isKeyPressed(Input.Keys.W)) { sprite.translate(0, 100 * deltaTime) }
        if(Gdx.input.isKeyPressed(Input.Keys.S)) { sprite.translate(0, -100 * deltaTime) }
        if(Gdx.input.isKeyPressed(Input.Keys.A)) { sprite.translate(-100 * deltaTime, 0) }
        if(Gdx.input.isKeyPressed(Input.Keys.D)) { sprite.translate(100 * deltaTime, 0) }
        if(Gdx.input.isKeyPressed(Input.Keys.Q)) { sprite.rotate(50 * deltaTime) }
        if(Gdx.input.isKeyPressed(Input.Keys.E)) { sprite.rotate(-50 * deltaTime) }
        if(Gdx.input.isKeyPressed(Input.Keys.Z)) { sprite.scale(0.5f * deltaTime) }
        if(Gdx.input.isKeyPressed(Input.Keys.C)) { sprite.scale(-0.5f * deltaTime) }
    }

    override def render(): Unit =
    {
        renderTexture.begin(spriteBatch)
        renderTexture.clear(0.5f)
        spriteBatch.begin()
        spriteBatch.draw(texture, 0, 0)
        font16.draw(spriteBatch, "Hello", 100, 128)
        spriteBatch.end()
        renderTexture.end(spriteBatch)

        Gdx.gl.glClearColor(0, 0, 0, 1)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        spriteBatch.begin()
        sprite.draw(spriteBatch)
        spriteBatch.end()
    }

    override def dispose(): Unit =
    {
        super.dispose()
        renderTexture.dispose()
        font16.dispose()
    }
}
