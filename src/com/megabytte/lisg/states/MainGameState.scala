package com.megabytte.lisg.states

import com.badlogic.gdx.Input.Keys
import com.badlogic.gdx.audio.Sound
import com.badlogic.gdx.graphics.{GL20, OrthographicCamera, Texture}
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.utils.Timer.Task
import com.badlogic.gdx.utils.viewport.ScreenViewport
import com.badlogic.gdx.utils.{TimeUtils, Timer}
import com.badlogic.gdx.{Gdx, InputAdapter, InputMultiplexer}
import com.megabytte.lisg.entities.player.Player
import com.megabytte.lisg.entities.util.Mouse
import com.megabytte.lisg.inventory.items.Item
import com.megabytte.lisg.states.gsm.{GameState, GameStateManager}
import com.megabytte.lisg.util.{Assets, CameraConstrainer}
import com.megabytte.lisg.world.{GameWorld, WorldGenerationParameters}

/**
  * Copyright © 2015 Keith Webb
  */
class MainGameState extends InputAdapter with GameState {
    loadAssets()

    var stage : Stage = null
    var gameWorld : GameWorld = null
    var inputMultiplexer : InputMultiplexer = null
    var pauseTimeDelay: Long = 0
    var mousePhysicsObject : Mouse = null
    var player : Player = null
    var camera : OrthographicCamera = null
    var cameraConstrainer : CameraConstrainer = null

    def this(worldGenerationParameters: WorldGenerationParameters) {
        this
        stage = new Stage(new ScreenViewport())
        gameWorld = new GameWorld(worldGenerationParameters)
        Assets.setup(assetManager, gameWorld)
        inputMultiplexer = new InputMultiplexer(stage)
        inputMultiplexer.addProcessor(this)
        Gdx.input.setInputProcessor(inputMultiplexer)
        mousePhysicsObject = new Mouse
        player = new Player(mousePhysicsObject, stage)
        //gameWorld.addHostile(1000, 100)
        camera = new OrthographicCamera(Gdx.graphics.getWidth, Gdx.graphics.getHeight)
        cameraConstrainer = new CameraConstrainer(camera, gameWorld.currentMap.map)

        //Cheats For Now
        player.itemHotBar.addItem(Item.IDs.GUN, 1)
        player.itemHotBar.addItem(Item.IDs.WELDER, 1)
        player.itemHotBar.addItem(Item.IDs.GRINDER, 1)
        player.itemHotBar.addItem(Item.IDs.DRILL, 1)
        player.itemHotBar.addItem(Item.IDs.TURRET, 1)

        player.inventory.addItem(Item.IDs.MEDIC_STATION, 1)
        player.inventory.addItem(Item.IDs.ICE_PROCESSOR, 1)
        player.inventory.addItem(Item.IDs.MATERIAL_ASSEMBLER, 1)
        player.inventory.addItem(Item.IDs.ICE, 9999)
        player.inventory.addItem(Item.IDs.SPACE_MEAT, 9999)
        player.inventory.addItem(Item.IDs.SPACE_MOSS, 9999)
        player.inventory.addItem(Item.IDs.FOOD_PROCESSOR, 1)

        player.inventory.addItem(Item.IDs.ORE_REFINERY, 1)
        player.inventory.addItem(Item.IDs.ORE_COBALT, 9999)
        player.inventory.addItem(Item.IDs.ORE_GOLD, 9999)
        player.inventory.addItem(Item.IDs.ORE_IRON, 9999)
        player.inventory.addItem(Item.IDs.ORE_MAGNESIUM, 9999)
        player.inventory.addItem(Item.IDs.ORE_NICKEL, 9999)
        player.inventory.addItem(Item.IDs.ORE_PLATINUM, 9999)
        player.inventory.addItem(Item.IDs.ROCK, 9999)
        player.inventory.addItem(Item.IDs.ORE_SILICON, 9999)
        player.inventory.addItem(Item.IDs.ORE_SILVER, 9999)
        player.inventory.addItem(Item.IDs.ORE_URANIUM, 9999)

        //Prevent Player Pop
        camera.position.set(player.sprite.getPixelX, player.sprite.getPixelY, 0.0f)
        cameraConstrainer.update()
        camera.update()
    }

    def this(path: String) {
        this
        stage = new Stage(new ScreenViewport())
        gameWorld = new GameWorld(path)
        Assets.setup(assetManager, gameWorld)
        inputMultiplexer = new InputMultiplexer(stage)
        inputMultiplexer.addProcessor(this)
        Gdx.input.setInputProcessor(inputMultiplexer)
        mousePhysicsObject = new Mouse

        player = new Player(mousePhysicsObject, stage)
        camera = new OrthographicCamera(Gdx.graphics.getWidth, Gdx.graphics.getHeight)
        cameraConstrainer = new CameraConstrainer(camera, gameWorld.currentMap.map)

        //Prevent Player Pop
        camera.position.set(player.sprite.getPixelX, player.sprite.getPixelY, 0.0f)
        cameraConstrainer.update()
        camera.update()
    }

    val debugRenderer = new Box2DDebugRenderer(true, false, true, true, false, true)

    var drawDebug = false

    override def update(deltaTime: Float): Unit = {
        stage.act(deltaTime)

        camera.position.set(player.sprite.getPixelX, player.sprite.getPixelY, 0.0f)
        cameraConstrainer.update()
        camera.update()

        gameWorld.update(deltaTime, camera)
        player.update()

        if (Gdx.input.isKeyJustPressed(Keys.ESCAPE)) {
            val bg = Assets.getScreenShot(0, 0, Gdx.graphics.getWidth, Gdx.graphics.getHeight, yDown = true)
            GameStateManager.pushState(new PauseGameState(bg))
        }

        if (Gdx.input.isKeyJustPressed(Keys.NUM_0))
            drawDebug = !drawDebug

        if (player.isAlive && player.health <= 0) {
            player.isAlive = false
            Assets.gameOverSound.play()

            Timer.schedule(new Task {
                override def run(): Unit = {
                    GameStateManager.changeState(new GameOverState)
                }
            }, 2)
        }

        mousePhysicsObject.update(camera)
    }

    override def touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean =
        player.touchDown(screenX, screenY, pointer, button, camera)

    override def scrolled(amount: Int): Boolean =
        player.scrolled(amount)

    override def render(): Unit = {
        Gdx.gl.glClearColor(0, 0, 0, 1)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        spriteBatch.setProjectionMatrix(camera.combined)

        gameWorld.renderMap()

        spriteBatch.begin()

        gameWorld.renderEntities(spriteBatch)

        player.render(spriteBatch)

        spriteBatch.end()

        if (drawDebug)
            debugRenderer.render(gameWorld.box2DWorld, spriteBatch.getProjectionMatrix.cpy().scale(Assets.PIXELS_TO_METERS, Assets.PIXELS_TO_METERS, 0))

        stage.draw()
    }

    override def dispose(): Unit = {
        super.dispose()
        player.dispose()
        mousePhysicsObject.dispose()
        debugRenderer.dispose()
        stage.dispose()
        gameWorld.dispose() //Dispose of physics objects before the world!!!
        Assets.emptyInventorySlotTexture.dispose()
    }

    override def resize(width: Int, height: Int): Unit = {
        camera.viewportWidth = width
        camera.viewportHeight = height
        cameraConstrainer.resize(width, height)
        camera.update()
        stage.getViewport.update(width, height, false)
    }

    override def resume(): Unit = {
        Gdx.input.setInputProcessor(inputMultiplexer)
        Timer.instance().delay(TimeUtils.nanosToMillis(TimeUtils.nanoTime()) - pauseTimeDelay)
        Timer.instance().start()
    }

    override def pause(): Unit = {
        Timer.instance().stop()
        pauseTimeDelay = TimeUtils.nanosToMillis(TimeUtils.nanoTime())
    }

    def loadAssets(): Unit = {
        assetManager.load("textures/player.png", classOf[Texture])

        assetManager.load("textures/items/tools/LaserGun.png", classOf[Texture])
        assetManager.load("textures/items/tools/Drill.png", classOf[Texture])
        assetManager.load("textures/items/tools/Grinder.png", classOf[Texture])
        assetManager.load("textures/items/tools/Welder.png", classOf[Texture])

        assetManager.load("textures/items/consumable/food.png", classOf[Texture])
        assetManager.load("textures/items/consumable/oxygenTank.png", classOf[Texture])
        assetManager.load("textures/items/consumable/waterTank.png", classOf[Texture])

        assetManager.load("textures/items/ores/Rock.png", classOf[Texture])
        assetManager.load("textures/items/ores/RockIce.png", classOf[Texture])
        assetManager.load("textures/items/ores/OreIron.png", classOf[Texture])
        assetManager.load("textures/items/ores/OreGold.png", classOf[Texture])
        assetManager.load("textures/items/ores/OreUranium.png", classOf[Texture])
        assetManager.load("textures/items/ores/OreNickel.png", classOf[Texture])
        assetManager.load("textures/items/ores/OrePlatinum.png", classOf[Texture])
        assetManager.load("textures/items/ores/OreCobalt.png", classOf[Texture])
        assetManager.load("textures/items/ores/OreSilver.png", classOf[Texture])
        assetManager.load("textures/items/ores/OreSilicon.png", classOf[Texture])
        assetManager.load("textures/items/ores/OreMagnesium.png", classOf[Texture])

        assetManager.load("textures/items/metals/CobaltIngot.png", classOf[Texture])
        assetManager.load("textures/items/metals/GoldIngot.png", classOf[Texture])
        assetManager.load("textures/items/metals/IronIngot.png", classOf[Texture])
        assetManager.load("textures/items/metals/Gravel.png", classOf[Texture])
        assetManager.load("textures/items/metals/MagnesiumPowder.png", classOf[Texture])
        assetManager.load("textures/items/metals/NickelIngot.png", classOf[Texture])
        assetManager.load("textures/items/metals/PlatinumIngot.png", classOf[Texture])
        assetManager.load("textures/items/metals/SilverIngot.png", classOf[Texture])
        assetManager.load("textures/items/metals/UraniumIngot.png", classOf[Texture])

        assetManager.load("textures/items/components/Moss.png", classOf[Texture])
        assetManager.load("textures/items/components/Meat.png", classOf[Texture])
        assetManager.load("textures/items/components/Computer.png", classOf[Texture])
        assetManager.load("textures/items/components/ConstructionComponents.png", classOf[Texture])
        assetManager.load("textures/items/components/Display.png", classOf[Texture])
        assetManager.load("textures/items/components/InteriorPlate.png", classOf[Texture])
        assetManager.load("textures/items/components/LargeSteelTube.png", classOf[Texture])
        assetManager.load("textures/items/components/SmallSteelTube.png", classOf[Texture])
        assetManager.load("textures/items/components/MedicalComponents.png", classOf[Texture])
        assetManager.load("textures/items/components/MetalGrid.png", classOf[Texture])
        assetManager.load("textures/items/components/Motor.png", classOf[Texture])
        assetManager.load("textures/items/components/RadioCommunicationsComponents.png", classOf[Texture])
        assetManager.load("textures/items/components/ReactorComponents.png", classOf[Texture])
        assetManager.load("textures/items/components/SiliconWafer.png", classOf[Texture])
        assetManager.load("textures/items/components/SteelPlate.png", classOf[Texture])
        assetManager.load("textures/items/components/SuperconductorComponents.png", classOf[Texture])

        assetManager.load("textures/machines/machine.png", classOf[Texture])
        assetManager.load("textures/machines/medicStation.png", classOf[Texture])
        assetManager.load("textures/machines/turret.png", classOf[Texture])
        assetManager.load("textures/machines/iceProcessor.png", classOf[Texture])
        assetManager.load("textures/machines/foodProcessor.png", classOf[Texture])
        assetManager.load("textures/machines/oreRefinery.png", classOf[Texture])
        assetManager.load("textures/machines/materialAssembler.png", classOf[Texture])
        assetManager.load("textures/machines/generator.png", classOf[Texture])
        assetManager.load("textures/machines/distressBeacon.png", classOf[Texture])

        assetManager.load("textures/bullet.png", classOf[Texture])

        assetManager.load("audio/jets.wav", classOf[Sound])
        assetManager.load("audio/bullet_hit.wav", classOf[Sound])
        assetManager.load("audio/destroy.wav", classOf[Sound])
        assetManager.load("audio/laser.wav", classOf[Sound])
        assetManager.load("audio/bounce.wav", classOf[Sound])
        assetManager.load("audio/gameOver.wav", classOf[Sound])
        assetManager.load("audio/scaredAnimal.wav", classOf[Sound])

        assetManager.finishLoading()
        //Get Loading Screen Implemented
    }
}
