package com.megabytte.lisg.states

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.scenes.scene2d.ui.TextButton
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.scenes.scene2d.{InputEvent, Stage}
import com.badlogic.gdx.utils.viewport.ScreenViewport
import com.megabytte.lisg.states.gsm.{GameState, GameStateManager}
import com.megabytte.lisg.util.Assets

/**
  * Copyright © 2015 Keith Webb
  */
class OptionsState extends GameState {
    val stage = new Stage(new ScreenViewport())

    //Add Buttons
    val backButton = new TextButton("Back", Assets.ui_skin, "default")
    backButton.setWidth(100f)
    backButton.setHeight(20f)
    backButton.setPosition(100f, 30f)
    backButton.addListener(new ClickListener() {
        override def clicked(event : InputEvent, x : Float, y : Float): Unit = {
            Assets.ui_clickSound.play()
            GameStateManager.changeState(new MainMenuState)
        }
    })
    stage.addActor(backButton)

    Gdx.input.setInputProcessor(stage)

    override def update(deltaTime: Float): Unit = {
        stage.act(deltaTime)
    }

    override def render(): Unit = {
        Gdx.gl.glClearColor(0, 0, 0, 1)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        stage.draw()
    }

    override def dispose(): Unit = {
        super.dispose()
        stage.dispose()
    }

    override def resize(width: Int, height: Int) : Unit = {
        stage.getViewport.update(width, height, false)
    }
}
