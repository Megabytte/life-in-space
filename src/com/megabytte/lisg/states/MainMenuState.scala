package com.megabytte.lisg.states

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input.Keys
import com.badlogic.gdx.graphics._
import com.badlogic.gdx.scenes.scene2d.ui.{Skin, TextButton}
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.scenes.scene2d.{InputEvent, Stage}
import com.badlogic.gdx.utils.viewport.ScreenViewport
import com.megabytte.lisg.states.gsm.{GameState, GameStateManager}
import com.megabytte.lisg.util.Assets
import com.megabytte.lisg.world.WorldGenerationParameters

/**
  * Copyright © 2015 Keith Webb
  */
class MainMenuState extends GameState
{
    Assets.ui_clickSound = Gdx.audio.newSound(Gdx.files.internal("ui/switch1.ogg"))
    Assets.ui_skin = new Skin(Gdx.files.internal("ui/uiskin.json"))

    val stage = new Stage(new ScreenViewport())

    val newGameButton = new TextButton("New Game", Assets.ui_skin, "default")
    newGameButton.setWidth(200f)
    newGameButton.setHeight(20f)
    newGameButton.setPosition(Gdx.graphics.getWidth / 2 - 100f, Gdx.graphics.getHeight / 2 - 10f)
    newGameButton.addListener(new ClickListener() {
        override def clicked(event : InputEvent, x : Float, y : Float): Unit = {
            Assets.ui_clickSound.play()
            GameStateManager.changeState(new NewGameState)
        }
    })
    stage.addActor(newGameButton)

    val loadGameButton = new TextButton("Load Game", Assets.ui_skin, "default")
    loadGameButton.setWidth(200f)
    loadGameButton.setHeight(20f)
    loadGameButton.setPosition(Gdx.graphics.getWidth / 2 - 100f, Gdx.graphics.getHeight / 2 - 40f)
    loadGameButton.addListener(new ClickListener() {
        override def clicked(event : InputEvent, x : Float, y : Float): Unit = {
            Assets.ui_clickSound.play()
            GameStateManager.changeState(new LoadGameState)
        }
    })
    stage.addActor(loadGameButton)

    val optionsButton = new TextButton("Options", Assets.ui_skin, "default")
    optionsButton.setWidth(200f)
    optionsButton.setHeight(20f)
    optionsButton.setPosition(Gdx.graphics.getWidth / 2 - 100f, Gdx.graphics.getHeight / 2 - 70f)
    optionsButton.addListener(new ClickListener() {
        override def clicked(event : InputEvent, x : Float, y : Float): Unit = {
            Assets.ui_clickSound.play()
            GameStateManager.changeState(new OptionsState)
        }
    })
    stage.addActor(optionsButton)

    val exitButton = new TextButton("Exit", Assets.ui_skin, "default")
    exitButton.setWidth(200f)
    exitButton.setHeight(20f)
    exitButton.setPosition(Gdx.graphics.getWidth / 2 - 100f, Gdx.graphics.getHeight / 2 - 100f)
    exitButton.addListener(new ClickListener() {
        override def clicked(event : InputEvent, x : Float, y : Float): Unit = {
            Gdx.app.exit()
        }
    })
    stage.addActor(exitButton)

    Gdx.input.setInputProcessor(stage)

    override def update(deltaTime: Float): Unit = {
        stage.act(deltaTime)

        if(Gdx.input.isKeyJustPressed(Keys.SPACE)) {
            GameStateManager.changeState(new MainGameState(new WorldGenerationParameters))
        }

    }

    override def render(): Unit = {
        Gdx.gl.glClearColor(0, 0, 0, 1)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        stage.draw()
    }

    override def dispose(): Unit = {
        super.dispose()
        stage.dispose()
    }

    override def resize(width: Int, height: Int) : Unit = {
        stage.getViewport.update(width, height, false)
    }

}
