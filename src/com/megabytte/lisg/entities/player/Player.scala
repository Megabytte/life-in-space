package com.megabytte.lisg.entities.player

import com.badlogic.gdx.graphics.g2d.{SpriteBatch, TextureRegion}
import com.badlogic.gdx.graphics.{Color, OrthographicCamera}
import com.badlogic.gdx.math.{Vector2, Vector3}
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType
import com.badlogic.gdx.scenes.scene2d.ui.{Dialog, ProgressBar, TextButton}
import com.badlogic.gdx.scenes.scene2d.{InputEvent, InputListener, Stage}
import com.badlogic.gdx.utils.Timer
import com.badlogic.gdx.utils.Timer.Task
import com.badlogic.gdx.{Gdx, Input}
import com.megabytte.lisg.entities.Entity
import com.megabytte.lisg.entities.items._
import com.megabytte.lisg.entities.machines.Machine
import com.megabytte.lisg.entities.util.{Box2DSprite, Mouse}
import com.megabytte.lisg.inventory.items.{InventoryItem, Item}
import com.megabytte.lisg.inventory.{HotBar, Inventory, ItemIO}
import com.megabytte.lisg.util.{Assets, Config}
import com.megabytte.lisg.world.blocks.{FloorHole, AsteroidHole}

/**
  * Copyright © 2015 Keith Webb
  */
class Player(lMouse: Mouse, stage: Stage) extends Entity {
    val mouse = lMouse
    friendly = true
    body = Config.setupBody(BodyType.DynamicBody, 30, 30, fixedRotation = true, 0.2f)
    fixture = Config.setupFixture(body, "Square", 30, 7, 9)
    fixture.setUserData(this)

    var location = "Space"

    val inventory = new Inventory(stage, "Player Inventory", 8, 4, Gdx.graphics.getWidth / 2, Gdx.graphics.getHeight / 2)
    val itemHotBar = new HotBar(inventory, stage, Assets.ui_skin)
    val itemIO = new ItemIO(stage, this, 850, 50)
    var mostRecentMachinePlaced: Machine = null

    var machineWithInventory : Machine = null

    val gun = new Gun(friendly)
    val welder = new Welder
    val drill = new Drill
    val grinder = new Grinder

    val healthBar = new ProgressBar(0, 100, 1, true, Assets.ui_skin)
    val oxygenBar = new ProgressBar(0, 100, 1, true, Assets.ui_skin)
    val hungerBar = new ProgressBar(0, 100, 1, true, Assets.ui_skin)
    val thirstBar = new ProgressBar(0, 100, 1, true, Assets.ui_skin)

    setupUI()

    stage.addActor(healthBar)
    stage.addActor(oxygenBar)
    stage.addActor(hungerBar)
    stage.addActor(thirstBar)

    val depleteOxygenTask = Timer.schedule(new Task {
        override def run(): Unit = {
            if(oxygen > 0)
                oxygen -= 1
        }
    }, Config.playerDrainOxygenSpeed, Config.playerDrainOxygenSpeed)
    val depleteHungerTask = Timer.schedule(new Task {
        override def run(): Unit = {
            if(hunger > 0)
                hunger -= 1
        }
    }, Config.playerDrainHungerSpeed, Config.playerDrainHungerSpeed)
    val depleteThirstTask = Timer.schedule(new Task {
        override def run(): Unit = {
            if(thirst > 0)
                thirst -= 1
        }
    }, Config.playerDrainThirstSpeed, Config.playerDrainThirstSpeed)
    val depleteHealthTask = new Task {
        override def run(): Unit = {
            if(health > 0)
                health -= 1
        }
    }

    val HEALTH_DEPLETE_NONE = 0; val HEALTH_DEPLETE_SLOW = 1; val HEALTH_DEPLETE_FAST = 2
    var healthDepleteMode = HEALTH_DEPLETE_NONE

    val upRegion = new TextureRegion(Assets.playerTexture, 0, 0, 14, 18)
    val downRegion = new TextureRegion(Assets.playerTexture, 0, 36, 14, 18)
    val leftRegion = new TextureRegion(Assets.playerTexture, 0, 18, 14, 18)
    val rightRegion = new TextureRegion(Assets.playerTexture, 0, 54, 14, 18)

    sprite = new Box2DSprite(body, rightRegion)
    acceleration = Config.playerAcceleration
    speedLimit = Config.playerSpeedLimit

    var oxygen = 100.0f
    var hunger = 100.0f
    var thirst = 100.0f

    var linearDampening = true

    override def update(): Unit = {
        if(!isAlive)
            return

        super.update()

        healthBar.setValue(health)
        oxygenBar.setValue(oxygen)
        hungerBar.setValue(hunger)
        thirstBar.setValue(thirst)

        if((thirst == 0 || hunger == 0) && oxygen > 0 && healthDepleteMode != HEALTH_DEPLETE_SLOW) {
            healthDepleteMode = HEALTH_DEPLETE_SLOW
            if(depleteHealthTask.isScheduled)
                depleteHealthTask.cancel()
            Timer.schedule(depleteHealthTask, Config.playerDrainHealthSlowSpeed, Config.playerDrainHealthSlowSpeed)
        }
        else if(oxygen == 0 && healthDepleteMode != HEALTH_DEPLETE_FAST) {
            healthDepleteMode = HEALTH_DEPLETE_FAST
            if(depleteHealthTask.isScheduled)
                depleteHealthTask.cancel()
            Timer.schedule(depleteHealthTask, Config.playerDrainHealthFastSpeed, Config.playerDrainHealthFastSpeed)
        }
        else if((thirst > 0 && hunger > 0 && oxygen > 0) && healthDepleteMode != HEALTH_DEPLETE_NONE) {
            healthDepleteMode = HEALTH_DEPLETE_NONE
            if(depleteHealthTask.isScheduled)
                depleteHealthTask.cancel()
        }

        gun.update()

        if(!inventory.isEnabled) {
            if (Gdx.input.isKeyPressed(Input.Keys.W)) {
                sprite.setRegion(upRegion)
                body.applyForceToCenter(0, acceleration, true)
            }
            if (Gdx.input.isKeyPressed(Input.Keys.S)) {
                sprite.setRegion(downRegion)
                body.applyForceToCenter(0, -acceleration, true)
            }
            if (Gdx.input.isKeyPressed(Input.Keys.A)) {
                sprite.setRegion(leftRegion)
                body.applyForceToCenter(-acceleration, 0, true)
            }
            if (Gdx.input.isKeyPressed(Input.Keys.D)) {
                sprite.setRegion(rightRegion)
                body.applyForceToCenter(acceleration, 0, true)
            }

            if (Gdx.input.isKeyJustPressed(Input.Keys.W)) {
                Assets.jetsSound.play()
            }
            if (Gdx.input.isKeyJustPressed(Input.Keys.S)) {
                Assets.jetsSound.play()
            }
            if (Gdx.input.isKeyJustPressed(Input.Keys.A)) {
                Assets.jetsSound.play()
            }
            if (Gdx.input.isKeyJustPressed(Input.Keys.D)) {
                Assets.jetsSound.play()
            }
            if (Gdx.input.isKeyJustPressed(Input.Keys.Z)) {
                linearDampening = !linearDampening
            }

            if (Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT)) {
                body.setLinearDamping(3.0f)
            }
            else if (linearDampening) {
                body.setLinearDamping(0.2f)
            }
            else {
                body.setLinearDamping(0.0f)
            }
        }

        if(Gdx.input.isKeyJustPressed(Input.Keys.E) || Gdx.input.isKeyJustPressed(Input.Keys.I)) {
            if(!inventory.isEnabled) {
                inventory.show()
                itemIO.show()
                val bk = itemHotBar.hotBarSelectedID
                itemHotBar.hotBarSelectedID = -1
                itemHotBar.select()
                itemHotBar.hotBarSelectedID = bk
            }
            else {
                inventory.hide()
                itemIO.hide()
                itemHotBar.select()
                if(machineWithInventory != null) {
                    machineWithInventory.hideInventory()
                }
            }
        }
    }

    override def render(spriteBatch: SpriteBatch): Unit = {
        if(isAlive)
            sprite.draw(spriteBatch)

        gun.render(spriteBatch)
    }

    def scrolled(amount: Int): Boolean = {
        false
    }

    def touchDown(screenX : Int, screenY : Int, pointer : Int, button : Int, camera : OrthographicCamera) : Boolean = {
        if(button == Input.Buttons.LEFT) {

            if(mouse.item != null) {
                val item = mouse.item
                item.isAlive = false
                inventory.addItem(item.ID, item.count)
                return true
            }

            if(mouse.asteroid != null) {
                mouse.asteroid match {
                    case _: AsteroidHole if Assets.spaceMode =>

                        val dialog = new Dialog("Do you wish to crawl into this hole?", Assets.ui_skin)
                        dialog.setSize(300, 50)
                        dialog.setPosition(Gdx.graphics.getWidth / 2 - 150, Gdx.graphics.getHeight / 2 - 25)
                        val yesButton = new TextButton("Yes", Assets.ui_skin)
                        val noButton = new TextButton("No", Assets.ui_skin)

                        val asteroid_x = mouse.asteroid.xID
                        val asteroid_y = mouse.asteroid.yID
                        val player = this

                        yesButton.addListener(new InputListener {
                            override def touchDown(event: InputEvent, x: Float, y: Float, pointer: Int, button: Int): Boolean = {
                                Assets.gameWorld.enterHole(asteroid_x, asteroid_y, player)
                                true
                            }
                        })

                        dialog.button(yesButton)
                        dialog.button(noButton)
                        stage.addActor(dialog)
                        return true
                    case _: FloorHole if !Assets.spaceMode =>

                        val dialog = new Dialog("Do you wish to crawl into this hole?", Assets.ui_skin)
                        dialog.setSize(300, 50)
                        dialog.setPosition(Gdx.graphics.getWidth / 2 - 150, Gdx.graphics.getHeight / 2 - 25)
                        val yesButton = new TextButton("Yes", Assets.ui_skin)
                        val noButton = new TextButton("No", Assets.ui_skin)

                        val asteroid_x = mouse.asteroid.xID
                        val asteroid_y = mouse.asteroid.yID
                        val player = this

                        yesButton.addListener(new InputListener {
                            override def touchDown(event: InputEvent, x: Float, y: Float, pointer: Int, button: Int): Boolean = {
                                Assets.gameWorld.exitHole(asteroid_x, asteroid_y, player)
                                true
                            }
                        })

                        dialog.button(yesButton)
                        dialog.button(noButton)
                        stage.addActor(dialog)
                        return true
                    case _ =>
                }
            }

            if(mouse.machine != null) {
                //Open Machine UI
                if(mouse.machine.hasGUI) {
                    if(machineWithInventory != null && inventory.isEnabled) {
                        machineWithInventory.hideInventory()
                        inventory.moveReset()
                    }

                    machineWithInventory = mouse.machine
                    machineWithInventory.showInventory(inventory)

                    inventory.show()
                    val bk = itemHotBar.hotBarSelectedID
                    itemHotBar.hotBarSelectedID = -1
                    itemHotBar.select()
                    itemHotBar.hotBarSelectedID = bk
                }

                return true
            }

            if(inventory.isEnabled)
                return false

            if(itemHotBar.selectedItem == null)
                return false

            val item = itemHotBar.selectedItem

            if(item.itemType == Item.Types.UsableType) {
                val worldPoint = camera.unproject(new Vector3(screenX, screenY, 0))
                val userPosition = new Vector2(Assets.toPfM(body.getPosition.x), Assets.toPfM(body.getPosition.y))
                val targetPosition = new Vector2(worldPoint.x, worldPoint.y)

                //Have To Be Direct Until Better Solution Presents Itself
                item.ID match {
                    case Item.IDs.GUN => gun.use(userPosition, targetPosition)
                    case Item.IDs.DRILL =>
                        drill.use(mouse)
                    case Item.IDs.WELDER =>
                    case Item.IDs.GRINDER =>
                    case Item.IDs.OXYGEN_TANK =>
                        oxygen += Config.oxygenTankRestoreValue
                        itemHotBar.selectedItem.subFromCount(1)
                    case Item.IDs.WATER_TANK =>
                        thirst += Config.waterTankRestoreValue
                        itemHotBar.selectedItem.subFromCount(1)
                    case Item.IDs.FOOD =>
                        hunger += Config.foodRestoreValue
                        itemHotBar.selectedItem.subFromCount(1)
                    case _ =>
                }

            }
            if(item.itemType == Item.Types.MachineType) {
                val worldPoint = camera.unproject(new Vector3(screenX, screenY, 0))

                Assets.gameWorld.addMachine(item.ID, stage, worldPoint.x, worldPoint.y)
                itemHotBar.selectedItem.subFromCount(1)

                return true
            }
        }
        else if(button == Input.Buttons.RIGHT) {
            if(mouse.machine != null) {
                if(mouse.machine.hasGUI) {
                    for(((x: Int, y: Int), item: InventoryItem) <- mouse.machine.inventory.slots) {
                        val count = item.count
                        val ID = item.ID

                        inventory.addItem(ID, count)
                    }
                    mouse.machine.inventory.hide()
                }

                if(itemHotBar.getNumberOfEmptySlots > 0)
                    itemHotBar.addItem(mouse.machine.id, 1)
                else
                    inventory.addItem(mouse.machine.id, 1)

                mouse.machine.dispose()
                return true
            }
        }

        false
    }

    private def setupUI(): Unit = {
        val w = Gdx.graphics.getWidth

        val barHeight = 200

        healthBar.getStyle.knobBefore = healthBar.getStyle.knob
        healthBar.setPosition(w - healthBar.getPrefWidth * 4, 0)
        healthBar.setSize(healthBar.getPrefWidth, barHeight)
        healthBar.setColor(Color.RED)

        oxygenBar.getStyle.knobBefore = oxygenBar.getStyle.knob
        oxygenBar.setPosition(w - oxygenBar.getPrefWidth * 3, 0)
        oxygenBar.setSize(oxygenBar.getPrefWidth, barHeight)
        oxygenBar.setColor(Color.CYAN)

        hungerBar.getStyle.knobBefore = hungerBar.getStyle.knob
        hungerBar.setPosition(w - hungerBar.getPrefWidth * 2, 0)
        hungerBar.setSize(hungerBar.getPrefWidth, barHeight)
        hungerBar.setColor(Color.ORANGE)

        thirstBar.getStyle.knobBefore = thirstBar.getStyle.knob
        thirstBar.setPosition(w - thirstBar.getPrefWidth, 0)
        thirstBar.setSize(thirstBar.getPrefWidth, barHeight)
        thirstBar.setColor(Color.BLUE)
    }
}