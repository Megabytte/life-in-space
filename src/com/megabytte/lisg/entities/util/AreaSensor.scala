package com.megabytte.lisg.entities.util

import com.badlogic.gdx.physics.box2d.Body
import com.megabytte.lisg.entities.Entity
import com.megabytte.lisg.util.Config

/**
  * Copyright © 2015 Keith Webb
  */
class AreaSensor(body: Body, lParentEntity: Entity, areaToAffectSize: Float = 100) {
    var sensorFixture = Config.setupFixture(body, "Circle", 0, radiusInPixels = areaToAffectSize, isSensor = true)
    sensorFixture.setUserData(this)

    var parentEntity : Entity = lParentEntity
}
