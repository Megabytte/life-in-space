package com.megabytte.lisg.entities.util

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType
import com.megabytte.lisg.entities.Entity
import com.megabytte.lisg.entities.items.GenericItem
import com.megabytte.lisg.entities.machines.Machine
import com.megabytte.lisg.util.{Assets, Config}
import com.megabytte.lisg.world.blocks.Asteroid

/**
  * Copyright © 2015 Keith Webb
  */
class Mouse {
    val position = new Vector2(Gdx.input.getX, Gdx.graphics.getHeight - Gdx.input.getY)
    val entity = new Entity

    var hasSetup = false
    setupEntity()

    def setupEntity(): Unit = {
        if(hasSetup) {
            entity.body.destroyFixture(entity.fixture)
            entity.body.getWorld.destroyBody(entity.body)
        }

        hasSetup = true
        entity.body = Config.setupBody(BodyType.DynamicBody, position.x, position.y, fixedRotation = true, 0.0f)
        entity.body.setSleepingAllowed(false) //Without this the mouse will quit working after a moment
        entity.fixture = Config.setupFixture(entity.body, "Circle", 1, radiusInPixels = 1.0f, isSensor = true)
        entity.fixture.setUserData(this)
    }

    var machine: Machine = null
    var asteroid: Asteroid = null
    var item: GenericItem = null

    def update(camera: OrthographicCamera): Unit = {
        position.set(Gdx.input.getX, Gdx.graphics.getHeight - Gdx.input.getY)
        position.add(camera.position.x - Gdx.graphics.getWidth / 2.0f, camera.position.y - Gdx.graphics.getHeight / 2.0f)

        entity.body.setTransform(Assets.toMetersFromPixels(position.x), Assets.toMetersFromPixels(position.y), 0)
    }

    def dispose(): Unit = {
        entity.dispose()
    }
}
