package com.megabytte.lisg.entities.util

import com.badlogic.gdx.physics.box2d.{ContactFilter, Fixture}
import com.megabytte.lisg.world.blocks.Asteroid

/**
  * Copyright © 2015 Keith Webb
  */
class GameContactFilter extends ContactFilter {

    override def shouldCollide(fixtureA: Fixture, fixtureB: Fixture): Boolean = {
        //Do I need this??
        val data1 = fixtureA.getUserData
        val data2 = fixtureB.getUserData

        val isD1Asteroid = data1.isInstanceOf[Asteroid]
        val isD2Asteroid = data2.isInstanceOf[Asteroid]

        if(isD1Asteroid && isD2Asteroid)
            return false

//        val isD1Bullet = data1.isInstanceOf[Bullet]
//        val isD1Machine = data1.isInstanceOf[Machine]
//        val isD1Wall = data1.isInstanceOf[Wall]
//        val isD1Hostile = data1.isInstanceOf[Hostile]
//        val isD1Asteroid = data1.isInstanceOf[Asteroid]
//        val isD1Player = data1.isInstanceOf[Player]
//        val isD1Mouse = data1.isInstanceOf[Mouse]
//        val isD1MedicStation = data1.isInstanceOf[MedicStation]

//        val isD2Bullet = data2.isInstanceOf[Bullet]
//        val isD2Machine = data2.isInstanceOf[Machine]
//        val isD2Wall = data2.isInstanceOf[Wall]
//        val isD2Hostile = data2.isInstanceOf[Hostile]
//        val isD2Asteroid = data2.isInstanceOf[Asteroid]
//        val isD2Player = data2.isInstanceOf[Player]
//        val isD2Mouse = data2.isInstanceOf[Mouse]
//        val isD2MedicStation = data2.isInstanceOf[MedicStation]

//        if(isD1Mouse || isD2Mouse)
//            return true
//        if(isD1Bullet || isD2Bullet)
//            return true
//        if(isD1Machine || isD2Machine)
//            return true
//        if(isD1Hostile || isD2Hostile)
//            return true
//        if(isD1Wall || isD2Wall)
//            return true
//        if(isD1Asteroid || isD2Asteroid)
//            return true
//        if(isD1Player || isD2Player)
//            return true



        true
    }


}
