package com.megabytte.lisg.entities.util

import com.badlogic.gdx.physics.box2d._
import com.megabytte.lisg.util.CollisionCases

/**
  * Copyright © 2015 Keith Webb
  */
class GameContactListener extends ContactListener {
    override def postSolve(contact: Contact, impulse: ContactImpulse): Unit = {}

    override def endContact(contact: Contact): Unit = {
        if (contact == null || contact.getFixtureA == null || contact.getFixtureB == null)
            return

        CollisionCases.handle(contact.getFixtureA.getUserData, contact.getFixtureB.getUserData, isStart = false)
    }

    override def preSolve(contact: Contact, oldManifold: Manifold): Unit = {}

    override def beginContact(contact: Contact): Unit = {
        if(contact == null || contact.getFixtureA == null || contact.getFixtureB == null)
            return

        CollisionCases.handle(contact.getFixtureA.getUserData, contact.getFixtureB.getUserData, isStart = true)
    }
}