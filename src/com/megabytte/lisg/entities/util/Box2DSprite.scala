package com.megabytte.lisg.entities.util

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.{Sprite, SpriteBatch, TextureRegion}
import com.badlogic.gdx.math.{MathUtils, Vector2}
import com.badlogic.gdx.physics.box2d.Body
import com.megabytte.lisg.util.Assets

/**
  * Copyright © 2015 Keith Webb
  */
class Box2DSprite(lBody: Body) {

    var sprite = new Sprite()
    var body = lBody

    def this(box2DSprite: Box2DSprite) = {
        this(box2DSprite.body)
        sprite = box2DSprite.sprite
        sprite.setOriginCenter()
    }
    def this(lBody: Body, texture: Texture) = {
        this(lBody)
        sprite = new Sprite(texture)
        sprite.setOriginCenter()
    }
    def this(lBody: Body, textureRegion: TextureRegion) = {
        this(lBody)
        sprite = new Sprite(textureRegion)
        sprite.setOriginCenter()
    }

    def draw(spriteBatch: SpriteBatch): Unit = {
        sprite.setPosition(Assets.toPixelsFromMeters(body.getPosition.x) - getOriginX,
            Assets.toPixelsFromMeters(body.getPosition.y) - getOriginY)

        sprite.setRotation(MathUtils.radiansToDegrees * body.getAngle)
        sprite.draw(spriteBatch)
    }

    def setRegion(region: TextureRegion): Unit = sprite.setRegion(region)
    def setTexture(texture: Texture): Unit = sprite.setTexture(texture)

    def setScale(scale: Float) = sprite.setScale(scale)

    def getWidth: Float = sprite.getRegionWidth
    def getHeight: Float = sprite.getRegionHeight
    def getOriginX: Float = sprite.getOriginX
    def getOriginY: Float = sprite.getOriginY

    def getPixelPosition: Vector2 = new Vector2(getPixelX, getPixelY)
    def getPhysicsPosition: Vector2 = body.getPosition
    def getPixelX: Float = sprite.getX
    def getPixelY: Float = sprite.getY
    def getPhysicsX: Float = body.getPosition.x
    def getPhysicsY: Float = body.getPosition.y


}