package com.megabytte.lisg.entities

import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.physics.box2d._
import com.megabytte.lisg.entities.util.Box2DSprite
import com.megabytte.lisg.util.{Config, Assets}

/**
  * Copyright © 2015 Keith Webb
  */
class Entity {

    var body : Body = null
    var fixture : Fixture = null
    var sprite : Box2DSprite = null

    private def checkSetup: Boolean = body != null && fixture != null && sprite != null
    private var isSetup = false

    var isAlive = true
    var isDisposed = false
    var friendly = true

    var health = 100.0f

    var speedLimit = Config.entitySpeedLimit
    var acceleration = Config.entityAcceleration

    def update(): Unit = {
        if(!isSetup) {
            isSetup = checkSetup
            if(!isSetup)
                return
        } else if(isDisposed)
            return

        if(Math.abs(body.getLinearVelocity.x) > speedLimit) {
            if(body.getLinearVelocity.x > 0)
                body.setLinearVelocity(speedLimit, body.getLinearVelocity.y)
            else
                body.setLinearVelocity(-speedLimit, body.getLinearVelocity.y)
        }
        if(Math.abs(body.getLinearVelocity.y) > speedLimit) {
            if(body.getLinearVelocity.y > 0)
                body.setLinearVelocity(body.getLinearVelocity.x, speedLimit)
            else
                body.setLinearVelocity(body.getLinearVelocity.x, -speedLimit)
        }
    }

    def render(spriteBatch: SpriteBatch): Unit = {
        if(!isSetup) {
            isSetup = checkSetup
            return
        } else if(isDisposed)
            return

        sprite.draw(spriteBatch)
    }

    def dispose(): Unit = {
        if(body != null && Assets.world != null && fixture != null) {
            body.destroyFixture(fixture)
            body.getWorld.destroyBody(body)
        }
        isSetup = false
        isDisposed = true
    }

    def areaCollide(entity: Entity): Unit = {}
    def areaSeparate(entity: Entity): Unit = {}
}
