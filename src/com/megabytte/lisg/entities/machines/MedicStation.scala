package com.megabytte.lisg.entities.machines

import com.badlogic.gdx.scenes.scene2d.Stage
import com.megabytte.lisg.entities.Entity
import com.megabytte.lisg.entities.items.Bullet
import com.megabytte.lisg.entities.player.Player
import com.megabytte.lisg.entities.util.AreaSensor
import com.megabytte.lisg.inventory.items.Item
import com.megabytte.lisg.util.{Assets, Config}

/**
  * Copyright © 2015 Keith Webb
  */
class MedicStation(stage: Stage, x: Float, y: Float) extends Machine(stage, x, y) {
    //Heals player, maybe recharge suit energy?

    id = Item.IDs.MEDIC_STATION

    val areaSensor = new AreaSensor(body, this)

    sprite.setTexture(Assets.determineTextureFromItemID(id))

    var startTime: Long = System.currentTimeMillis()
    var processTime: Long = 0
    var isEnabled = false

    override def update(): Unit = {
        super.update()

        if(!isEnabled)
            return

        processTime = System.currentTimeMillis() - startTime

        if(Config.medicStationHealRate * 1000 > processTime)
            return

        if(entityToAffect != null) {
            if(!entityToAffect.friendly && friendly)
                return

            if(entityToAffect.health + Config.medicStationHealAmount <= 100) {
                entityToAffect.health += Config.medicStationHealAmount
            }
        }
    }

    override def start(): Unit = {
        if(!isEnabled)
            isEnabled = true
    }

    override def stop(): Unit = {
        isEnabled = false
    }

    override def areaCollide(entity: Entity): Unit = {
        if(entity.isInstanceOf[Bullet])
            return

        val isPlayer = entity.isInstanceOf[Player]

        if(entityToAffect == null && isPlayer) {
            entityToAffect = entity
            start()
        }
    }
    override def areaSeparate(entity: Entity): Unit = {
        if(entity.isInstanceOf[Bullet])
            return

        val isPlayer = entity.isInstanceOf[Player]

        if(entityToAffect != null && isPlayer) {
            entityToAffect = null
            stop()
        }
    }


}
