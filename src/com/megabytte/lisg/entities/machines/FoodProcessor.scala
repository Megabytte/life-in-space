package com.megabytte.lisg.entities.machines

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.scenes.scene2d.Stage
import com.megabytte.lisg.inventory.items.Item
import com.megabytte.lisg.util.{Config, Assets}

/**
  * Copyright © 2015 Keith Webb
  */
class FoodProcessor(stage: Stage, x: Float, y: Float) extends Machine(stage, x, y) {
    //Turns space moss and alien meat into edible food

    /*  Operations
    -----------------------------------
    SPACE_MOSS => FOOD
    SPACE_MEAT => FOOD
    */

    id = Item.IDs.FOOD_PROCESSOR

    var concurrentMode = false

    createGUI(stage, "Food Processor", 3, 1, Gdx.graphics.getWidth / 8, Gdx.graphics.getHeight / 2)

    sprite.setTexture(Assets.determineTextureFromItemID(id))

    var startTime: Long = System.currentTimeMillis()
    var processTime: Long = 0

    override def update(): Unit = {
        super.update()

        processTime = System.currentTimeMillis() - startTime

        if(Config.foodProcessorTickTime * 1000 > processTime)
            return

        startTime = System.currentTimeMillis()
        processTime = 0

        val hasSpaceMeat = inventory.containsItem(Item.IDs.SPACE_MEAT)
        val hasSpaceMoss = inventory.containsItem(Item.IDs.SPACE_MOSS)

        val hasFood = inventory.containsItem(Item.IDs.FOOD)

        var emptySlots = inventory.getNumberOfEmptySlots

        if(hasSpaceMeat && hasFood) {
            inventory.getASlotWithID(Item.IDs.SPACE_MEAT).subFromCount(1)
            inventory.getASlotWithID(Item.IDs.FOOD).addToCount(1)
            if(!concurrentMode)
                return
        }
        else if(hasSpaceMeat && emptySlots >= 1) {
            inventory.getASlotWithID(Item.IDs.SPACE_MEAT).subFromCount(1)
            inventory.addItem(Item.IDs.FOOD, 1)
            if(!concurrentMode)
                return
        }

        emptySlots = inventory.getNumberOfEmptySlots

        if(hasSpaceMoss && hasFood) {
            inventory.getASlotWithID(Item.IDs.SPACE_MOSS).subFromCount(1)
            inventory.getASlotWithID(Item.IDs.FOOD).addToCount(1)
            if(!concurrentMode)
                return
        }
        else if(hasSpaceMoss && emptySlots >= 1) {
            inventory.getASlotWithID(Item.IDs.SPACE_MOSS).subFromCount(1)
            inventory.addItem(Item.IDs.FOOD, 1)
            if(!concurrentMode)
                return
        }
    }
}
