package com.megabytte.lisg.entities.machines

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.scenes.scene2d.Stage
import com.megabytte.lisg.inventory.items.Item
import com.megabytte.lisg.inventory.{CraftingGUI, Inventory}
import com.megabytte.lisg.util.{Assets, Config}

/**
  * Copyright © 2015 Keith Webb
  */
class MaterialAssembler(stage: Stage, x: Float, y: Float) extends Machine(stage, x, y) {
    //Builds all the things

    id = Item.IDs.MATERIAL_ASSEMBLER

    val craftingGUI = new CraftingGUI(stage)
    createGUI(stage, "Material Assembler", 2, 5, Gdx.graphics.getWidth - (Gdx.graphics.getWidth / 8), Gdx.graphics.getHeight / 2)

    override def showInventory(externalInventory: Inventory): Unit = {
        super.showInventory(externalInventory)
        craftingGUI.show()
    }
    override def hideInventory(): Unit = {
        super.hideInventory()
        craftingGUI.hide()
    }

    sprite.setTexture(Assets.determineTextureFromItemID(id))

    /* Operations
    ---------------------
    Queue Items From a List
    Every tick, build the item if capable
    */

    var startTime: Long = System.currentTimeMillis()
    var processTime: Long = 0

    override def update(): Unit = {
        super.update()

        processTime = System.currentTimeMillis() - startTime

        if(Config.assemblerSpeed * 1000 > processTime)
            return

        startTime = System.currentTimeMillis()
        processTime = 0

        val opt = craftingGUI.craftingQueueList.get(0)
        if(opt.isEmpty)
            return
        val top = opt.get

        val requiredMaterials = Assets.getItemRecipeFromID(top.ID)

        for((id: Int, count: Int) <- requiredMaterials) {
            if(!inventory.containsItemWithCount(id, count))
                return
        }

        if(top.ID != 0) {
            for((id: Int, count: Int) <- requiredMaterials) {
                inventory.removeCountOfItem(id, count)
            }

            inventory.addItem(top.ID, 1)
            top.subFromCount(1)
            craftingGUI.reorder()
        }
    }
}
