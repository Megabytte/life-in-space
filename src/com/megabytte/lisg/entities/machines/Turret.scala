package com.megabytte.lisg.entities.machines

import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.Stage
import com.megabytte.lisg.entities.Entity
import com.megabytte.lisg.entities.hostiles.Hostile
import com.megabytte.lisg.entities.items.{Bullet, Gun}
import com.megabytte.lisg.entities.player.Player
import com.megabytte.lisg.entities.util.AreaSensor
import com.megabytte.lisg.inventory.items.Item
import com.megabytte.lisg.util.{Assets, Config}

/**
  * Copyright © 2015 Keith Webb
  */
class Turret(stage: Stage, x: Float, y: Float, lFriendly: Boolean = true) extends Machine(stage, x, y) {
    //shoots stuff, can be friendly or hostile

    id = Item.IDs.TURRET

    val areaSensor = new AreaSensor(body, this, 500)

    friendly = lFriendly
    val gun = new Gun(friendly)
    gun.bulletSpeed *= 2

    sprite.setTexture(Assets.turretTexture)

    var startTime: Long = System.currentTimeMillis()
    var processTime: Long = 0
    var isEnabled = false

    override def update(): Unit = {
        super.update()

        gun.update()

        if(!isEnabled)
            return

        processTime = System.currentTimeMillis() - startTime

        if(Config.turretFireRate * 1000 > processTime)
            return

        startTime = System.currentTimeMillis()
        processTime = 0

        if(entityToAffect != null) {
            if(entityToAffect.friendly && friendly)
                return

            val userPosition = new Vector2(Assets.toPfM(body.getPosition.x), Assets.toPfM(body.getPosition.y))
            val targetPosition = new Vector2(entityToAffect.sprite.getPixelX, entityToAffect.sprite.getPixelY)

            gun.use(userPosition, targetPosition)
        }
    }

    override def render(spriteBatch: SpriteBatch): Unit = {
        super.render(spriteBatch)

        gun.render(spriteBatch)
    }

    override def start(): Unit = {
        if(!isEnabled)
            isEnabled = true
    }

    override def stop(): Unit = {
        isEnabled = false
    }

    override def areaCollide(entity: Entity): Unit = {
        if(entity.isInstanceOf[Bullet])
            return

        val isPlayer = entity.isInstanceOf[Player]
        val isHostile = entity.isInstanceOf[Hostile]

        if(friendly && isHostile) {
            if(entityToAffect == null) {
                entityToAffect = entity
                start()
            }
        }
        else if(!friendly && isPlayer) {
            if(entityToAffect == null) {
                entityToAffect = entity
                start()
            }
        }
    }
    override def areaSeparate(entity: Entity): Unit = {
        if(entity.isInstanceOf[Bullet])
            return

        val isPlayer = entity.isInstanceOf[Player]
        val isHostile = entity.isInstanceOf[Hostile]

        if(friendly && isHostile) {
            if(entityToAffect != null) {
                entityToAffect = null
                stop()
            }
        }
        else if(!friendly && isPlayer) {
            if(entityToAffect != null) {
                entityToAffect = null
                stop()
            }
        }

    }
}
