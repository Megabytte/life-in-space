package com.megabytte.lisg.entities.machines

import com.badlogic.gdx.physics.box2d.BodyDef.BodyType
import com.badlogic.gdx.scenes.scene2d.Stage
import com.megabytte.lisg.entities.Entity
import com.megabytte.lisg.entities.util.Box2DSprite
import com.megabytte.lisg.inventory.Inventory
import com.megabytte.lisg.inventory.items.Item
import com.megabytte.lisg.util.{Config, Assets}

/**
  * Copyright © 2015 Keith Webb
  */
class Machine(stage: Stage, x : Float, y : Float) extends Entity {
    val size = 8

    var id = Item.IDs.GENERIC_MACHINE

    var hasGUI = false
    var inventory : Inventory = null
    def createGUI(stage: Stage, windowTitle: String, widthInSlots: Int, heightInSlots: Int, centerX: Int, centerY: Int): Unit = {
        hasGUI = true
        inventory = new Inventory(stage, windowTitle, widthInSlots, heightInSlots, centerX, centerY)
    }

    def showInventory(externalInventory: Inventory): Unit = {
        if(hasGUI) {
            inventory.externalInventory = externalInventory
            inventory.show()
        }
    }
    def hideInventory(): Unit = {
        if(hasGUI) {
            inventory.hide()
            inventory.externalInventory = null
        }
    }

    // Use Big Box2D Sensor Circles for the Area of Effect

    body = Config.setupBody(BodyType.DynamicBody, x, y, fixedRotation = false, 0.2f)
    body.setAngularDamping(0.1f)
    fixture = Config.setupFixture(body, "Square", 30f, size, size) //Changed Density From 0.01f
    fixture.setUserData(this)

    sprite = new Box2DSprite(body, Assets.genericMachineTexture)

    def pointInside(x : Float, y : Float): Boolean = {
        val bodyX = body.getPosition.x
        val bodyY = body.getPosition.y

        val sizeX = Assets.toMetersFromPixels(size)
        val sizeY = sizeX

        if(x >= bodyX - sizeX && x <= bodyX + sizeX) {
            if(y >= bodyY - sizeY && y <= bodyY + sizeY) {
                return true
            }
        }

        false
    }

    var entityToAffect: Entity = null
    def start(): Unit = {}
    def stop(): Unit = {}

    override def dispose(): Unit = {
        isAlive = false
    }
}
