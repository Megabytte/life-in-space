package com.megabytte.lisg.entities.machines

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.scenes.scene2d.Stage
import com.megabytte.lisg.inventory.items.Item
import com.megabytte.lisg.util.{Assets, Config}

/**
  * Copyright © 2015 Keith Webb
  */
class OreRefinery(stage: Stage, x: Float, y: Float) extends Machine(stage, x, y) {
    //refines ores into metals for the material assembler

    /*  Operations
    -----------------------------------
    ORE_ROCK => GRAVEL
    ORE_IRON => IRON_INGOT
    ORE_GOLD => GOLD_INGOT
    ORE_URANIUM => URANIUM_INGOT
    ORE_COBALT => COBALT_INGOT
    ORE_NICKEL => NICKEL_INGOT
    ORE_SILVER => SILVER_INGOT
    ORE_PLATINUM => PLATINUM_INGOT
    ORE_MAGNESIUM => MAGNESIUM_POWDER
    ORE_SILICON => SILICON_WAFER
    */

    id = Item.IDs.ORE_REFINERY

    var concurrentMode = false

    createGUI(stage, "Ore Refinery", 2, 5, Gdx.graphics.getWidth / 8, Gdx.graphics.getHeight / 2)

    sprite.setTexture(Assets.determineTextureFromItemID(id))

    var startTime: Long = System.currentTimeMillis()
    var processTime: Long = 0

    override def update(): Unit = {
        super.update()

        processTime = System.currentTimeMillis() - startTime

        if(Config.oreRefineryTickTime * 1000 > processTime)
            return

        startTime = System.currentTimeMillis()
        processTime = 0

        val hasRock = inventory.containsItem(Item.IDs.ROCK)
        val hasOreIron = inventory.containsItem(Item.IDs.ORE_IRON)
        val hasOreGold = inventory.containsItem(Item.IDs.ORE_GOLD)
        val hasOreUranium = inventory.containsItem(Item.IDs.ORE_URANIUM)
        val hasOreCobalt = inventory.containsItem(Item.IDs.ORE_COBALT)
        val hasOreNickel = inventory.containsItem(Item.IDs.ORE_NICKEL)
        val hasOreSilver = inventory.containsItem(Item.IDs.ORE_SILVER)
        val hasOrePlatinum = inventory.containsItem(Item.IDs.ORE_PLATINUM)
        val hasOreMagnesium = inventory.containsItem(Item.IDs.ORE_MAGNESIUM)
        val hasOreSilicon = inventory.containsItem(Item.IDs.ORE_SILICON)

        val hasGravel = inventory.containsItem(Item.IDs.GRAVEL)
        val hasIronIngot = inventory.containsItem(Item.IDs.IRON_INGOT)
        val hasGoldIngot = inventory.containsItem(Item.IDs.GOLD_INGOT)
        val hasUraniumIngot = inventory.containsItem(Item.IDs.URANIUM_INGOT)
        val hasCobaltIngot = inventory.containsItem(Item.IDs.COBALT_INGOT)
        val hasNickelIngot = inventory.containsItem(Item.IDs.NICKEL_INGOT)
        val hasSilverIngot = inventory.containsItem(Item.IDs.SILVER_INGOT)
        val hasPlatinumIngot = inventory.containsItem(Item.IDs.PLATINUM_INGOT)
        val hasMagnesiumPowder = inventory.containsItem(Item.IDs.MAGNESIUM_POWDER)
        val hasSiliconWafer= inventory.containsItem(Item.IDs.SILICON_WAFER)

        var emptySlots = inventory.getNumberOfEmptySlots

        if(hasRock && hasGravel) {
            inventory.getASlotWithID(Item.IDs.ROCK).subFromCount(1)
            inventory.getASlotWithID(Item.IDs.GRAVEL).addToCount(1)
            if(!concurrentMode)
                return
        }
        else if(hasRock && emptySlots >= 1) {
            inventory.getASlotWithID(Item.IDs.ROCK).subFromCount(1)
            inventory.addItem(Item.IDs.GRAVEL, 1)
            if(!concurrentMode)
                return
        }

        emptySlots = inventory.getNumberOfEmptySlots

        if(hasOreIron && hasIronIngot) {
            inventory.getASlotWithID(Item.IDs.ORE_IRON).subFromCount(1)
            inventory.getASlotWithID(Item.IDs.IRON_INGOT).addToCount(1)
            if(!concurrentMode)
                return
        }
        else if(hasOreIron && emptySlots >= 1) {
            inventory.getASlotWithID(Item.IDs.ORE_IRON).subFromCount(1)
            inventory.addItem(Item.IDs.IRON_INGOT, 1)
            if(!concurrentMode)
                return
        }

        emptySlots = inventory.getNumberOfEmptySlots

        if(hasOreGold && hasGoldIngot) {
            inventory.getASlotWithID(Item.IDs.ORE_GOLD).subFromCount(1)
            inventory.getASlotWithID(Item.IDs.GOLD_INGOT).addToCount(1)
            if(!concurrentMode)
                return
        }
        else if(hasOreGold && emptySlots >= 1) {
            inventory.getASlotWithID(Item.IDs.ORE_GOLD).subFromCount(1)
            inventory.addItem(Item.IDs.GOLD_INGOT, 1)
            if(!concurrentMode)
                return
        }

        emptySlots = inventory.getNumberOfEmptySlots

        if(hasOreUranium && hasUraniumIngot) {
            inventory.getASlotWithID(Item.IDs.ORE_URANIUM).subFromCount(1)
            inventory.getASlotWithID(Item.IDs.URANIUM_INGOT).addToCount(1)
            if(!concurrentMode)
                return
        }
        else if(hasOreUranium && emptySlots >= 1) {
            inventory.getASlotWithID(Item.IDs.ORE_URANIUM).subFromCount(1)
            inventory.addItem(Item.IDs.URANIUM_INGOT, 1)
            if(!concurrentMode)
                return
        }

        emptySlots = inventory.getNumberOfEmptySlots

        if(hasOreCobalt && hasCobaltIngot) {
            inventory.getASlotWithID(Item.IDs.ORE_COBALT).subFromCount(1)
            inventory.getASlotWithID(Item.IDs.COBALT_INGOT).addToCount(1)
            if(!concurrentMode)
                return
        }
        else if(hasOreCobalt && emptySlots >= 1) {
            inventory.getASlotWithID(Item.IDs.ORE_COBALT).subFromCount(1)
            inventory.addItem(Item.IDs.COBALT_INGOT, 1)
            if(!concurrentMode)
                return
        }

        emptySlots = inventory.getNumberOfEmptySlots

        if(hasOreNickel && hasNickelIngot) {
            inventory.getASlotWithID(Item.IDs.ORE_NICKEL).subFromCount(1)
            inventory.getASlotWithID(Item.IDs.NICKEL_INGOT).addToCount(1)
            if(!concurrentMode)
                return
        }
        else if(hasOreNickel && emptySlots >= 1) {
            inventory.getASlotWithID(Item.IDs.ORE_NICKEL).subFromCount(1)
            inventory.addItem(Item.IDs.NICKEL_INGOT, 1)
            if(!concurrentMode)
                return
        }

        emptySlots = inventory.getNumberOfEmptySlots

        if(hasOreSilver && hasSilverIngot) {
            inventory.getASlotWithID(Item.IDs.ORE_SILVER).subFromCount(1)
            inventory.getASlotWithID(Item.IDs.SILVER_INGOT).addToCount(1)
            if(!concurrentMode)
                return
        }
        else if(hasOreSilver && emptySlots >= 1) {
            inventory.getASlotWithID(Item.IDs.ORE_SILVER).subFromCount(1)
            inventory.addItem(Item.IDs.SILVER_INGOT, 1)
            if(!concurrentMode)
                return
        }

        emptySlots = inventory.getNumberOfEmptySlots

        if(hasOrePlatinum && hasPlatinumIngot) {
            inventory.getASlotWithID(Item.IDs.ORE_PLATINUM).subFromCount(1)
            inventory.getASlotWithID(Item.IDs.PLATINUM_INGOT).addToCount(1)
            if(!concurrentMode)
                return
        }
        else if(hasOrePlatinum && emptySlots >= 1) {
            inventory.getASlotWithID(Item.IDs.ORE_PLATINUM).subFromCount(1)
            inventory.addItem(Item.IDs.PLATINUM_INGOT, 1)
            if(!concurrentMode)
                return
        }

        emptySlots = inventory.getNumberOfEmptySlots

        if(hasOreMagnesium && hasMagnesiumPowder) {
            inventory.getASlotWithID(Item.IDs.ORE_MAGNESIUM).subFromCount(1)
            inventory.getASlotWithID(Item.IDs.MAGNESIUM_POWDER).addToCount(1)
            if(!concurrentMode)
                return
        }
        else if(hasOreMagnesium && emptySlots >= 1) {
            inventory.getASlotWithID(Item.IDs.ORE_MAGNESIUM).subFromCount(1)
            inventory.addItem(Item.IDs.MAGNESIUM_POWDER, 1)
            if(!concurrentMode)
                return
        }

        emptySlots = inventory.getNumberOfEmptySlots

        if(hasOreSilicon && hasSiliconWafer) {
            inventory.getASlotWithID(Item.IDs.ORE_SILICON).subFromCount(1)
            inventory.getASlotWithID(Item.IDs.SILICON_WAFER).addToCount(1)
            if(!concurrentMode)
                return
        }
        else if(hasOreSilicon && emptySlots >= 1) {
            inventory.getASlotWithID(Item.IDs.ORE_SILICON).subFromCount(1)
            inventory.addItem(Item.IDs.SILICON_WAFER, 1)
            if(!concurrentMode)
                return
        }
    }
}