package com.megabytte.lisg.entities.machines

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.scenes.scene2d.Stage
import com.megabytte.lisg.inventory.items.Item
import com.megabytte.lisg.util.Assets

/**
  * Copyright © 2015 Keith Webb
  */
class Generator(stage: Stage, x: Float, y: Float) extends Machine(stage, x, y) {
    //EXPERIMENTAL CLASS IDEA
    //Provides power for other structures

    /* Ideas
    -------
    Connect to entities with wires
    Battery Class to store power
    Solar Panel Class to generate power without uranium
    */

    id = Item.IDs.GENERATOR

    createGUI(stage, "Generator", 1, 1, Gdx.graphics.getWidth / 8, Gdx.graphics.getHeight / 2)

    sprite.setTexture(Assets.generatorTexture)
}
