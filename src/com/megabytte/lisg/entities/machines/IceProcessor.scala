package com.megabytte.lisg.entities.machines

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.scenes.scene2d.Stage
import com.megabytte.lisg.inventory.items.Item
import com.megabytte.lisg.util.{Assets, Config}

/**
  * Copyright © 2015 Keith Webb
  */
class IceProcessor(stage: Stage, x: Float, y: Float) extends Machine(stage, x, y) {
    //Gives Oxygen and Water From Ice

    /*  Operations
    -----------------------------------
    ICE => WATER_TANK AND OXYGEN_TANK
    */

    id = Item.IDs.ICE_PROCESSOR

    createGUI(stage, "Ice Processor", 3, 1, Gdx.graphics.getWidth / 8, Gdx.graphics.getHeight / 2)

    sprite.setTexture(Assets.determineTextureFromItemID(id))

    var startTime: Long = System.currentTimeMillis()
    var processTime: Long = 0

    override def update(): Unit = {
        super.update()

        processTime = System.currentTimeMillis() - startTime

        if(Config.iceProcessorTickTime * 1000 > processTime)
            return

        startTime = System.currentTimeMillis()
        processTime = 0

        val hasIce = inventory.containsItem(Item.IDs.ICE)

        val hasOxygenTank = inventory.containsItem(Item.IDs.OXYGEN_TANK)
        val hasWaterTank = inventory.containsItem(Item.IDs.WATER_TANK)

        val emptySlots = inventory.getNumberOfEmptySlots

        if(hasIce && hasOxygenTank && hasWaterTank) {
            inventory.getASlotWithID(Item.IDs.ICE).subFromCount(1)
            inventory.getASlotWithID(Item.IDs.OXYGEN_TANK).addToCount(1)
            inventory.getASlotWithID(Item.IDs.WATER_TANK).addToCount(1)
        }
        else if(hasIce && hasOxygenTank && emptySlots >= 1) {
            inventory.getASlotWithID(Item.IDs.ICE).subFromCount(1)
            inventory.getASlotWithID(Item.IDs.OXYGEN_TANK).addToCount(1)
            inventory.addItem(Item.IDs.WATER_TANK, 1)
        }
        else if(hasIce && hasWaterTank && emptySlots >= 1) {
            inventory.getASlotWithID(Item.IDs.ICE).subFromCount(1)
            inventory.getASlotWithID(Item.IDs.WATER_TANK).addToCount(1)
            inventory.addItem(Item.IDs.OXYGEN_TANK, 1)
        }
        else if(hasIce && emptySlots >= 2) {
            inventory.getASlotWithID(Item.IDs.ICE).subFromCount(1)
            inventory.addItem(Item.IDs.WATER_TANK, 1)
            inventory.addItem(Item.IDs.OXYGEN_TANK, 1)
        }
    }
}
