package com.megabytte.lisg.entities.machines

import com.badlogic.gdx.scenes.scene2d.Stage
import com.megabytte.lisg.inventory.items.Item
import com.megabytte.lisg.states.GameOverState
import com.megabytte.lisg.states.gsm.GameStateManager
import com.megabytte.lisg.util.{Config, Assets}

/**
  * Copyright © 2015 Keith Webb
  */
class DistressBeacon(stage: Stage, x: Float, y: Float) extends Machine(stage, x, y) {
    //End Game Goal Item
    id = Item.IDs.DISTRESS_BEACON

    sprite.setTexture(Assets.determineTextureFromItemID(id))

    var startTime: Long = System.currentTimeMillis()
    var timeAlive: Long = 0

    override def update(): Unit = {
        super.update()

        timeAlive = System.currentTimeMillis() - startTime

        if(Config.distressBeaconWinTime * 1000 <= timeAlive) {
            GameStateManager.changeState(new GameOverState)
        }
    }
}
