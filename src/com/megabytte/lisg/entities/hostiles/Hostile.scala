package com.megabytte.lisg.entities.hostiles

import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.{MathUtils, Vector2}
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType
import com.megabytte.lisg.entities.Entity
import com.megabytte.lisg.entities.items.{Bullet, Gun}
import com.megabytte.lisg.entities.machines.Machine
import com.megabytte.lisg.entities.player.Player
import com.megabytte.lisg.entities.util.{AreaSensor, Box2DSprite}
import com.megabytte.lisg.util.{Assets, CollisionCases, Config}

/**
  * Copyright © 2015 Keith Webb
  */
class Hostile(x : Float, y : Float) extends Entity {
    body = Config.setupBody(BodyType.DynamicBody, x, y, fixedRotation = true, 0.2f)
    fixture = Config.setupFixture(body, "Square", 10.0f, 8.0f, 8.0f)
    fixture.setUserData(this)
    sprite = new Box2DSprite(body, Assets.genericMachineTexture)

    val areaSensor = new AreaSensor(body, this, 500)

    friendly = false
    val gun = new Gun(friendly)

    val STATE_WANDER = 1; val STATE_CHASE = 2; val STATE_RUN = 3
    var currentState = STATE_WANDER

    acceleration = Config.hostileAcceleration
    speedLimit = Config.hostileSpeedLimit

    var direction = new Vector2(MathUtils.random(-100, 100), MathUtils.random(-100, 100))
    direction.nor().scl(acceleration)

    var target: Entity = null

    var startTime: Long = System.currentTimeMillis()
    var processTime: Long = 0
    var isEnabled = true

    override def update(): Unit = {
        if(!isAlive)
            return

        super.update()

        gun.update()

        if(health <= 0) {
            dispose()
            Assets.destroySound.play()
            return
        }

        if(health <= 20 && target != null)  {
            currentState = STATE_RUN
        } else if(currentState != STATE_CHASE) {
            currentState = STATE_WANDER
        }

        if(currentState == STATE_WANDER) {
            body.applyForceToCenter(direction, true)
        } else if(currentState == STATE_CHASE) {
            //Follow Target
            val targetPos = target.body.getPosition
            val finalVec = targetPos.sub(body.getPosition).nor().scl(acceleration)

            body.applyForceToCenter(finalVec, true)
        } else if(currentState == STATE_RUN) {
            //Avoid Player
            if(target == null)
                currentState = STATE_WANDER

            val targetPos = target.body.getPosition
            val finalVec = targetPos.sub(body.getPosition).nor().scl(acceleration)

            finalVec.set(-finalVec.x, -finalVec.y)

            body.applyForceToCenter(finalVec, true)
        }

        processTime = System.currentTimeMillis() - startTime

        if(!isEnabled)
            return

        if(Config.hostileFireRate * 1000 > processTime)
            return

        startTime = System.currentTimeMillis()
        processTime = 0

        if(currentState == STATE_WANDER) {
            //Go In Random Direction
            direction.set(MathUtils.random(-100, 100), MathUtils.random(-100, 100))
            direction.nor().scl(acceleration)
        } else {
            //Fire Bullet Toward Target
            if(currentState == STATE_RUN || target == null)
                return

            val userPosition = new Vector2(Assets.toPfM(body.getPosition.x), Assets.toPfM(body.getPosition.y))
            val targetPosition = new Vector2(target.sprite.getPixelX, target.sprite.getPixelY)
            gun.use(userPosition, targetPosition)
        }
    }

    override def render(spriteBatch: SpriteBatch): Unit = {
        if(isAlive) {
            super.render(spriteBatch)
            gun.render(spriteBatch)
        }
    }

    override def dispose(): Unit = {
        isAlive = false
        Assets.world.destroyBody(body)
    }

    override def areaCollide(entity: Entity): Unit = {
        if(entity.isInstanceOf[Bullet])
            return

        val isPlayer = entity.isInstanceOf[Player]
        val isMachine = entity.isInstanceOf[Machine]
        val isFriendly = entity.friendly

        if(isPlayer) {
            target = entity
            currentState = STATE_CHASE
        } else if(isMachine && !target.isInstanceOf[Player]) {
            if(isFriendly) {
                target = entity
                currentState = STATE_CHASE
            }
        }
    }
    override def areaSeparate(entity: Entity): Unit = {
        if(entity.isInstanceOf[Bullet])
            return

        val isPlayer = entity.isInstanceOf[Player]
        val isMachine = entity.isInstanceOf[Machine]
        val isFriendly = entity.friendly

        if(isPlayer) {
            if(target == entity) {
                if(CollisionCases.collisions.contains(this)) {
                    for(thing <- CollisionCases.collisions(this)) {
                        val isBullet = thing.isInstanceOf[Bullet]
                        val isMachine = thing.isInstanceOf[Machine]

                        if(!isBullet && isMachine) {
                            val machine = thing.asInstanceOf[Machine]
                            if(machine.friendly) {
                                target = machine
                                currentState = STATE_CHASE
                                return
                            }
                        }
                    }
                }
                else {
                    target = null
                    currentState = STATE_WANDER
                }
            }
        }
        else if(isMachine) {
            if(isFriendly) {
                if(target != null && !target.isInstanceOf[Player]) {
                    target = null
                    currentState = STATE_WANDER
                }
            }
        }
    }
}
