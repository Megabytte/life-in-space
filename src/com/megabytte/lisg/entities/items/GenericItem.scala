package com.megabytte.lisg.entities.items

import com.badlogic.gdx.physics.box2d.BodyDef.BodyType
import com.megabytte.lisg.entities.Entity
import com.megabytte.lisg.entities.util.Box2DSprite
import com.megabytte.lisg.util.{Assets, Config}

/**
  * Copyright © 2015 Keith Webb
  */
class GenericItem(x: Float, y: Float, id: Int, lCount: Int) extends Entity {
    //Physics Object
    var ID = id
    var count = lCount
    body = Config.setupBody(BodyType.DynamicBody, x, y, fixedRotation = false, 0.2f)
    body.setAngularDamping(0.1f)
    fixture = Config.setupFixture(body, "Square", 10f, 8, 8)
    fixture.setUserData(this)
    sprite = new Box2DSprite(body, Assets.determineTextureFromItemID(ID))
    //sprite.setScale(0.5f)
}