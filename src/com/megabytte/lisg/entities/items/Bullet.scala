package com.megabytte.lisg.entities.items

import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.{MathUtils, Vector2}
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType
import com.megabytte.lisg.entities.Entity
import com.megabytte.lisg.entities.util.Box2DSprite
import com.megabytte.lisg.util.{Assets, Config}

/**
  * Copyright © 2015 Keith Webb
  */
class Bullet(angle : Float, position : Vector2, velocity : Vector2, lFriendly : Boolean) extends Entity {
    friendly = lFriendly
    body = Config.setupBody(BodyType.DynamicBody, position.x, position.y, fixedRotation = true, 0.0f)
    body.setBullet(true)
    body.setTransform(body.getPosition, angle * MathUtils.degreesToRadians)
    body.setLinearVelocity(velocity)
    fixture = Config.setupFixture(body, "Square", 10.0f, 2.5f, 5.0f, isSensor = true)
    fixture.setUserData(this)
    isAlive = true
    sprite = new Box2DSprite(body, Assets.bulletTexture)

    var startTime: Long = System.currentTimeMillis()
    var timeAlive: Long = 0

    override def update(): Unit = {
        if(!isAlive)
            return

        timeAlive = System.currentTimeMillis() - startTime

        if(Config.bulletDeathTime * 1000 <= timeAlive)
            dispose()
    }

    override def render(spriteBatch: SpriteBatch): Unit = {
        if(isAlive) {
            super.render(spriteBatch)
        }
    }

    override def dispose(): Unit = {
        isAlive = false
    }

}
