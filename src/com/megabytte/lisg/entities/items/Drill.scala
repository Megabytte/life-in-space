package com.megabytte.lisg.entities.items

import com.megabytte.lisg.entities.util.Mouse
import com.megabytte.lisg.inventory.items.Item
import com.megabytte.lisg.util.Assets
import com.megabytte.lisg.world.blocks.AsteroidHole

/**
  * Copyright © 2015 Keith Webb
  */
class Drill {

    def use(mouse: Mouse): Unit = {
        val asteroid = mouse.asteroid
        if(asteroid != null) {
            if(asteroid.isInstanceOf[AsteroidHole])
                return

            val xID = asteroid.xID
            val yID = asteroid.yID

            if(Assets.spaceMode) {
                Assets.gameWorld.changeTile(xID, yID, "Asteroid Hole")

                val asteroidHole = new AsteroidHole(asteroid.body, false)
                asteroidHole.fixture = asteroid.fixture
                asteroidHole.sensorFixture = asteroid.sensorFixture
                asteroidHole.sensorFixture.setUserData(asteroidHole)
                mouse.asteroid = asteroidHole

                //println("Drill: " + xID + " " + yID)
            } else {
                val world = Assets.gameWorld
                val tileName = world.getTileName(xID, yID)
                world.changeTile(xID, yID, "Floor")

                asteroid.fixture.setUserData(null)
                asteroid.sensorFixture.setUserData(null)
                world.box2DWorld.destroyBody(asteroid.body)
                mouse.asteroid = null

                //Add 16 to center item on tile center
                val pixelX = Assets.toPfM(asteroid.body.getPosition.x) + 16
                val pixelY = Assets.toPfM(asteroid.body.getPosition.y) + 16

                if(tileName == "Asteroid")
                    world.addItem(pixelX, pixelY, Item.IDs.ROCK, 1)
                else if(tileName == "Iron Ore")
                    world.addItem(pixelX, pixelY, Item.IDs.ORE_IRON, 1)
                else if(tileName == "Cobalt Ore")
                    world.addItem(pixelX, pixelY, Item.IDs.ORE_COBALT, 1)
                else if(tileName == "Gold Ore")
                    world.addItem(pixelX, pixelY, Item.IDs.ORE_GOLD, 1)
                else if(tileName == "Nickel Ore")
                    world.addItem(pixelX, pixelY, Item.IDs.ORE_NICKEL, 1)
                else if(tileName == "Platinum Ore")
                    world.addItem(pixelX, pixelY, Item.IDs.ORE_PLATINUM, 1)
                else if(tileName == "Magnesium Ore")
                    world.addItem(pixelX, pixelY, Item.IDs.ORE_MAGNESIUM, 1)
                else if(tileName == "Silicon Ore")
                    world.addItem(pixelX, pixelY, Item.IDs.ORE_SILICON, 1)
                else if(tileName == "Silver Ore")
                    world.addItem(pixelX, pixelY, Item.IDs.ORE_SILVER, 1)
                else if(tileName == "Uranium Ore")
                    world.addItem(pixelX, pixelY, Item.IDs.ORE_URANIUM, 1)
                else if(tileName == "Ice")
                    world.addItem(pixelX, pixelY, Item.IDs.ICE, 1)
            }
        }
    }
}