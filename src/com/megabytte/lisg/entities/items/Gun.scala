package com.megabytte.lisg.entities.items

import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.Vector2
import com.megabytte.lisg.util.{Config, Assets}

import scala.collection.mutable

/**
  * Copyright © 2015 Keith Webb
  */
class Gun(lFriendly : Boolean) {
    var bullets = new mutable.HashSet[Bullet]()
    var bulletSpeed = Config.bulletSpeed

    var friendly = lFriendly

    var lastAdded: Bullet = null

    def use(userPosition: Vector2, targetPosition: Vector2): Unit = {
        val userPositionBK = userPosition.cpy()
        var finalVec = userPosition sub targetPosition
        val angle = finalVec.angle() + 90

        finalVec = finalVec.nor.scl(bulletSpeed)
        finalVec.set(-finalVec.x, -finalVec.y)

        val bullet = new Bullet(angle, userPositionBK, finalVec, friendly)
        lastAdded = bullet

        bullets += bullet
        Assets.laserSound.play()
    }

    def update(): Unit = {
        for(bullet: Bullet <- bullets) {
            if(!bullet.isAlive) {
                Assets.world.destroyBody(bullet.body)
                bullets -= bullet
            }
            else {
                bullet.update()
            }
        }
    }

    def render(spriteBatch: SpriteBatch): Unit = {
        for(bullet: Bullet <- bullets) {
            bullet.render(spriteBatch)
        }
    }
}