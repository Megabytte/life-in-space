package com.megabytte.lisg.world

import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.maps.tiled.TiledMap
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.World
import com.badlogic.gdx.scenes.scene2d.Stage
import com.megabytte.lisg.entities.hostiles.Hostile
import com.megabytte.lisg.entities.items.GenericItem
import com.megabytte.lisg.entities.machines.Machine
import com.megabytte.lisg.entities.player.Player
import com.megabytte.lisg.entities.util.{GameContactFilter, GameContactListener}
import com.megabytte.lisg.util.{Assets, Config}
import com.megabytte.lisg.world.blocks._
import net.dermetfan.gdx.physics.box2d.Box2DMapObjectParser

import scala.collection.mutable

/**
  * Copyright © 2015 Keith Webb
  */
class MapChunk(aMap: TiledMap, lName: String) {

    val map = aMap
    val name = lName
    val mapRenderer = new OrthogonalTiledMapRenderer(map)
    val box2DWorld = new World(new Vector2(0, 0), true)
    var parser: Box2DMapObjectParser = null
    val contactListener = new GameContactListener
    val contactFilter = new GameContactFilter
    box2DWorld.setContactListener(contactListener)
    box2DWorld.setContactFilter(contactFilter)

    def setupPlayer(player: Player, posXInPixels: Int, posYInPixels: Int, usePos: Boolean = true): Unit = {
        player.location = name
        val playerBodyDef = Config.createBodyDefFromBody(player.body)
        if(usePos)
            playerBodyDef.position.set(Assets.toMfP(posXInPixels), Assets.toMfP(posYInPixels))
        playerBodyDef.linearVelocity.set(0, 0)

        player.body.destroyFixture(player.fixture)
        player.body.getWorld.destroyBody(player.body)

        player.body = box2DWorld.createBody(playerBodyDef)
        player.fixture = Config.setupFixture(player.body, "Square", 30, 7, 9)
        player.fixture.setUserData(player)
        player.sprite.body = player.body

        player.mouse.setupEntity()
    }

    var hostiles = new mutable.HashSet[Hostile]()
    var items = new mutable.HashSet[GenericItem]()
    var machines = new mutable.HashSet[Machine]()

    def initializePhysics(): Unit = {
        parser = new Box2DMapObjectParser(1.0f / Assets.PIXELS_TO_METERS)
        parser.load(box2DWorld, map)

        val iterator = parser.getFixtures.keys()
        while (iterator.hasNext()) {
            val key = iterator.next()
            val value = parser.getFixtures.get(key)

            if (key.contains("Asteroid Hole")) {
                new AsteroidHole(value.getBody, true)
            }
            else if (key.contains("Asteroid")) {
                new Asteroid(value.getBody, true)
            }
            else if (key.contains("Wall")) {
                new Wall(value.getBody, true)
            }
            else if (key.contains("Floor Hole")) {
                new FloorHole(value.getBody, true)
            }
            else if (key.contains("Ore")) {
                new Ore(value.getBody, key, true)
            }
            else if (key.contains("Ice")) {
                new Ice(value.getBody, true)
            }
        }
    }

    def addHostile(x : Float, y : Float): Hostile = {
        val hostile = new Hostile(x, y)
        hostiles += hostile
        hostile
    }
    def addItem(x : Float, y : Float, ID: Int, count: Int): GenericItem = {
        val item = new GenericItem(x, y, ID, count)
        items += item
        item
    }
    def addMachine(ID: Int, stage: Stage, x : Float, y : Float): Machine = {
        val machine = Assets.newMachineFromItemID(ID, stage, x, y)
        machines += machine
        machine
    }

    def update(deltaTime: Float, camera: OrthographicCamera): Unit = {
        box2DWorld.step(1.0f / 60.0f, 6, 2)
        mapRenderer.setView(camera)

        for(item: GenericItem <- items) {
            if(!item.isAlive) {
                Assets.world.destroyBody(item.body)
                items -= item
            }
            else
                item.update()
        }
        for(machine: Machine <- machines) {
            if(!machine.isAlive) {
                Assets.world.destroyBody(machine.body)
                machines -= machine
            }
            else
                machine.update()
        }
        for(hostile: Hostile <- hostiles) {
            if(!hostile.isAlive)
                hostiles -= hostile
            else
                hostile.update()
        }
    }

    def renderMap(): Unit = {
        mapRenderer.render()
    }

    def renderEntities(spriteBatch: SpriteBatch): Unit = {
        for(item: GenericItem <- items) {
            item.render(spriteBatch)
        }
        for(machine: Machine <- machines) {
            machine.render(spriteBatch)
        }
        for(hostile: Hostile <- hostiles) {
            hostile.render(spriteBatch)
        }
    }

    def dispose(): Unit = {
        for(hostile: Hostile <- hostiles) {
            hostile.dispose()
        }
        for(item: GenericItem <- items) {
            item.dispose()
        }
        for(machine: Machine <- machines) {
            machine.dispose()
        }

        box2DWorld.dispose()
        mapRenderer.dispose()
    }
}
