package com.megabytte.lisg.world

/**
  * Copyright © 2015 Keith Webb
  */
class WorldGenerationParameters {
    //I have no idea what values to put here

    var seed : Long = System.currentTimeMillis()
    var tileSize = 32
    var chunkWidth = 100
    var chunkHeight = 100

    //Rates control Size and Density

    //Asteroid Parameters
    var asteroidRateLower = 0.7f
    var asteroidRateUpper = 1.0f
    
    val oreLower = 0.75f

    //Resource Rates
    var ironRateLower = oreLower
    var ironRateUpper = 1.0f

    var cobaltRateLower = oreLower
    var cobaltRateUpper = 1.0f

    var goldRateLower = oreLower
    var goldRateUpper = 1.0f

    var nickelRateLower = oreLower
    var nickelRateUpper = 1.0f

    var platinumRateLower = oreLower
    var platinumRateUpper = 1.0f

    var magnesiumRateLower = oreLower
    var magnesiumRateUpper = 1.0f

    var siliconRateLower = oreLower
    var siliconRateUpper = 1.0f

    var silverRateLower = oreLower
    var silverRateUpper = 1.0f

    var uraniumRateLower = oreLower
    var uraniumRateUpper = 1.0f

    var iceRateLower = oreLower
    var iceRateUpper = 1.0f

    var mossRateLower = oreLower
    var mossRateUpper = 1.0f

    //Hostile Rate
    var raiderRate = 1
    var predatorRate = 1
}
