package com.megabytte.lisg.world.blocks

import com.badlogic.gdx.physics.box2d.Body

/**
  * Copyright © 2015 Keith Webb
  */
class Ice(lBody: Body, createSensor: Boolean) extends Asteroid(lBody, createSensor) {
    id = 2
}
