package com.megabytte.lisg.world.blocks

import com.badlogic.gdx.physics.box2d.Body

/**
  * Copyright © 2015 Keith Webb
  */
class Wall(lBody: Body, createSensor: Boolean) extends Block(lBody, createSensor) {
    id = 4
}
