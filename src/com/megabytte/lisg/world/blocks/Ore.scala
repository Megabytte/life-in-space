package com.megabytte.lisg.world.blocks

import com.badlogic.gdx.physics.box2d.Body

/**
  * Copyright © 2015 Keith Webb
  */
class Ore(lBody: Body, name: String, createSensor: Boolean) extends Asteroid(lBody, createSensor) {
    /* Ore Types

        0 = Iron Ore
        1 = Cobalt Ore
        2 = Gold Ore
        3 = Nickel Ore
        4 = Platinum Ore
        5 = Magnesium Ore
        6 = Silicon Ore
        7 = Silver Ore
        8 = Uranium Ore
        9 = Ice
    */

    if (name == "Iron Ore")
        id = 0
    else if (name == "Cobalt Ore")
        id = 1
    else if (name == "Gold Ore")
        id = 2
    else if (name == "Nickel Ore")
        id = 3
    else if (name == "Platinum Ore")
        id = 4
    else if (name == "Magnesium Ore")
        id = 5
    else if (name == "Silicon Ore")
        id = 6
    else if (name == "Silver Ore")
        id = 7
    else if (name == "Uranium Ore")
        id = 8
    else if (name == "Ice")
        id = 9
    else
        id = -1
}