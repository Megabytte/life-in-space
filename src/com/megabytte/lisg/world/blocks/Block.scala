package com.megabytte.lisg.world.blocks

import com.badlogic.gdx.physics.box2d.{Body, Fixture, FixtureDef}
import com.megabytte.lisg.util.Assets

/**
  * Copyright © 2015 Keith Webb
  */
class Block(lBody: Body, createSensor: Boolean) {
    var id = -1
    var body = lBody
    var fixture = lBody.getFixtureList.get(0) //Should be solid fixture

    val xID = Math.ceil(Assets.toPfM(body.getPosition.x) / 32).toInt
    val yID = Math.ceil(Assets.toPfM(body.getPosition.y) / 32).toInt

    var sensorFixture: Fixture = null

    if(createSensor)
        sensorFixture = makeSensor()

    def makeSensor(): Fixture = {
        val fixtureDef = new FixtureDef

        fixtureDef.density = 1
        fixtureDef.isSensor = true

        fixtureDef.shape = fixture.getShape

        val toReturn = body.createFixture(fixtureDef)
        toReturn.setUserData(this)
        toReturn
    }
}
