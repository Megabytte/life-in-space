package com.megabytte.lisg.world

import com.badlogic.gdx.graphics.g2d.{SpriteBatch, TextureRegion}
import com.badlogic.gdx.graphics.{OrthographicCamera, Texture}
import com.badlogic.gdx.maps.tiled.{TiledMapTile, TiledMapTileLayer, TmxMapLoader}
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.{Body, FixtureDef, World}
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.{Gdx, utils}
import com.megabytte.lisg.entities.hostiles.Hostile
import com.megabytte.lisg.entities.items.GenericItem
import com.megabytte.lisg.entities.machines.Machine
import com.megabytte.lisg.entities.player.Player
import com.megabytte.lisg.util.{Assets, Config}
import com.megabytte.lisg.world.blocks._

import scala.collection.mutable

/**
  * Copyright © 2015 Keith Webb
  */
class GameWorld {
    var box2DWorld: World = null
    var worldGenParameters = new WorldGenerationParameters //Todo: Serialize Parameters

    var chunkGenerator: WorldGenerator = null
    var tilesTexture: Texture = null
    var splitTiles: Array[Array[TextureRegion]] = null
    var namedTiles = new mutable.HashMap[String, TextureRegion]()
    var namedMapTiles = new mutable.HashMap[String, TiledMapTile]()
    var nameToIDMap = new mutable.HashMap[String, Int]()
    var IDToNameMap = new mutable.HashMap[Int, String]()

    var currentMap : MapChunk = null
    var spaceMap : MapChunk = null
    var spaceMode = true
    var mapWidth = 0
    var mapHeight = 0
    var tileSize = 0
    val playerBKPos = new Vector2()

    var asteroidMaps = new mutable.HashMap[(Int, Int), MapChunk]()

    def this(worldGenerationParameters: WorldGenerationParameters) {
        this

        mapWidth = worldGenParameters.chunkWidth
        mapHeight = worldGenParameters.chunkHeight
        tileSize = worldGenParameters.tileSize

        tilesTexture = new Texture(Gdx.files.internal("textures/tileSheet.png"))
        splitTiles = TextureRegion.split(tilesTexture, worldGenerationParameters.tileSize, worldGenerationParameters.tileSize)

        generateNameToIDMap()
        generateIDToNameMap()
        generateNameToRegionMapFromTexture()

        chunkGenerator = new WorldGenerator(worldGenerationParameters, splitTiles, tilesTexture, namedTiles)
        spaceMap = new MapChunk(chunkGenerator.generateSpaceMapChunk(0, 0), "Space")
        Config.addWallsToMap(spaceMap.map.getLayers.get("Solid"),
            worldGenerationParameters.chunkWidth,
            worldGenerationParameters.chunkHeight,
            worldGenerationParameters.tileSize)

        generateNameToTileMap()

        currentMap = spaceMap
        currentMap.initializePhysics()

        box2DWorld = currentMap.box2DWorld
        Assets.world = box2DWorld
    }
    def this(path: String) {
        this

        spaceMap = new MapChunk(new TmxMapLoader().load(path), "Space")
        mapWidth = spaceMap.map.getProperties.get("width").asInstanceOf[Int]
        mapHeight = spaceMap.map.getProperties.get("height").asInstanceOf[Int]
        tileSize = spaceMap.map.getProperties.get("tilewidth").asInstanceOf[Int]

        tilesTexture = new Texture(Gdx.files.internal("textures/tileSheet.png"))

        generateNameToIDMap()
        generateIDToNameMap()
        generateNameToRegionMapFromTiledMap()
        generateNameToTileMap()

        currentMap = spaceMap

        Config.addSolidObjectsToMap(currentMap.map, IDToNameMap, tileSize)
        Config.addWallsToMap(currentMap.map.getLayers.get("Solid"), mapWidth, mapHeight, tileSize)
        currentMap.initializePhysics()
        chunkGenerator = new WorldGenerator(worldGenParameters, splitTiles, tilesTexture, namedTiles)

        box2DWorld = currentMap.box2DWorld
        Assets.world = box2DWorld
    }

    def getName(textureRegion: TextureRegion): String = {
        for((key: String, value: TextureRegion) <- namedTiles) {
            if(textureRegion == value)
                return key
        }

        "Unknown"
    }

    def getTileName(x: Int, y: Int): String = {
        if(x < 0 || y < 0 || x > mapWidth || y > mapHeight)
            return "Error"

        val map = currentMap.map
        val layer = map.getLayers.get("Tiles").asInstanceOf[TiledMapTileLayer]
        val cell = layer.getCell(x, y)
        if(cell == null)
            return "Error"

        IDToNameMap.get(cell.getTile.getId).get
    }
    def getTileID(x: Int, y: Int): Int = {
        if(x < 0 || y < 0 || x > mapWidth || y > mapHeight)
            return -1

        val map = currentMap.map
        val layer = map.getLayers.get("Tiles").asInstanceOf[TiledMapTileLayer]
        val cell = layer.getCell(x, y)
        if(cell == null)
            return -1

        cell.getTile.getId
    }

    def changeTile(x: Int, y: Int, name: String): Unit = {
        if(x < 0 || y < 0 || x > mapWidth || y > mapHeight) {
            System.err.println("Out of bounds!")
            return
        }

        val map = currentMap.map
        val layer = map.getLayers.get("Tiles").asInstanceOf[TiledMapTileLayer]
        val cell = layer.getCell(x, y)
        if(cell == null) {
            System.err.println("Cell at " + x + ", " + y + " was null!")
            return
        }
        val cellName = getName(cell.getTile.getTextureRegion)
        cell.setTile(namedMapTiles.get(name).get)
        cell.getTile.setId(nameToIDMap.get(name).get)

        val objects = map.getLayers.get("Solid").getObjects
        val objectBody = objects.get(cellName + " (" + x + "," + y + ")")

        if(objectBody == null) {
            System.err.println("No object with name: " + cellName + " (" + x + "," + y + ")")
            return
        }

        objectBody.setName(name + " (" + x + "," + y + ")")

        if(name == "Space" || name == "Floor")
            objects.remove(objectBody)
    }

    private def generateAsteroid(x: Int, y: Int): Unit = {
        val name = "Asteroid (" + x + ", " + y + ")"
        val mapChunk = new MapChunk(chunkGenerator.generateAsteroidMapChunk(0, 0), name) //Todo: Make X and Y work here...
        Config.addWallsToMap(mapChunk.map.getLayers.get("Solid"),
            worldGenParameters.chunkWidth,
            worldGenParameters.chunkHeight,
            worldGenParameters.tileSize)

        asteroidMaps.put((x, y), mapChunk)
    }

    def enterHole(x: Int, y: Int, player: Player): Unit = {
        Assets.spaceMode = false
        spaceMode = Assets.spaceMode

        if(asteroidMaps.contains((x, y))) {
            currentMap = asteroidMaps.get((x, y)).get
            switchMode(x, y)
        }
        else {
            generateAsteroid(x, y)
            currentMap = asteroidMaps.get((x, y)).get
            switchMode(x, y)

            //Prepare Player "Landing Zone"
            changeTile(x, y, "Floor Hole")
            changeTile(x + 1, y + 1, "Floor")
            changeTile(x + 1, y - 1, "Floor")
            changeTile(x + 1, y, "Floor")
            changeTile(x - 1, y + 1, "Floor")
            changeTile(x - 1, y - 1, "Floor")
            changeTile(x - 1, y, "Floor")
            changeTile(x, y + 1, "Floor")
            changeTile(x, y - 1, "Floor")
        }

        playerBKPos.set(player.body.getPosition)
        currentMap.setupPlayer(player, x * tileSize, y * tileSize)

        currentMap.initializePhysics()
    }
    def exitHole(x: Int, y: Int, player: Player): Unit = {
        Assets.spaceMode = true
        spaceMode = Assets.spaceMode
        currentMap = spaceMap
        switchMode(x, y)

        currentMap.setupPlayer(player, x * tileSize, y * tileSize)
        player.body.setTransform(playerBKPos, 0)

        currentMap.initializePhysics()
    }

    def switchMode(x: Int, y: Int): Unit = {
        box2DWorld = currentMap.box2DWorld
        Assets.world = box2DWorld

        val bodies = new utils.Array[Body](box2DWorld.getBodyCount)
        box2DWorld.getBodies(bodies)

        for(i <- 0 until bodies.size) {
            val body = bodies.get(i)

            for (i <- 0 until body.getFixtureList.size) {
                if (i < body.getFixtureList.size) {
                    val fixture = body.getFixtureList.get(i)
                    val info = fixture.getUserData

                    if (!spaceMode) {
                        info match {
                            case asteroidHole: AsteroidHole =>

                                if (x == asteroidHole.xID && y == asteroidHole.yID) {
                                    val floorHole = new FloorHole(body, false)
                                    asteroidHole.body.destroyFixture(asteroidHole.fixture)
                                    asteroidHole.fixture = null
                                    floorHole.fixture = null
                                    floorHole.sensorFixture = fixture
                                    fixture.setUserData(floorHole)
                                }

                            case _ =>
                        }
                    }
                    else {
                        info match {
                            case floorHole: FloorHole =>

                                if (x == floorHole.xID && y == floorHole.yID) {
                                    val asteroidHole = new AsteroidHole(body, false)

                                    val fixtureDef = new FixtureDef
                                    fixtureDef.density = 1
                                    fixtureDef.restitution = 1
                                    fixtureDef.shape = fixture.getShape
                                    asteroidHole.fixture = body.createFixture(fixtureDef)
                                    fixture.setUserData(asteroidHole)
                                }

                            case _ =>
                        }
                    }
                }
            }
        }
    }

    def update(deltaTime: Float, camera: OrthographicCamera): Unit = {
        currentMap.update(deltaTime, camera)
    }

    def addHostile(x : Float, y : Float): Hostile = currentMap.addHostile(x, y)
    def addItem(x : Float, y : Float, ID: Int, count: Int): GenericItem = currentMap.addItem(x, y, ID, count)
    def addMachine(ID: Int, stage: Stage, x : Float, y : Float): Machine = currentMap.addMachine(ID, stage, x, y)

    def renderMap(): Unit = {
        currentMap.renderMap()
    }
    def renderEntities(spriteBatch: SpriteBatch): Unit = {
        currentMap.renderEntities(spriteBatch)
    }

    def dispose(): Unit = {
        val mapIterator = asteroidMaps.valuesIterator
        while(mapIterator.hasNext) {
            mapIterator.next().dispose()
        }

        spaceMap.dispose()

        if(chunkGenerator != null)
            chunkGenerator.dispose()
    }

    private def generateNameToRegionMapFromTiledMap(): Unit = {
        val tileSets = spaceMap.map.getTileSets
        namedTiles.put("Space", tileSets.getTile(1).getTextureRegion)
        namedTiles.put("Asteroid", tileSets.getTile(2).getTextureRegion)
        namedTiles.put("Iron Ore", tileSets.getTile(3).getTextureRegion)
        namedTiles.put("Asteroid Hole", tileSets.getTile(4).getTextureRegion)
        namedTiles.put("Magnesium Ore", tileSets.getTile(5).getTextureRegion)
        namedTiles.put("Cobalt Ore", tileSets.getTile(6).getTextureRegion)
        namedTiles.put("Gold Ore", tileSets.getTile(7).getTextureRegion)
        namedTiles.put("Silver Ore", tileSets.getTile(8).getTextureRegion)
        namedTiles.put("Silicon Ore", tileSets.getTile(9).getTextureRegion)
        namedTiles.put("Platinum Ore", tileSets.getTile(10).getTextureRegion)
        namedTiles.put("Nickel Ore", tileSets.getTile(11).getTextureRegion)
        namedTiles.put("Uranium Ore", tileSets.getTile(12).getTextureRegion)
        namedTiles.put("Ice", tileSets.getTile(13).getTextureRegion)
        namedTiles.put("Floor", tileSets.getTile(14).getTextureRegion)
        namedTiles.put("Floor Hole", tileSets.getTile(15).getTextureRegion)
    }
    private def generateNameToRegionMapFromTexture(): Unit = {
        namedTiles.put("Space", splitTiles(0)(0))
        namedTiles.put("Asteroid", splitTiles(0)(1))
        namedTiles.put("Iron Ore", splitTiles(0)(2))
        namedTiles.put("Asteroid Hole", splitTiles(0)(3))
        namedTiles.put("Magnesium Ore", splitTiles(0)(4))
        namedTiles.put("Cobalt Ore", splitTiles(1)(0))
        namedTiles.put("Gold Ore", splitTiles(1)(1))
        namedTiles.put("Silver Ore", splitTiles(1)(2))
        namedTiles.put("Silicon Ore", splitTiles(1)(3))
        namedTiles.put("Platinum Ore", splitTiles(1)(4))
        namedTiles.put("Nickel Ore", splitTiles(2)(0))
        namedTiles.put("Uranium Ore", splitTiles(2)(1))
        namedTiles.put("Ice", splitTiles(2)(2))
        namedTiles.put("Floor", splitTiles(2)(3))
        namedTiles.put("Floor Hole", splitTiles(2)(4))
    }
    private def generateNameToIDMap(): Unit = {
        nameToIDMap.put("Space", 1); nameToIDMap.put("Asteroid", 2); nameToIDMap.put("Iron Ore", 3)
        nameToIDMap.put("Asteroid Hole", 4); nameToIDMap.put("Magnesium Ore", 5); nameToIDMap.put("Cobalt Ore", 6)
        nameToIDMap.put("Gold Ore", 7); nameToIDMap.put("Silver Ore", 8); nameToIDMap.put("Silicon Ore", 9)
        nameToIDMap.put("Platinum Ore", 10); nameToIDMap.put("Nickel Ore", 11); nameToIDMap.put("Uranium Ore", 12)
        nameToIDMap.put("Ice", 13); nameToIDMap.put("Floor", 14); nameToIDMap.put("Floor Hole", 15)
    }
    private def generateIDToNameMap(): Unit = {
        IDToNameMap.put(1, "Space"); IDToNameMap.put(2, "Asteroid"); IDToNameMap.put(3, "Iron Ore")
        IDToNameMap.put(4, "Asteroid Hole"); IDToNameMap.put(5, "Magnesium Ore"); IDToNameMap.put(6, "Cobalt Ore")
        IDToNameMap.put(7, "Gold Ore"); IDToNameMap.put(8, "Silver Ore"); IDToNameMap.put(9, "Silicon Ore")
        IDToNameMap.put(10, "Platinum Ore"); IDToNameMap.put(11, "Nickel Ore"); IDToNameMap.put(12, "Uranium Ore")
        IDToNameMap.put(13, "Ice"); IDToNameMap.put(14, "Floor"); IDToNameMap.put(15, "Floor Hole")
    }
    private def generateNameToTileMap(): Unit = {
        val tileSets = spaceMap.map.getTileSets
        namedMapTiles.put("Space", tileSets.getTile(1))
        namedMapTiles.put("Asteroid", tileSets.getTile(2))
        namedMapTiles.put("Iron Ore", tileSets.getTile(3))
        namedMapTiles.put("Asteroid Hole", tileSets.getTile(4))
        namedMapTiles.put("Magnesium Ore", tileSets.getTile(5))
        namedMapTiles.put("Cobalt Ore", tileSets.getTile(6))
        namedMapTiles.put("Gold Ore", tileSets.getTile(7))
        namedMapTiles.put("Silver Ore", tileSets.getTile(8))
        namedMapTiles.put("Silicon Ore", tileSets.getTile(9))
        namedMapTiles.put("Platinum Ore", tileSets.getTile(10))
        namedMapTiles.put("Nickel Ore", tileSets.getTile(11))
        namedMapTiles.put("Uranium Ore", tileSets.getTile(12))
        namedMapTiles.put("Ice", tileSets.getTile(13))
        namedMapTiles.put("Floor", tileSets.getTile(14))
        namedMapTiles.put("Floor Hole", tileSets.getTile(15))
    }
}
