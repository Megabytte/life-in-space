package com.megabytte.lisg.world

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.maps.MapLayer
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell
import com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile
import com.badlogic.gdx.maps.tiled.{TiledMap, TiledMapTile, TiledMapTileLayer, TiledMapTileSet}
import com.megabytte.lisg.util.{Config, OpenSimplexNoise}

import scala.collection.mutable

/**
  * Copyright © 2015 Keith Webb
  */
class WorldGenerator(worldGenerationParameters: WorldGenerationParameters, splitTiles: Array[Array[TextureRegion]],
                     tilesTexture: Texture, namedTiles: mutable.HashMap[String, TextureRegion]) {

    var generator = new OpenSimplexNoise(worldGenerationParameters.seed)
    val chunkWidth = worldGenerationParameters.chunkWidth
    val chunkHeight = worldGenerationParameters.chunkHeight
    val tileSize = worldGenerationParameters.tileSize
    var namedMapTiles = new mutable.HashMap[String, TiledMapTile]()
    var nameToIDMap = new mutable.HashMap[String, Int]()

    val tileSet = new TiledMapTileSet
    tileSet.putTile(1, new StaticTiledMapTile(namedTiles.get("Space").get))
    tileSet.putTile(2, new StaticTiledMapTile(namedTiles.get("Asteroid").get))
    tileSet.putTile(3, new StaticTiledMapTile(namedTiles.get("Iron Ore").get))
    tileSet.putTile(4, new StaticTiledMapTile(namedTiles.get("Asteroid Hole").get))
    tileSet.putTile(5, new StaticTiledMapTile(namedTiles.get("Magnesium Ore").get))
    tileSet.putTile(6, new StaticTiledMapTile(namedTiles.get("Cobalt Ore").get))
    tileSet.putTile(7, new StaticTiledMapTile(namedTiles.get("Gold Ore").get))
    tileSet.putTile(8, new StaticTiledMapTile(namedTiles.get("Silver Ore").get))
    tileSet.putTile(9, new StaticTiledMapTile(namedTiles.get("Silicon Ore").get))
    tileSet.putTile(10, new StaticTiledMapTile(namedTiles.get("Platinum Ore").get))
    tileSet.putTile(11, new StaticTiledMapTile(namedTiles.get("Nickel Ore").get))
    tileSet.putTile(12, new StaticTiledMapTile(namedTiles.get("Uranium Ore").get))
    tileSet.putTile(13, new StaticTiledMapTile(namedTiles.get("Ice").get))
    tileSet.putTile(14, new StaticTiledMapTile(namedTiles.get("Floor").get))
    tileSet.putTile(15, new StaticTiledMapTile(namedTiles.get("Floor Hole").get))
    tileSet.setName("Default")
    tileSet.getProperties.put("tilewidth", tileSize)
    tileSet.getProperties.put("tileheight", tileSize)
    tileSet.getProperties.put("imagesource", "../../textures/tileSheet.png")
    tileSet.getProperties.put("imagewidth", tilesTexture.getWidth)
    tileSet.getProperties.put("imageheight", tilesTexture.getHeight)

    nameToIDMap.put("Space", 1); nameToIDMap.put("Asteroid", 2); nameToIDMap.put("Iron Ore", 3)
    nameToIDMap.put("Asteroid Hole", 4); nameToIDMap.put("Magnesium Ore", 5); nameToIDMap.put("Cobalt Ore", 6)
    nameToIDMap.put("Gold Ore", 7); nameToIDMap.put("Silver Ore", 8); nameToIDMap.put("Silicon Ore", 9)
    nameToIDMap.put("Platinum Ore", 10); nameToIDMap.put("Nickel Ore", 11); nameToIDMap.put("Uranium Ore", 12)
    nameToIDMap.put("Ice", 13); nameToIDMap.put("Floor", 14); nameToIDMap.put("Floor Hole", 15)

    private def betweenRange(value: Float, min: Float, max: Float): Boolean = {
        var result = true

        if(value < min)
            result = false
        if(value > max)
            result = false

        result
    }

    def valueToTexture(value: Float, index: Int, spaceMode: Boolean): String = {
        var toReturn = ""

        if(spaceMode) {
            toReturn = "Space"
            if (betweenRange(value, worldGenerationParameters.asteroidRateLower, worldGenerationParameters.asteroidRateUpper))
                toReturn = "Asteroid"
        }
        else {
            toReturn = "Asteroid"

            //Ore Generation
            if (index == 0 && betweenRange(value, worldGenerationParameters.ironRateLower, worldGenerationParameters.ironRateUpper))
                toReturn = "Iron Ore"
            else if (index == 1 && betweenRange(value, worldGenerationParameters.cobaltRateLower, worldGenerationParameters.cobaltRateUpper))
                toReturn = "Cobalt Ore"
            else if (index == 2 && betweenRange(value, worldGenerationParameters.goldRateLower, worldGenerationParameters.goldRateUpper))
                toReturn = "Gold Ore"
            else if (index == 3 && betweenRange(value, worldGenerationParameters.nickelRateLower, worldGenerationParameters.nickelRateUpper))
                toReturn = "Nickel Ore"
            else if (index == 4 && betweenRange(value, worldGenerationParameters.platinumRateLower, worldGenerationParameters.platinumRateUpper))
                toReturn = "Platinum Ore"
            else if (index == 5 && betweenRange(value, worldGenerationParameters.magnesiumRateLower, worldGenerationParameters.magnesiumRateUpper))
                toReturn = "Magnesium Ore"
            else if (index == 6 && betweenRange(value, worldGenerationParameters.siliconRateLower, worldGenerationParameters.siliconRateUpper))
                toReturn = "Silicon Ore"
            else if (index == 7 && betweenRange(value, worldGenerationParameters.silverRateLower, worldGenerationParameters.silverRateUpper))
                toReturn = "Silver Ore"
            else if (index == 8 && betweenRange(value, worldGenerationParameters.uraniumRateLower, worldGenerationParameters.uraniumRateUpper))
                toReturn = "Uranium Ore"
            else if (index == 9 && betweenRange(value, worldGenerationParameters.iceRateLower, worldGenerationParameters.iceRateUpper))
                toReturn = "Ice"
        }

        toReturn
    }

    def rawValueGeneration(sx: Int, sy: Int): Array[Array[Float]] = {
        val rawValues = new Array[Array[Float]](chunkWidth)

        val scale = 0.07f
        for(x <- sx until sx + chunkWidth) {
            rawValues(x) = new Array[Float](chunkHeight)
            for(y <- sy until sy + chunkHeight) {
                rawValues(x - sx)(y - sy) = generator.eval_specific(16, x, y, 0.3f, scale, 0, 1)
            }
        }

        //PixmapIO.writePNG(Gdx.files.absolute("/Users/Megabytte/IdeaProjects/LifeInSpace/assets/test.png"), OpenSimplexNoise.generatePixmap(rawValues))

        rawValues
    }

    def oreGeneration(sx: Int, sy: Int): Array[Array[Array[Float]]] = {
        val toReturn = new Array[Array[Array[Float]]](10)

        for(i <- 0 until 10) {
            generator = new OpenSimplexNoise(worldGenerationParameters.seed + i)
            toReturn(i) = rawValueGeneration(sx, sy)
        }

        generator = new OpenSimplexNoise(worldGenerationParameters.seed)

        toReturn
    }

    def generateSpaceMapChunk(chunk_x: Int, chunk_y: Int): TiledMap = {
        val map = new TiledMap

        map.getProperties.put("width", worldGenerationParameters.chunkWidth)
        map.getProperties.put("height", worldGenerationParameters.chunkHeight)
        map.getProperties.put("tilewidth", worldGenerationParameters.tileSize)
        map.getProperties.put("tileheight", worldGenerationParameters.tileSize)

        map.getTileSets.addTileSet(tileSet)

        val layers = map.getLayers

        val tileLayer = new TiledMapTileLayer(chunkWidth, chunkHeight, tileSize, tileSize)
        tileLayer.setName("Tiles")
        val objectGroup = new MapLayer
        objectGroup.setName("Solid")

        val oreRawValues = rawValueGeneration(chunk_x, chunk_y)

        for(x <- 0 until chunkWidth) {
            for(y <- 0 until chunkHeight) {
                val texID = valueToTexture(oreRawValues(x)(y), 0, spaceMode = true)

                if(texID != "Space") {
                    objectGroup.getObjects.add(Config.createMapObject(texID, x, y, tileSize, tileSize))
                }

                val cell = new Cell
                cell.setTile(tileSet.getTile(nameToIDMap.get(texID).get))
                cell.getTile.setId(nameToIDMap.get(texID).get)

                tileLayer.setCell(x, y, cell)
            }
        }

        layers.add(tileLayer)
        layers.add(objectGroup)

        map
    }

    private def nameToOreID(texID: String): Int = {
        if(texID == "Iron Ore")
            0
        else if(texID == "Cobalt Ore")
            1
        else if(texID == "Gold Ore")
            2
        else if(texID == "Nickel Ore")
            3
        else if(texID == "Platinum Ore")
            4
        else if(texID == "Magnesium Ore")
            5
        else if(texID == "Silicon Ore")
            6
        else if(texID == "Silver Ore")
            7
        else if(texID == "Uranium Ore")
            8
        else if(texID == "Ice")
            9
        else
            -1
    }
    private def oreIDToName(id: Int): String = {
        if(id == 0)
            "Iron Ore"
        else if(id == 1)
            "Cobalt Ore"
        else if(id == 2)
            "Gold Ore"
        else if(id == 3)
            "Nickel Ore"
        else if(id == 4)
            "Platinum Ore"
        else if(id == 5)
            "Magnesium Ore"
        else if(id == 6)
            "Silicon Ore"
        else if(id == 7)
            "Silver Ore"
        else if(id == 8)
            "Uranium Ore"
        else if(id == 9)
            "Ice"
        else
            "Unknown"
    }

    //Todo: Have Generator Make Cave Systems In Asteroids
    def generateAsteroidMapChunk(chunk_x: Int, chunk_y: Int): TiledMap = {
        val map = new TiledMap

        map.getProperties.put("width", worldGenerationParameters.chunkWidth)
        map.getProperties.put("height", worldGenerationParameters.chunkHeight)
        map.getProperties.put("tilewidth", worldGenerationParameters.tileSize)
        map.getProperties.put("tileheight", worldGenerationParameters.tileSize)

        map.getTileSets.addTileSet(tileSet)

        val layers = map.getLayers

        val tileLayer = new TiledMapTileLayer(chunkWidth, chunkHeight, tileSize, tileSize)
        tileLayer.setName("Tiles")
        val objectGroup = new MapLayer
        objectGroup.setName("Solid")

        val oreGenResults = oreGeneration(chunk_x, chunk_y)

        for(i <- oreGenResults.indices) {
            for (x <- 0 until chunkWidth) {
                for (y <- 0 until chunkHeight) {
                    //This Code is Ran One Hundred Thousand Times!!! 100,000!!!
                    val rawValues = oreGenResults(i)
                    val texID = valueToTexture(rawValues(x)(y), i, spaceMode = false)

                    if(nameToOreID(texID) == i) {
                        objectGroup.getObjects.add(Config.createMapObject(texID, x, y, tileSize, tileSize))

                        val cell = new Cell
                        cell.setTile(tileSet.getTile(nameToIDMap.get(texID).get))
                        cell.getTile.setId(nameToIDMap.get(texID).get)

                        tileLayer.setCell(x, y, cell)
                    }
                }
            }
        }

        for (x <- 0 until chunkWidth) {
            for (y <- 0 until chunkHeight) {
                val cell = tileLayer.getCell(x, y)
                if(cell == null) {
                    objectGroup.getObjects.add(Config.createMapObject("Asteroid", x, y, tileSize, tileSize))
                    val cell = new Cell
                    cell.setTile(tileSet.getTile(nameToIDMap.get("Asteroid").get))
                    cell.getTile.setId(nameToIDMap.get("Asteroid").get)
                    tileLayer.setCell(x, y, cell)
                }
            }
        }

        layers.add(tileLayer)
        layers.add(objectGroup)

        map
    }

    def dispose(): Unit = {
        tilesTexture.dispose()
    }



}
